# EL-LB-FullScale

Supporting code to the article **Dapelo, D., Kymmerländer, A., Krause, M., Bridgeman, J. (2023). Lattice-Boltzmann LES modelling of a full-scale, biogas-mixed anaerobic digester.** ***Eng with Comp.*** **17. DOI 10.1007/s00366-023-01854-3**.

Please cite the article if using any part of the code provided here. Article `.pdf` and `.bib` file are provided in `article/`.

## Prerequisites
- GNU Make
- C++17 or later
- (for parallel CPU processing) OpenMPI
- (for job handling and post-processing) Python 3 with the following packages:
  - Matplotlib
  - Numpy
  - Pandas

## Install
1. Clone this repository on a folder of your choice:
    ```
    git clone https://gitlab.com/davidedapelo/el-lb-fullscale.git
    ```

2. Install [OpenLB](https://www.openlb.net/) 1.6 on a folder of your choice. [Direct link](https://zenodo.org/record/7773497/files/olb-1.6r0.tgz?download=1) to the version tarball. Please refer to the [User Guide](https://www.openlb.net/wp-content/uploads/2023/06/olb_ug-1.6r0.pdf) Chapter 9.

3. Edit the file `fullScale_final/Makefile`. Change `OLB_ROOT` into the folder of OpenLB.

4. Build `fullscale_final.cpp` by invoking `make` from the `fullscale_final` folder. If successful, the binary file `fullscale_final/fullscale_final` will be created.

5. Create the folder `fullscale_final/git_ignore` and copy the binary file `fullscale_final/fullscale_final` therein.

## Getting started

The folder `fullscale_final/jobs` contains a number of `.sh` shell scripts to launch arrays of simulations on either the local machine, or an HPC equipped with the SLURM job scheduler. Other `.sh` scripts run array of post-processing operations. It is to the User to modify these scripts according to their needs.

A good starting point is running the test script `fullscale_final/jobs/test.sh` either on HPC (by setting the SLURM directives accordingly), or on a local machine. That must be done from the `fullscale_final` folder:
```
cd fullscale_final
./jobs.test.sh
```