#! /bin/bash

# python3 resources/replot_phi.py \
#         <path of the run> \
#         <name of the folder containing the replots> \
#         <Colorbar label> \
#         <minimum value in the colorbar> \
#         <maximum value in the colorbar> \
#         <slice orientation> \ # perpendicular to Y, or to Z
#         <string to be contained in rawData filename_1> \
#         ...
#         <string to be contained in rawData filename_N> \

GREENBLUE='#EAFAF1'
ORANGE='#FEF9E7'

FS0_015_080=videos__TS=0__flow=05__d=015__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__ball__antiDiff=0.0/80-0/
FS0_050_080=videos__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__ball__antiDiff=0.0/80-0/
FS0_050_160=videos__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__ball__antiDiff=0.0/160-0/
FS1_050_080=videos__TS=1__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__ball__antiDiff=0.0/80-0/
FS2_050_080=videos__TS=2__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__ball__antiDiff=0.0/80-0/

#for FOLDER_SET in $FS0_050_080 $FS0_015_080 $FS0_050_160 $FS0_050_160 $FS1_050_080 $FS2_050_080; do
for FOLDER_SET in $FS2_050_080; do
  FOLDER_RUN=git_ignore/videos/$FOLDER_SET

  if   [ "$FOLDER_SET" = "$FS0_015_080" ] ; then COLOUR=$GREENBLUE ; echo TS=0, d=015, nx=080
  elif [ "$FOLDER_SET" = "$FS0_050_160" ] ; then COLOUR=$GREENBLUE ; echo TS=0, d=050, nx=160
  elif [ "$FOLDER_SET" = "$FS0_050_160" ] ; then COLOUR=$GREENBLUE ; echo TS=0, d=050, nx=160
  elif [ "$FOLDER_SET" = "$FS1_050_080" ] ; then COLOUR=$GREENBLUE ; echo TS=1, d=050, nx=080
  elif [ "$FOLDER_SET" = "$FS2_050_080" ] ; then COLOUR=$ORANGE    ; echo TS=2, d=015, nx=080
  else                                           COLOUR=white      ; echo TS=0, d=050, nx=080
  fi

  python3 resources/replot_u.py \
          $FOLDER_RUN \
          data_plots_u \
          "Velocity magnitude (m/s)" \
          $COLOUR \
          0.0 \
          0.3 \
          Y \

  python3 resources/replot_visco.py \
          $FOLDER_RUN \
          data_plots_visco \
          "log10 of the kinematic viscosity" \
          $COLOUR \
          -1 \
          1 \
          Y \

  python3 resources/replot_phi.py \
          $FOLDER_RUN \
          data_plots_phi \
          "Finite-difference scalar field" \
          $COLOUR \
          0.0 \
          0.01 \
          Y \
          phi \

  ./resources/scripts_postProcessing/toVid_simple.sh $FOLDER_RUN"tmp/data_plots_u"
  ./resources/scripts_postProcessing/toVid_simple.sh $FOLDER_RUN"tmp/data_plots_visco"
  ./resources/scripts_postProcessing/toVid_simple.sh $FOLDER_RUN"tmp/data_plots_phi"

done
