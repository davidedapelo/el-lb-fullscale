#! /bin/bash

# python3 resources/replot_phi.py \
#         <path of the run> \
#         <name of the folder containing the replots> \
#         <Colorbar label> \
#         <minimum value in the colorbar> \
#         <maximum value in the colorbar> \
#         <slice orientation> \ # perpendicular to Y, or to Z
#         <string to be contained in rawData filename_1> \
#         ...
#         <string to be contained in rawData filename_N> \


do_geo() {
  python3 resources/replot_geo.py \
          $FOLDER_IN \
          data_plots_geo \
          "Material number" \
          white \
          0.0 \
          3 \
          $1 \
          geo $2 \
          0
  mv $FOLDER_IN/tmp/data_plots_geo/0.pdf $FOLDER_OUT/geo$1.pdf
  rmdir $FOLDER_IN/tmp/data_plots_geo
}

do_u() {
  python3 resources/replot_u.py \
          $FOLDER_IN \
          data_plots_u \
          "Velocity magnitude (m/s)" \
          white \
          0.0 \
          0.3 \
          Y $EXTRA \
          $TIMESTEP $1 $2 $3 $4 $5
  mv $FOLDER_IN/tmp/data_plots_u/$TIMESTEP.pdf $FOLDER_OUT/$NAME_PREFIX-flow-$NAME_SUFFIX-$NAME_END.pdf
  rmdir $FOLDER_IN/tmp/data_plots_u
}

do_visco() {
  python3 resources/replot_visco.py \
          $FOLDER_IN \
          data_plots_visco \
          "log10 of the kinematic viscosity" \
          white \
          -1 \
          1 \
          Y $EXTRA \
          $TIMESTEP
  mv $FOLDER_IN/tmp/data_plots_visco/$TIMESTEP.pdf $FOLDER_OUT/$NAME_PREFIX-visco-$NAME_SUFFIX-$NAME_END.pdf
  rmdir $FOLDER_IN/tmp/data_plots_visco
}

do_phi() {
  python3 resources/replot_phi.py \
          $FOLDER_IN \
          data_plots_phi \
          "Finite-difference scalar field" \
          white \
          0.0 \
          0.01 \
          Y \
          phi $EXTRA \
          $TIMESTEP
  mv $FOLDER_IN/tmp/data_plots_phi/$TIMESTEP.pdf $FOLDER_OUT/$NAME_PREFIX-$1-$NAME_SUFFIX-$NAME_END.pdf
  rmdir $FOLDER_IN/tmp/data_plots_phi
}


##########################################################################
# Common data
##########################################################################
# Container folder
FOLDER_OUT=git_ignore/el-lb-fullscale
if [ ! -d $FOLDER_OUT ] ; then mkdir $FOLDER_OUT ; fi
# Suffix containing nx
NAME_SUFFIX=80


##########################################################################
# Geometry
##########################################################################
# Y
FOLDER_IN=git_ignore/longP13/long60__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__seed__antiDiff=0.0/80-0/
do_geo Y 0.00
do_geo Z 0.05


##########################################################################
# Flow patterns
##########################################################################
# 2.5% TS
FOLDER_IN=git_ignore/0beforeCorrection/TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__antiDiff=0.0/80-0/
NAME_PREFIX=T0-d50-sc020
TIMESTEP=18990
NAME_END=300s
do_u vortex 3 6.5 9.5 13
do_visco
TIMESTEP=37980
NAME_END=600s
do_u vortex 3 6.5 9.5 13
do_visco

# 5.4% TS
FOLDER_IN=git_ignore/0beforeCorrection/TS=1__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__antiDiff=0.0/80-0/
NAME_PREFIX=T1-d50-sc020
TIMESTEP=18060
NAME_END=300s
do_u vortex 3 6.5 9.5 13
do_visco
TIMESTEP=36120
NAME_END=600s
do_u vortex 3 6.5 9.5 13
do_visco

# 7.5% TS
FOLDER_IN=git_ignore/0beforeCorrection/TS=2__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__antiDiff=0.0/80-0/
NAME_PREFIX=T2-d50-sc020
TIMESTEP=16470
NAME_END=300s
do_u vortex 3 6.5 9.5 13
do_visco
TIMESTEP=32940
NAME_END=600s
do_u vortex 3 6.5 9.5 13
do_visco


##########################################################################
# Uniformity index
##########################################################################
# Seed
FOLDER_IN=git_ignore/0beforeCorrection/TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__antiDiff=0.0/80-0/
NAME_PREFIX=T0-d50-sc020
TIMESTEP=0
EXTRA=__0.dat
NAME_END=0s
do_phi sparse
TIMESTEP=633
EXTRA=__633.dat
NAME_END=10s
do_phi sparse
TIMESTEP=18990
EXTRA=__18990.dat
NAME_END=300s
do_phi sparse
TIMESTEP=37980
EXTRA=__37980.dat
NAME_END=600s
do_phi sparse

# Ball
FOLDER_IN=git_ignore/0beforeCorrection/TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__ball__antiDiff=0.0/80-0/
TIMESTEP=0
EXTRA=__0.dat
NAME_END=0s
do_phi ball
TIMESTEP=633
EXTRA=__633.dat
NAME_END=10s
do_phi ball
TIMESTEP=18990
EXTRA=__18990.dat
NAME_END=300s
do_phi ball
TIMESTEP=37980
EXTRA=__37980.dat
NAME_END=600s
do_phi ball


# From now on, no extra necessary
EXTRA=""


##########################################################################
# Lagrangian subcycle plots
##########################################################################
# 20 Lagrangian subcycles
FOLDER_IN=git_ignore/long60__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__ball__antiDiff=0.0/80-0/
TIMESTEP=227820
NAME_PREFIX=T0-d50-sc020
NAME_END=3600s
do_u
do_visco
do_phi ball

# 400 Lagrangian subcycles
FOLDER_IN=git_ignore/long60__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=400__ball__antiDiff=0.0/80-0/
NAME_PREFIX=T0-d50-sc400
do_u
do_visco
do_phi ball


##########################################################################
# Bubble size plots
##########################################################################
# 15 mm bubble size
FOLDER_IN=git_ignore/longP13/long60__TS=0__flow=05__d=015__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__seed__antiDiff=0.0/80-0/
TIMESTEP=26124
NAME_PREFIX=T0-d15-sc100
NAME_END=840s
do_u
do_visco
do_phi sparse

# 50 mm bubble size
FOLDER_IN=git_ignore/longP13/long60__TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=100__seed__antiDiff=0.0/80-0/
TIMESTEP=227820
NAME_PREFIX=T0-d50-sc100
do_u
do_visco
do_phi sparse
