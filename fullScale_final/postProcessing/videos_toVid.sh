#! /bin/bash

FOLDER_SERIES=git_ignore/videos/

FOLDER_D=data_plots_d
FOLDER_NX=data_plots_nx
FOLDER_TS=data_plots_ts

for F in $FOLDER_D $FOLDER_NX $FOLDER_TS; do
  NAME=$FOLDER_SERIES$F
  ffmpeg -r 30 -f image2 -i $NAME/%d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" $NAME".mp4"
done
