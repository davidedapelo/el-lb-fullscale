#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import Parent_Index_Occurences_SortingAlgorithm as SortingAlgorithm
from helpers_data import extractData_vortices as extractData
from helpers_plot import plotVortexCoordinate

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 2:
        raise ValueError('Exactly ONE argv argument (==path of the folder containing the runs) must by inputted. Quitting.')
    if not Path(sys.argv[1]).is_dir():
        raise ValueError('Folder ' + sys.argv[1] + ' does not exist or is not a directory. Quitting.')

    def funct_x (x, a, b, c):
        return a + b/x + c/x**2

    def funct_y (x, a, b, c):
        return a + b/x + c/x**2

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/vortexAverage/').mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'nx', SortingAlgorithm())
    plotVortexCoordinate ( data[['x_avg', 'x_std']].rename(columns={'x_avg': 'avg', 'x_std': 'std'}, inplace=False),
                           sys.argv[1]+'/vortexAverage/vortexX.pdf',
                           'Lattice nodes across the X direction',
                           ['Average vortex X position (m)', 'Error on average vortex X position (m)'],
                           'lower right',
                           funct_x,
                           [['$x_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_x=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                           60
                         )
    plotVortexCoordinate ( data[['y_avg', 'y_std']].rename(columns={'y_avg': 'avg', 'y_std': 'std'}, inplace=False),
                           sys.argv[1]+'/vortexAverage/vortexY.pdf',
                           'Lattice nodes across the X direction',
                           ['Average vortex Y position (m)', 'Error on average vortex Y position (m)'],
                           'upper right',
                           funct_y,
                           [['$y_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_y=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                           200
                         )