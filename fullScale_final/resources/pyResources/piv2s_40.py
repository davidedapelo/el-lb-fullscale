import matplotlib.pyplot as plt
import numpy as np

import pyResources.plotting as pt
import pyResources.dataElaboration as de
import pyResources.filesHandling as fh

plt.switch_backend('agg') # Added to try to solve issues on HPC



##############################################################################################



file_out  = '../../git_ignore/0expDataPIV/figures/cmc2s_40.pdf'
file_test = '../../git_ignore/0expDataPIV/figures/test2s_40.png'
x         = np.genfromtxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-x.txt' )
y         = np.genfromtxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-y.txt' )
u         = np.genfromtxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-u.txt' )
v         = np.genfromtxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-v.txt' )
d         = fh.readDictFile( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-dict.txt' )

xx = x
yy = y

centre = np.array([ d['centreX'], d['centreY'] ])
top    = np.array([ centre[0], d['top'] ])
bottom = np.array([ centre[0], d['bottom'] ])

xmin = centre[0] - d['radius'] - (x[1]-x[0])
xmax = centre[0] + d['radius'] + (x[1]-x[0])
xminShift = centre[0] - np.sqrt(d['radius']**2-d['zShift']**2) - (x[1]-x[0])
xmaxShift = centre[0] + np.sqrt(d['radius']**2-d['zShift']**2) + (x[1]-x[0])

pt.testPlotPIV(file_test, xx, yy, u, v, d['uLegend'], centre, top, bottom, xminShift, xmaxShift)

xxC, yyC, uC, vC = de.cutFieldsPIV(xx, yy, u, v, xmin, xmax, bottom[1], top[1], centre[0])
np.savetxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-xC.txt' , xxC)
np.savetxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-yC.txt' , yyC)
np.savetxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-uC.txt' ,  uC)
np.savetxt( '../../git_ignore/0expDataPIV/postProcess/CMC02_40_s-vC.txt' ,  vC)

plt.clf()
pt.plotPlane(file_out, xxC, yyC, uC, vC, np.sqrt(uC**2 + vC**2), d['uLegend'])



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
