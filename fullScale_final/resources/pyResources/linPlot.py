import sys
import numpy as np
import matplotlib.pyplot as plt



##############################################################################################

if len(sys.argv) == 24:
    matrixInput = np.genfromtxt( str(sys.argv[1]) )
    fileOutX  = str(sys.argv[2])
    fileOutY  = str(sys.argv[3])
    fileOut   = str(sys.argv[4])
    xRef      = float(sys.argv[5])
    yRef      = float(sys.argv[6])
    labelX1   = str(sys.argv[7])
    labelY1   = str(sys.argv[8])
    bins1     = int(sys.argv[9])
    legend1   = str(sys.argv[10])
    labelX2   = str(sys.argv[11])
    labelY2   = str(sys.argv[12])
    bins2     = int(sys.argv[13])
    legend2   = str(sys.argv[14])
    labelX3   = str(sys.argv[15])
    labelY3   = str(sys.argv[16])
    bins3     = int(sys.argv[17])
    legend3   = str(sys.argv[18])
    labelYs   = str(sys.argv[19])
    smallL    = float(sys.argv[20])
    smallB    = float(sys.argv[21])
    smallW    = float(sys.argv[22])
    smallH    = float(sys.argv[23])
    variation = 'y'

elif len(sys.argv) == 4:
    matrixInput = np.genfromtxt( str(sys.argv[1]) )
    fileOutX  = str(sys.argv[2])
    fileOutY  = str(sys.argv[3])
    labelX1   = 'x'
    labelY1   = "y"
    labelX2   = 'x'
    labelY2   = "y"
    legend1 = 'lower right'
    legend2 = 'lower right'
    bins1 = 7
    bins2 = 7
    variation = 'n'

else:
    matrixInput = np.genfromtxt( '../../git_ignore/cvA1_1_images/vortexCoords.txt' )
    fileOutX  = '../../git_ignore/cvA1_1_images/vortexCoordsX.png'
    fileOutY  = '../../git_ignore/cvA1_1_images/vortexCoordsY.png'
    variation = 'n'

N = matrixInput[:,0]
x = matrixInput[:,1]
y = matrixInput[:,2]

if variation == 'y':
    constRefX = N*0. + xRef
    constRefY = N*0. + yRef

plt.clf()
plt.rcParams.update({'font.size': 14})
plt.locator_params(axis='x', nbins=bins1)
plt.plot(N, x, '-x', label='Numerical')
if variation == 'y':
    plt.plot(N, constRefX, ':', label='Experiment')
plt.xlabel(labelX1)
plt.ylabel(labelY1)
plt.legend(loc=legend1, frameon=False)
plt.tight_layout()
plt.savefig(fileOutX, format='pdf')

plt.clf()
plt.rcParams.update({'font.size': 14})
plt.locator_params(axis='x', nbins=bins2)
plt.plot(N, y, '-x', label='Numerical')
if variation == 'y':
    plt.plot(N, constRefY, ':', label='Experiment')
plt.xlabel(labelX2)
plt.ylabel(labelY2)
plt.legend(loc=legend2, frameon=False)
#plt.tight_layout()

plt.savefig(fileOutY, format='pdf')

plt.clf()
plt.rcParams.update({'font.size': 14})
plt.locator_params(axis='x', nbins=bins3)
plt.plot(N, 100*(x-constRefX)/constRefX,  '-xr', label='X coordinate')
plt.plot(N, 100*(y-constRefY)/constRefY, '--Db', label='Y coordinate')
#if variation == 'y':
    #plt.plot(N, constRefY, ':', label='Experiment')
plt.xlabel(labelX3)
plt.ylabel(labelY3)
plt.legend(loc=legend3, frameon=False)
#plt.tight_layout()

plt.rcParams.update({'font.size': 7})
a = plt.axes([smallL, smallB, smallW, smallH])
plt.locator_params(axis='x', nbins=bins3)
plt.plot(N, 100*(y-constRefY)/constRefY, '--Db', label='Y coordinate')
plt.xlabel(labelX3)
plt.ylabel(labelYs)
plt.legend(loc=legend2, frameon=False)


plt.savefig(fileOut, format='pdf')



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
