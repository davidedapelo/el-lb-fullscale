import subprocess as sp
import sys
import numpy as np

import pyResources.plotting as pt
import pyResources.dataElaboration as de
import pyResources.filesHandling as fh



##############################################################################################

dirname = str(sys.argv[1])

for item in ['x',  'y', 'u', 'v']:
    de.averageAndSave( dirname+'-'+item+'.txt', fh.buildListOfNamesPIV( dirname, item+'.txt' ) )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
