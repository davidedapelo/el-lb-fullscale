import matplotlib.pyplot as plt
import numpy as np

from matplotlib.cm import get_cmap

plt.switch_backend('agg') # Added to solve issues on HPC



##############################################################################################
def plotGrideDep_deltaX(fig, figTitle, axisTitle, x, y):
##############################################################################################
    '''Plots a grid-dependence graph to pdf file: quantity against resolution.

    Args:
        matplotlib.pyplot.figure:  fig:                    figure handler
        string                     figTitle:               figure full title + extension
        string:                    axisTitle:              y axis title
        dict:                      x:                      dictionary of list[float] containing x entries:
                                   x['resolution']:        resolution (lower x axis)
                                   x['deltaXoverDBubble']: delta x over bubble size (upper x axis)
        list[float]:               y:                      y entries

        kwargs:                 
            float:  yExp:  y experimental data
    '''

#---------- cleaning from possible previous stuff on fig. ------------------------------------
    fig.clf()

#---------- x-y figure. ----------------------------------------------------------------------
    ax1 = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
    ax1.loglog(x['latticeSize'], [abs(item-x['fitCoeffs'][0]) for item in y              ], 'X', label='Numerical')
    ax1.loglog(x['latticeSize'], [abs(item-x['fitCoeffs'][0]) for item in x['dataFitted']], '-', label='Fitted')
    ax1.set_xlabel('Lattice size (m)')
    ax1.set_ylabel(axisTitle)

    plt.text ( 0.62, 0.05, \
               "p[0]=%4.3e\np[1]=%4.3e\np[2]=%4.3e" % (x['fitCoeffs'][0], x['fitCoeffs'][1], x['fitCoeffs'][2]), \
               transform=ax1.transAxes)

#---------- plotting on file. ----------------------------------------------------------------
    fig.savefig(figTitle, bbox_inches='tight', format='pdf')



##############################################################################################
def plotGrideDep_resolution(fig, figTitle, axisTitle, x, y, **kwargs):
##############################################################################################
    '''Plots a grid-dependence graph to pdf file: quantity against resolution.

    Args:
        matplotlib.pyplot.figure:  fig:        figure handler
        string                     figTitle:   figure full title + extension
        string:                    axisTitle:  y axis title
        list[float]:               x:          resolution (lower x axis)
        list[float]:               y:          y entries

    kwargs:
        float:        yExp:  y experimental data
        list[float]:  yFit:  y fitted data
    '''

#---------- Checking kwargs. -----------------------------------------------------------------
    (there_is_yExp, yExp) = (True, kwargs['yExp']) if 'yExp' in kwargs and not kwargs['yExp']==None else (False, None)
    (there_is_yFit, yFit) = (True, kwargs['yFit']) if 'yFit' in kwargs and not kwargs['yFit']==[]   else (False, []  )

    print('there_is_yExp=' + str(there_is_yExp))
    print('there_is_yFit=' + str(there_is_yFit))

#---------- cleaning from possible previous stuff on fig. ------------------------------------
    fig.clf()

#---------- x-y figure. ----------------------------------------------------------------------
    ax1 = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
    ax1.plot(x, y, 'X' if there_is_yFit else '-X', label='Numerical')
    if there_is_yExp:
        ax1.plot(x, [item*0. + yExp for item in x], ':', label='Experiment')
    if there_is_yFit:
            ax1.plot(x, yFit, '-', label='Fitted')
    ax1.set_xlabel('Voxels per unit length')
    ax1.set_ylabel(axisTitle)
    
#---------- extra top x axis. ----------------------------------------------------------------
    if 'deltaXoverDBubble' in x:
        ax2 = ax1.twiny()
        ax2.set_xlim(ax1.get_xlim())
        ax2.set_xticks(x)
        ax2.set_xticklabels(x['deltaXoverDBubble'])
        ax2.set_xlabel('deltaX over bubble diameter')

#---------- plotting on file. ----------------------------------------------------------------
    fig.savefig(figTitle, bbox_inches='tight', format='pdf')



##############################################################################################
def plotPlane( file_out, x1, x2, u1, u2, uMag, uMin, uMax, label='Velocity magnitude (m/s)', facecolor='white', cmap='viridis' ):
##############################################################################################
    """Plots the velocity field.
    
    Magnitude colormap & quivers are saved in a figure file.

    Args:
        str:      file_out:  output filename
        np.array: x1:        horizontal coordinate matrix
        np.array: x2:        vertical coordinate matrix
        np.array: u1:        velocity field, horizontal direction
        np.array: u2:        velocity field, vertical direction
        np.array: uMag:      velocity field, magnitude
        float:    uMin:      minimum velocity magnitude for colorbar
        float:    uMax:      maximum velocity magnitude for colorbar
        str:      label:     label of the colorbar
        str:      facecolor: background color
        str:      cmap:      colormap

    Returns:
        (nothing)
    """
    plt.rcParams.update({'font.size': 14})
    #plt.axis('off')

#---------- Colour plot. ---------------------------------------------------------------------
    # Default colormaps: viridis, plasma, inferno, magma and CIVIDIS
    plt.pcolormesh(x1, x2, uMag, vmin=uMin, vmax=uMax, cmap=cmap, shading='nearest',linewidth=0,rasterized=True)
    #plt.colorbar(label='Velocity magnitude (m/s)', ticks=np.arange(0, uLegend+0.01, 0.01)) # original, dimensioned and rigid
    #plt.colorbar(label='u / uExp', ticks=np.arange(0, 1.05*uLegend, 0.2*uLegend))
    plt.colorbar(label=label, ticks=np.arange(uMin, 1.05*uMax, 0.2*uMax))

#---------- Plane orthogonal to axis Y. ------------------------------------------------------
    # Resizing velocity field for unitary length quivers.
    with np.errstate(invalid='ignore'):
        uu1 = u1 / np.sqrt(u1**2 + u2**2)
        uu2 = u2 / np.sqrt(u1**2 + u2**2)

    # 20 quiver point for coord axis direction.
    stepX1 = int(x1.shape[0] / 10)
    stepX2 = int(x1.shape[1] / 10)

    plt.quiver( x1[::stepX1,::stepX2],  x2[::stepX1,::stepX2],
               uu1[::stepX1,::stepX2], uu2[::stepX1,::stepX2] )

    plt.tight_layout()

#---------- Saving on file. ------------------------------------------------------------------
    plt.savefig(file_out, format='pdf', facecolor=facecolor)



##############################################################################################
def plotPhiPlane( file_out, x1, x2, phi, vInf, vSup, label='Finite-difference field concentration', facecolor='white', cmap='viridis', discrete_cmap=False ):
##############################################################################################
    """Plots the finite-difference field.
    
    Args:
        str:      file_out:  output filename
        np.array: x1:        horizontal coordinate matrix
        np.array: x2:        vertical coordinate matrix
        np.array: phi:       finite-difference field
        float:    vInf:      inferior value of the legend for viscosity
        float:    vSup:      superior value of the legend for viscosity
        str:      label:     label of the colorbar
        str:      facecolor: background color
        str:      cmap:      colormap

    Returns:
        (nothing)
    """
    plt.rcParams.update({'font.size': 14})
    #plt.axis('off')

#---------- Colour plot. ---------------------------------------------------------------------
    # Default colormaps: viridis, plasma, inferno, magma and CIVIDIS
    plt.pcolormesh(x1, x2, phi,
                   vmin=(vInf-0.5 if discrete_cmap else vInf),
                   vmax=(vSup+0.5 if discrete_cmap else vSup),
                   cmap=( get_cmap(cmap, int(vSup-vInf+1)) if discrete_cmap else cmap ),
                   shading='nearest',linewidth=0,rasterized=True
                   )

    cbar = plt.colorbar(label=label)
    if discrete_cmap:
        cbar.ax.locator_params(nbins=vSup-vInf+1)

    plt.tight_layout()

#---------- Saving on file. ------------------------------------------------------------------
    plt.savefig(file_out, format='pdf', facecolor=facecolor)



##############################################################################################
def plotViscoPlane( file_out, x1, x2, visco, vInf, vSup, label='log10 of the kinematic viscosity', facecolor='white', cmap='viridis'  ):
##############################################################################################
    """Plots the (log10 of the) viscosity field.
    
    Args:
        str:      file_out:  output filename
        np.array: x1:        horizontal coordinate matrix
        np.array: x2:        vertical coordinate matrix
        np.array: visco:     (log10 of the ) viscosity field
        float:    vInf:      inferior value of the legend for viscosity
        float:    vSup:      superior value of the legend for viscosity
        str:      label:     label of the colorbar
        str:      facecolor: background color
        str:      cmap:      colormap

    Returns:
        (nothing)
    """
    plt.rcParams.update({'font.size': 14})
    #plt.axis('off')

#---------- Colour plot. ---------------------------------------------------------------------
    # Default colormaps: viridis, plasma, inferno, magma and CIVIDIS
    plt.pcolormesh(x1, x2, visco, vmin=vInf, vmax=vSup, cmap=cmap, shading='nearest',linewidth=0,rasterized=True)
    plt.colorbar(label=label)

    plt.tight_layout()

#---------- Saving on file. ------------------------------------------------------------------
    plt.savefig(file_out, format='pdf', facecolor=facecolor)



##############################################################################################
def testPlotPIV ( file_out, x, y, u, v, uLegend, centre, top, bottom, xmin, xmax ):          #
##############################################################################################
    """Test plot on PIV data to check whether min and max values are correct.
    
    Creates a figure file with:
    (1) centre point to be gauged against the centre of the bubble column;
    (2) bottom & top points to be gauged against bottom and top areas of the plot;
    (3) two vertical lines at the left and right borders of the plot to check that
        the chord width is built correctly.

    Args:
        str:      file_out: output filename
        np.array: x:        horizontal coordinate matrix
        np.array: y:        vertical coordinate matrix
        np.array: u:        velocity field, horizontal direction
        np.array: v:        velocity field, vertical direction
        float:    uLegend:  maximum velocity magnitude for colorbar
        np.array: centre:   x-y coords of the centre point
        np.array: top:      x-y coords of the top point
        np.array: bottom:   x-y coords of the bottom point
        float:    xmin:     x minimum value
        float:    xmax:     x maximum value

    Returns:
        (nothing)
    """

#---------- Colour plot. ---------------------------------------------------------------------
    # Default colormaps: viridis, plasma, inferno, magma and CIVIDIS
    plt.pcolormesh( x, y, np.sqrt(u**2 + v**2), vmin=0, vmax=uLegend, cmap='viridis' )

#---------- Line plots. ----------------------------------------------------------------------
    line1x = np.array([xmin, xmin])
    line2x = np.array([xmax, xmax])
    liney  = np.array([bottom[1], top[1]])

    plt.plot(line1x, liney, 'r')
    plt.plot(line2x, liney, 'r')

#---------- Point plots. ---------------------------------------------------------------------
    plt.plot(centre[0], centre[1], 'r+')
    plt.plot(top[0], top[1], 'g+')
    plt.plot(bottom[0], bottom[1], 'r+')

#---------- Saving on file. ------------------------------------------------------------------
    plt.savefig(file_out, format='png')



########### MAIN. ############################################################################
if __name__ == '__main__':                                                                   #
##############################################################################################

    help(plotPlane)
    help(testPlot)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
