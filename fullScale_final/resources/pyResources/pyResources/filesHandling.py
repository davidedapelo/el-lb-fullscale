import numpy as np
import sys
import subprocess as sp



##############################################################################################
def buildListOfNamesPIV( inputFolder, searchPath ):                                          #
##############################################################################################
    """Creates a list of filenames for the PIV experiments.

    Launches a 'ls' command inside the input folder with a star + path,
    and returns a list with the filenames.

    Args:
        str: inputfolder: input folder name
        str: searchPath:  search path - all the output entries must contain this string

    Returns:
        [str]: list of str, each indicating the name of a file contained in inputFolder
    """

#---------- Launching the ls command. --------------------------------------------------------
    # The output is a single, multi-line string, each indicating a file name.
    fn = sp.getoutput("ls "+ inputFolder + "/*" + searchPath)

#---------- Building the list. ---------------------------------------------------------------
    # Recursively splitting the fn string with newline character as the indicator.
    listOfNames = []
    while not fn == '':
        item, sep, fn = fn.partition('\n')
        listOfNames.append(item)

#---------- Returning. -----------------------------------------------------------------------
    return listOfNames



##############################################################################################
def buildListOfNamesLB( inputFolder, searchPath1, searchPath2 ):                             #
##############################################################################################
    """Creates a list of LB filenames with a timestep larger than a given one.

    Launches a 'ls' command inside the input folder with a star + path,
    and returns a list with the filenames.

    Args:
        str: inputfolder: input folder name
        str: searchPath1: 1st search path - all the output entries must contain this string
        str: searchPath2: 2nd search path - all the output entries must contain this string

    Returns:
        [str]: list of str, each indicating the name of a file contained in inputFolder
    """

#---------- Reading minimum timestep. --------------------------------------------------------
    # The minimum timestep (IN LATTICE UNITS) is read from a specific file
    # inside the input folder as a string.
    # Then, a fictious blank line is removed from the string variable.
    with open(inputFolder+'/0minTimestep.txt', 'r') as f:
        minTimestep = f.read()
    minTimestep, a, b = minTimestep.partition('\n')

#---------- Launching the ls command. --------------------------------------------------------
    # The output is a single, multi-line string, each indicating a file name.
    fn = sp.getoutput(
            "for F in `ls "+ inputFolder + "/*" + searchPath1 + "*" + searchPath2 + "*`;do"
            + " if [ `basename $F|cut -d'_' -f4` -gt " + minTimestep + " ]; then"
            + " echo $F;"
            + "fi;"
            + "done"
            )

#---------- Building the list. ---------------------------------------------------------------
    # Recursively splitting the fn string with newline character as the indicator.
    listOfNames = []
    while not fn == '':
        item, sep, fn = fn.partition('\n')
        listOfNames.append(item)

#---------- Returning. -----------------------------------------------------------------------
    return listOfNames



##############################################################################################
def checkArgs (legalArgvs):                                                                  #
##############################################################################################
    """Printing argv and checking they are as many as required. Quitting otherwise.

    Args:
        list[int]: legalArgvs: list containing all the legal numbers of argv's.
    """

#---------- Printing all the argv's. ---------------------------------------------------------
    print("argv=" + str(len(sys.argv)))
    for i in list(range(len(sys.argv))):
        print( "argv[" + str(i) + "]=" + str(sys.argv[i]) )
    print("")

#---------- token is True if the number of argv's does not match with any of the legalArgvs. -
    token=True
    for i in legalArgvs:
        if len(sys.argv) == i:
            token=False
            return

#---------- Quitting if token is True. -------------------------------------------------------
    raise TypeError('gridDep_risingVel.py: ' + str(len(sys.argv)) + ' arguments passed; ' + str(legalArgvs) + ' allowed. Quitting.')



##############################################################################################
def readDictFile ( name_in ):                                                                #
##############################################################################################
    """Returns a dictionary variable from a file.

    Input file is structured as:
    key1 float1
    key2 float2
    ...  ...

    Args:
        str: name_in: filename

    Returns:
        dict: d: dictionary
    """

#---------- Preliminary operations. ----------------------------------------------------------
    # Output dictionary variable initialized as empty
    d = dict()

    # Open input file
    f = open(name_in, 'r')

#---------- Scrolling the input file. --------------------------------------------------------
    for line in f:
        d[line.split()[0]] = float(line.split()[1])

#---------- Closing & returning. -------------------------------------------------------------
    f.close()
    return d



########### MAIN. ############################################################################
if __name__ == '__main__':                                                                   #
##############################################################################################

    help(buildListOfNamesPIV)
    help(buildListOfNamesLB)
    help(checkArgs)
    help(readDictFile)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
