import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from numpy.polynomial import Polynomial as P

plt.switch_backend('agg') # Added to solve issues on HPC



##############################################################################################
def average( outFilename, inFilenames ):                                                     #
##############################################################################################
    """Building time-averaged field.

    Read the field values over time from a list of files and compute the time average.

    Args:
        str:   outFilename: output filename
        [str]: inFilenames: list containing the filenames of the single timesteps.
    Returns:
        (nothing)
    """

#---------- Adding the timesteps. ------------------------------------------------------------
    # Scrolling list of filenames to build up averaged field.
    for item in inFilenames:
        x = np.genfromtxt(item)

        # If variable containing averaged field has not been defined yet:
        if 'xx' in locals():
            xx += x

        # If variable containing averaged field has been defined previously:
        else:
            xx = x

#---------- Averaging & reshaping. -----------------------------------------------------------
    # Dividing field by number of time entries
    xx /= len(inFilenames)

#---------- Saving on file. ------------------------------------------------------------------
    np.savetxt(outFilename, xx)



##############################################################################################
def averageAndSave( outFilename, inFilenames ):                                              #
##############################################################################################
    """Building and rearranging time-averaged field.

    Read the field values over time from a list of files, compute the time average,
    reshape it on a square xy matrix and save to file.

    Args:
        str:   outFilename: output filename
        [str]: inFilenames: list containing the filenames of the single timesteps.
                            Each file contains the data of a square matrix, rearranged
                            on a single column:
                            f(1, 1)
                            f(2, 1)
                            ...
                            f(nx,1)
                            f(1, 2)
                            f(2, 2)
                            ...
                            f(nx,2)
                            ...
                            f(1 ,ny)
                            f(2 ,ny)
                            ...
                            f(nx,ny)

    Returns:
        (nothing)
    """

#---------- Adding the timesteps. ------------------------------------------------------------
    # Scrolling list of filenames to build up averaged field.
    for item in inFilenames:
        x = np.genfromtxt(item)

        # If variable containing averaged field has not been defined yet:
        if 'xx' in locals():
            xx += x

        # If variable containing averaged field has been defined previously:
        else:
            xx = x

#---------- Averaging & reshaping. -----------------------------------------------------------
    # Dividing field by number of time entries
    xx /= len(inFilenames)

    #Reshaping field into a square matrix
    xx = xx.reshape( int(np.sqrt(xx.size)) , int(np.sqrt(xx.size)) )

#---------- Saving on file. ------------------------------------------------------------------
    np.savetxt(outFilename, xx)



##############################################################################################
def computeAvgVelocitiesLB ( x1, x2, u, xMin, xMax, yMin, yMax ):                            #
##############################################################################################
    """Computes average velocity magnitude over the whole domain and a given selection.

    Args:
        np.array: x1:   x coordinate field
        np.array: x2:   y coordinate field
        np.array: u:    velocity magnitude field
        float:    xMin: minimum x coordinate of the selection
        float:    xMax: maximum x coordinate of the selection
        float:    yMin: minimum y coordinate of the selection
        float:    yMax: maximum y coordinate of the selection

    Returns:
        float: uAvgTot:  Average velocity magnitude over the whole domain
        float: uAvgPart: Average velocity magnitude over the selection
    """

#---------- Cutting. -------------------------------------------------------------------------
    # Finding matrix indices corresponding to min and max values.
    iXmin = ( (np.where(x1[0,:]<=xMin))[0] )[-1]
    iXmax = ( (np.where(x1[0,:]>=xMax))[0] )[ 0]
    iYmin = ( (np.where(x2[:,0]<=yMin))[0] )[-1]
    iYmax = ( (np.where(x2[:,0]>=yMax))[0] )[ 0]

    # Performig the cuts.
    x1C = x1[iYmin:iYmax, iXmin:iXmax]
    x2C = x2[iYmin:iYmax, iXmin:iXmax]
    uC  =  u[iYmin:iYmax, iXmin:iXmax]

#---------- Averaging. -----------------------------------------------------------------------
    uAvgTot =  u.mean()
    uAvgPart = uC.mean()

#---------- Returning. -----------------------------------------------------------------------

    return uAvgTot, uAvgPart



##############################################################################################
def computeVortexLB ( x1, x2, u, xMin, xMax, yMin, yMax ):                                   #
##############################################################################################
    """Finds the coordinates of the vortex centre within a given selection for LB simulation.

    The vortex is localized at the minimum value of the velocity magnitude field.

    Args:
        np.array: x1:   x coordinate field
        np.array: x2:   y coordinate field
        np.array: u:    velocity magnitude field
        float:    xMin: minimum x coordinate of the selection
        float:    xMax: maximum x coordinate of the selection
        float:    yMin: minimum y coordinate of the selection
        float:    yMax: maximum y coordinate of the selection

    Returns:
        float: uMinX: x coordinate of the vortex centre
        float: uMinY: y coordinate of the vortex centre
    """

#---------- Cutting. -------------------------------------------------------------------------
    # Finding matrix indices corresponding to min and max values.
    iXmin = ( (np.where(x1[0,:]<=xMin))[0] )[-1]
    iXmax = ( (np.where(x1[0,:]>=xMax))[0] )[ 0]
    iYmin = ( (np.where(x2[:,0]<=yMin))[0] )[-1]
    iYmax = ( (np.where(x2[:,0]>=yMax))[0] )[ 0]

    # Performig the cuts.
    x1C = x1[iYmin:iYmax, iXmin:iXmax]
    x2C = x2[iYmin:iYmax, iXmin:iXmax]
    uC  =  u[iYmin:iYmax, iXmin:iXmax]

#---------- Finding the voxel with minimum velocity magnitude. -------------------------------
    # (i,j) indices of the minimum value of velocity magnitude
    uMinJ, uMinI = np.unravel_index( uC.argmin(), uC.shape )

    # From (i,j) to (x,y)
    X0 = x1C[0, uMinI]
    Y0 = x2C[uMinJ, 0]

#           Interpolating the vortex coordinates with 2nd order polynomy
#---------- passing through 1st neighbours of vortex with minimum value of velocity field. ---
    # Velocity magnitude's value at minimum voxel and 1st neighbour coordinates.
    u0  = uC[uMinJ, uMinI]
    uXP = uC[uMinJ, uMinI+1]
    uXM = uC[uMinJ, uMinI-1]
    uYP = uC[uMinJ+1, uMinI]
    uYM = uC[uMinJ-1, uMinI]

    # 1st neighbour coordinates.
    XM = x1C[0, uMinI-1]
    XP = x1C[0, uMinI+1]
    YM = x2C[uMinJ-1, 0]
    YP = x2C[uMinJ+1, 0]

    # Finding the coefficients a, b, c of the 2nd order polynomial obeying:
    # u0  = aX * X0^2 + bX * X0 + cX
    # uXP = aX * XP^2 + bX * XP + cX
    # uXM = aX * XM^2 + bX * XM + cX
    DeltaX  = X0**2 * (XP  -  XM)  +  XP**2 * (XM  - X0)  +  XM**2 * (X0 -  XP)
    DeltaXA = u0    * (XP  -  XM)  +  uXP   * (XM  - X0)  +  uXM   * (X0 -  XP)
    DeltaXB = X0**2 * (uXP - uXM)  +  XP**2 * (uXM - u0)  +  XM**2 * (u0 - uXP)

    aX = DeltaXA / DeltaX
    bX = DeltaXB / DeltaX

    # Finding the coefficients a, b, c of the 2nd order polynomial obeying:
    # u0  = aY * Y0^2 + bY * Y0 + cY
    # uYP = aY * YP^2 + bY * YP + cY
    # uYM = aY * YM^2 + bY * YM + cY
    DeltaY  = Y0**2 * (YP  -  YM)  +  YP**2 * (YM  - Y0)  +  YM**2 * (Y0 -  YP)
    DeltaYA = u0    * (YP  -  YM)  +  uYP   * (YM  - Y0)  +  uYM   * (Y0 -  YP)
    DeltaYB = Y0**2 * (uYP - uYM)  +  YP**2 * (uYM - u0)  +  YM**2 * (u0 - uYP)

    aY = DeltaYA / DeltaY
    bY = DeltaYB / DeltaY

    # The desired value is the local minimim of the 2nd degree polynomial,
    # viz., where the polynomial's 1st derivative is zero,
    # viz, 0 = 2 * aX * XX + bX
    # viz, 0 = 2 * aY * YY + bY
    XX = -bX / (2. * aX)
    YY = -bY / (2. * aY)

#---------- Returning. -----------------------------------------------------------------------
    return XX, YY



##############################################################################################
def computeVortexPIV ( x1, x2, u, xMin, xMax, yMin, yMax ):                                   #
##############################################################################################
    """Finds the coordinates of the vortex centre within a given selection for PIV experiment.

    The vortex is localized at the minimum value of the velocity magnitude field.

    Args:
        np.array: x1:   x coordinate field
        np.array: x2:   y coordinate field
        np.array: u:    velocity magnitude field
        float:    xMin: minimum x coordinate of the selection
        float:    xMax: maximum x coordinate of the selection
        float:    yMin: minimum y coordinate of the selection
        float:    yMax: maximum y coordinate of the selection

    Returns:
        float: uMinX: x coordinate of the vortex centre
        float: uMinY: y coordinate of the vortex centre
    """

#---------- Cutting. -------------------------------------------------------------------------
    # Finding matrix indices corresponding to min and max values.
    iXmin = ( (np.where(x1[0,:]<=xMin))[0] )[-1]
    iXmax = ( (np.where(x1[0,:]>=xMax))[0] )[ 0]
    iYmin = ( (np.where(x2[:,0]<=yMin))[0] )[ 0]
    iYmax = ( (np.where(x2[:,0]>=yMax))[0] )[-1]

    # Performig the cuts.
    x1C = x1[iYmax:iYmin, iXmin:iXmax]
    x2C = x2[iYmax:iYmin, iXmin:iXmax]
    uC  =  u[iYmax:iYmin, iXmin:iXmax]

#---------- Finding the vortex coordinates. --------------------------------------------------
    # (i,j) indices of the minimum value of velocity magnitude
    uMinJ, uMinI = np.unravel_index( uC.argmin(), uC.shape )

    # From (i,j) to (x,y)
    uMinX = x1C[0,uMinI]
    uMinY = x2C[uMinJ,0]
    
#---------- Returning. -----------------------------------------------------------------------
    return uMinX, uMinY



##############################################################################################
def computeAvgVelocitiesPIV ( x1, x2, u, xMin, xMax, yMin, yMax ):                            #
##############################################################################################
    """Computes average velocity magnitude over the whole domain and a given selection

    Args:
        np.array: x1:   x coordinate field
        np.array: x2:   y coordinate field
        np.array: u:    velocity magnitude field
        float:    xMin: minimum x coordinate of the selection
        float:    xMax: maximum x coordinate of the selection
        float:    yMin: minimum y coordinate of the selection
        float:    yMax: maximum y coordinate of the selection

    Returns:
        float: uAvgTot:  Average velocity magnitude over the whole domain
        float: uAvgPart: Average velocity magnitude over the selection
    """

#---------- Cutting. -------------------------------------------------------------------------
    # Finding matrix indices corresponding to min and max values.
    iXmin = ( (np.where(x1[0,:]<=xMin))[0] )[-1]
    iXmax = ( (np.where(x1[0,:]>=xMax))[0] )[ 0]
    iYmin = ( (np.where(x2[:,0]<=yMin))[0] )[-1]
    iYmax = ( (np.where(x2[:,0]>=yMax))[0] )[ 0]

    # Performig the cuts.
    x1C = x1[iYmax:iYmin, iXmin:iXmax]
    x2C = x2[iYmax:iYmin, iXmin:iXmax]
    uC  =  u[iYmax:iYmin, iXmin:iXmax]

#---------- Averaging. -----------------------------------------------------------------------
    uAvgTot =  u.mean()
    uAvgPart = uC.mean()

#---------- Returning. -----------------------------------------------------------------------

    return uAvgTot, uAvgPart



##############################################################################################
def correct ( outFile, inFile ):                                                             #
##############################################################################################
    """Corrects a matrix data file.

    The rigthmost column of an input file is cut and pasted on the left of the matrix,
    and then saved into another file.

    Args:
        str: outFile: output data filename
        str: itFile:  intput data filename

    Returns:
        (nothing)
    """

    x = np.genfromtxt(inFile)

    a = x[:, -1]
    a = a.reshape(int(a.size), 1)
    b = x[:,0:-1]
    c = np.concatenate( (a, b), 1 )

    #a = x[:, 0]
    #a = a.reshape(int(a.size), 1)
    #b = x[:,1:]
    #c = np.concatenate( (b, a), 1 )

#---------- Saving on file. ------------------------------------------------------------------
    np.savetxt(outFile, c)



##############################################################################################
def createPEPTfields ( x, y, z, t, d ):                                                      #
##############################################################################################
    """Creates radial velocity fields from a trajectory.
    First, the trajectory's tangential velocity is computed through a central (2nd-ordered)
    differentiation scheme. Then, a rectangular grid is filled up with the velocity values
    as the tra,ectory passes through the single grid locations.

    Args:
        np.array: x: Sequence of x values through the trajectory
        np.array: y: Sequence of y values through the trajectory
        np.array: z: Sequence of z values through the trajectory
        np.array: t: Sequence of t values through the trajectory
        dict:     d: Dictionary with the parameters for the given dataset.

    Returns:
        np.array: xx:   Field for x coordinate
        np.array: yy:   Field for y coordinate
        np.array: Ur:   Field for radial-component velocity
        np.array: Uy:   Field for vertical-component velocity
        np.array: UMag: Field for velocity magnitude
    """

#---------- Definitions/initializationa. -----------------------------------------------------
    # Parameters derived from dictionary
    R     = d['R'] / 1000.
    H     = ( float(d['yMax']) - float(d['yMin']) )/1000.
    Hcorr = H*( 1. + d['elevRatio'] )

    # Fields initialization
    P    = np.zeros(( d['Ny'], d['Nr'] )) # No. of times the trajectory passes through a location
    Ur   = np.zeros(( d['Ny'], d['Nr'] ))
    Uy   = np.zeros(( d['Ny'], d['Nr'] ))
    Umag = np.zeros(( d['Ny'], d['Nr'] ))

    yy, xx = np.mgrid [ 0 : Hcorr  : Hcorr / d['Ny'],
                        0 : R      : R     / d['Nr']]

#---------- Central differencing. ------------------------------------------------------------
    # Finite (2nd-ordered) differences in time and space coordinates
    deltaT = np.concatenate(( t, np.array([0., 0.]) )) - np.concatenate(( np.array([0., 0.]), t ))
    deltaT = np.delete(deltaT, -1)
    deltaT = np.delete(deltaT, -1)
    deltaT = np.delete(deltaT,  0)
    deltaT = np.delete(deltaT,  0)

    deltaX = np.concatenate(( x, np.array([0., 0.]) )) - np.concatenate(( np.array([0., 0.]), x ))
    deltaX = np.delete(deltaX, -1)
    deltaX = np.delete(deltaX, -1)
    deltaX = np.delete(deltaX,  0)
    deltaX = np.delete(deltaX,  0)

    deltaY = np.concatenate(( y, np.array([0., 0.]) )) - np.concatenate(( np.array([0., 0.]), y ))
    deltaY = np.delete(deltaY, -1)
    deltaY = np.delete(deltaY, -1)
    deltaY = np.delete(deltaY,  0)
    deltaY = np.delete(deltaY,  0)

    deltaZ = np.concatenate(( z, np.array([0., 0.]) )) - np.concatenate(( np.array([0., 0.]), z ))
    deltaZ = np.delete(deltaZ, -1)
    deltaZ = np.delete(deltaZ, -1)
    deltaZ = np.delete(deltaZ,  0)
    deltaZ = np.delete(deltaZ,  0)

    # Cutting off initial and final trajectory points
    # (you lose two points due to 2nd-ordered scheme)
    t = np.delete(t, -1)
    x = np.delete(x, -1)
    y = np.delete(y, -1)
    z = np.delete(z, -1)

    t = np.delete(t,  0)
    x = np.delete(x,  0)
    y = np.delete(y,  0)
    z = np.delete(z,  0)

    # Velocity components at each point of the trajectory
    ux = deltaX / deltaT
    uy = deltaY / deltaT
    uz = deltaZ / deltaT

#---------- Radial coordinates. --------------------------------------------------------------
    r = np.sqrt( x**2 + z**2 )

    ur = ( x*ux + z*uz ) / r
    v  = np.sqrt( ur**2 + uy**2 )

#---------- Building up fields from trajectory. ----------------------------------------------
    # Scrolling the trajectory and placing every velocity entry
    # inside an Nr x Ny grid. Increasing the occupancy of corresponding grid locations by 1.
    for n in range(0, t.shape[0]):
        ir = int( np.fix( r[n]/R*d['Nr'] - d['tolerance']*d['R']/d['Nr'] ) )
        iy = int( np.fix( y[n]/Hcorr*d['Ny']  - d['tolerance']*Hcorr/d['Ny']  ) )

        P   [iy, ir] = P   [iy, ir] + 1
        Ur  [iy, ir] = Ur  [iy, ir] + ur[n]
        Uy  [iy, ir] = Uy  [iy, ir] + uy[n]
        Umag[iy, ir] = Umag[iy, ir] + v[n]

    # Normalizing the field values based on local occupancy values
    Umag[P>0] = Umag[P>0] / P[P>0]
    Ur  [P>0] = Ur  [P>0] / P[P>0]
    Uy  [P>0] = Uy  [P>0] / P[P>0]

    # If the trajectory did not pass through a given location,
    # corresponding field values are nan
    Umag[P==0] = float('NaN')
    Ur  [P==0] = float('NaN')
    Uy  [P==0] = float('NaN')

    # Mirroring through y axis
    xx   = np.concatenate( (-np.fliplr(xx),   xx  ), axis=1 )
    yy   = np.concatenate( ( np.fliplr(yy),   yy  ), axis=1 )
    Ur   = np.concatenate( (-np.fliplr(Ur),   Ur  ), axis=1 )
    Uy   = np.concatenate( ( np.fliplr(Uy),   Uy  ), axis=1 )
    Umag = np.concatenate( ( np.fliplr(Umag), Umag), axis=1 )

#---------- Returning. -----------------------------------------------------------------------
    return xx, yy, Ur, Uy, Umag



##############################################################################################
def createPEPTtrajectory ( fileName, d ):                                                    #
##############################################################################################
    """Creates a trajectory from data file and dictionary.
    The trajectory is represented as a sequence of space-time point P1, P2, ... Pn,
    where each point is represented by coordinates and time x, y, z, t.

    Args:
        str:  filename: Data file name
        dict: d:        Dictionary with the parameters for the given dataset.

    Returns:
        np.array: x: Sequence of x values
        np.array: y: Sequence of y values
        np.array: z: Sequence of z values
        np.array: t: Sequence of t values
    """

#---------- Reading raw data matrix. ---------------------------------------------------------
    data = np.genfromtxt(fileName)

#---------- Elaboration. ---------------------------------------------------------------------
    # Reading from raw data & centering.
    t = data[:,0]
    x = data[:,1] - d['centreX']
    y = data[:,2] - d['yMin']
    z = data[:,3] - d['centreZ']

    # Cutting between min and max time intervals.
    x = x[t >= d['tMin']]
    y = y[t >= d['tMin']]
    z = z[t >= d['tMin']]
    t = t[t >= d['tMin']]

    x = x[t <= d['tMax']]
    y = y[t <= d['tMax']]
    z = z[t <= d['tMax']]
    t = t[t <= d['tMax']]

    # From ms-mm to s-m.
    x /= 1000.
    y /= 1000.
    z /= 1000.
    t /= 1000.

#---------- Returning. -----------------------------------------------------------------------
    return x, y, z, t



##############################################################################################
def cutFieldsPIV ( xx, yy, u, v, xmin, xmax, ymin, ymax, centre ):                           #
##############################################################################################
    """Fits PIV coordinate and velocity fields to min and max values and resize.

    Args:
        np.array: xx:       horizontal coordinate matrix
        np.array: yy:       vertical coordinate matrix
        np.array: u:        velocity field, horizontal direction
        np.array: v:        velocity field, vertical direction
        float:    xmin:     x minimum value
        float:    xmax:     x maximum value
        float:    ymin:     y minimum value
        float:    ymax:     y maximum value
        float:    centre:   centre

    Returns:
        np.array: xxC:      horizontal coordinate matrix
        np.array: yyC:      vertical coordinate matrix
        np.array: uC:       velocity field, horizontal direction
        np.array: vC:       velocity field, vertical direction
    """

#---------- Cutting. -------------------------------------------------------------------------
    # Finding matrix indices corresponding to min and max values.
    iXmin = ( (np.where(xx[0,:]<=xmin))[0] )[-1]
    iXmax = ( (np.where(xx[0,:]>=xmax))[0] )[ 0]
    iYmin = ( (np.where(yy[:,0]<=ymin))[0] )[ 0]
    iYmax = ( (np.where(yy[:,0]>=ymax))[0] )[-1]

    # Performig the cuts.
    xxC = xx[iYmax+1:iYmin+1, iXmin+1:iXmax]
    yyC = yy[iYmax+1:iYmin+1, iXmin+1:iXmax]
    uC  =  u[iYmax+1:iYmin+1, iXmin+1:iXmax]
    vC  =  v[iYmax+1:iYmin+1, iXmin+1:iXmax]

#---------- Resizing (from mm to m). ---------------------------------------------------------
    xxC = (xxC - centre)/1000
    yyC = (yyC - yyC[-1,-1])/1000

#---------- Returning. -----------------------------------------------------------------------
    return xxC, yyC, uC, vC



##############################################################################################
def elabGridDependence(inDir, parity, **kwargs):
##############################################################################################
    '''Extract data for grid indpendence tests.

    Args:
        string:  inDir:   input folder
        int:     parity:  0: all data; -1: odd only; 1: even only

        kwargs
            float:  dExt:    diameter of the computational domain
            float:  dBubble: diameter of biogas bubbles

    Returns:
        dict:        dataGlobal:                      dictionary of list[float] containing invariant elaborated data:
                     dataGlobal['resolution']:        resolutions (from data file names)
                     dataGlobal['deltaXoverDBubble']: delta x over bubble size
        list[float]: xCoord:                          first specific data (from data file content)
        list[float]: yCoord:                          second specific data (from data file content)
    '''

    # basenames is a string array for file access; resolution is a float array for x axis.
    basenames = [ item.split('.',1)[0] for item in os.listdir(inDir) ] # reading from input folder
    basenames.sort(key=float) # sorting in increasing order
    basenames = basenames[::2] if parity==1 else ( basenames[1::2] if parity==-1 else basenames ) # applying parity

    dataGlobal = {}
    dataGlobal['resolution'] = [ float(item) for item in basenames]
    dataGlobal['latticeSize'] = [ kwargs['dExt']/item for item in dataGlobal['resolution']]
    if 'dExt' in kwargs and 'dBubble' in kwargs:
        dataGlobal['deltaXoverDBubble'] = [truncate( (kwargs['dExt']/item)/kwargs['dBubble'] , 3 ) for item in dataGlobal['resolution']]
        dataLen = len(dataGlobal['deltaXoverDBubble'])
        for i in np.arange(dataLen-1, -1, -1):
            if not (i % (dataLen // 4) == 0):
                dataGlobal['deltaXoverDBubble'][i] = ""

    xyCoord = [ open(inDir+'/'+item+'.txt', 'r').readline().rstrip() for item in basenames ]
    xCoord  = [ float(item.split(' ',1)[0]) for item in xyCoord ]
    yCoord  = [ float(item.split(' ',1)[1]) for item in xyCoord ]

    return dataGlobal, xCoord, yCoord



##############################################################################################
def fitGridDependence(dataGlobal, y, outFile):
##############################################################################################
    '''Polynomial fit for grid independence tests: y = p(dataGlobal['latticeSize']

    Args:
        dict:        dataGlobal: dictionary of list[float] containing invariant elaborated data:
        list[float]: y:          numerical data to be fitted
        str:         outFile:    output file for polynomial coefficients

    Returns:
        list[float]: fitted data
        list[float]: polynomial coeffs [a_0, ..., a_n]
    '''
    p = P.fit( dataGlobal['latticeSize'], \
                             y, \
                             dataGlobal['polyOrder'], \
                             window=[ min(dataGlobal['latticeSize']), max(dataGlobal['latticeSize']) ] \
                           )
    with open(outFile, 'w') as f:
        f.write( str(p.coef ) )

    return p(np.asarray(dataGlobal['latticeSize'])), p.coef



##############################################################################################
def truncate(f, n):                                                                          #
##############################################################################################
    '''Truncates/pads a float f to n decimal places without rounding.

    Args:
        float: f: float to be truncated
        int:   n: number of integers

    Returns:
        float: truncated float
    '''

    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])



########### MAIN. ############################################################################
if __name__ == '__main__':                                                                   #
##############################################################################################

    help(average)
    help(averageAndSave)
    help(computeAvgVelocitiesLB)
    help(computeAvgVelocitiesPIV)
    help(computeVortexLB)
    help(computeVortexPIV)
    help(correct)
    help(createPEPTfields)
    help(createPEPTtrajectory)
    help(cutFieldsPIV)
    help(elaborateGridDependence)
    help(truncate)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
