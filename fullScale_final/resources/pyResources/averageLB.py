import subprocess as sp
import sys
import numpy as np

import pyResources.plotting as pt
import pyResources.dataElaboration as de
import pyResources.filesHandling as fh

sys.stdout = open('pyOut_averageLB.txt', 'a+') # Redirecting stdout
sys.stderr = open('pyErr_averageLB.txt', 'a+') # Redirecting stderr



##############################################################################################

if len(sys.argv) == 5:
    dirname    = str(sys.argv[1])
    searchPath = str(sys.argv[2])
    searchChar = str(sys.argv[3])
    inName     = str(sys.argv[4])

    de.average( inName, fh.buildListOfNamesLB(dirname, searchPath, searchChar) )

else:
    if len(sys.argv) == 3:
        dirname    = str(sys.argv[1])
        searchPath = str(sys.argv[2])
    elif len(sys.argv) == 2:
        dirname    = str(sys.argv[1])
        searchPath = 'velY_0.029041'
    else:
        dirname    ='../../git_ignore/data_rawData'
        searchPath = 'velY_0.029041'

    for item in ['x',  'z']:
        de.average( dirname+'/0'+item+'.txt', fh.buildListOfNamesLB(dirname, searchPath, item) )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
