import sys
import numpy as np
import matplotlib.pyplot as plt

import pyResources.plotting as pt
import pyResources.dataElaboration as de
import pyResources.filesHandling as fh



##############################################################################################

fileOut   = '../../git_ignore/0expDataPEPT/figures/4s_40.png'
fileData1 = '../../git_ignore/0expDataPEPT/data/a01.txt'
fileData2 = '../../git_ignore/0expDataPEPT/data/aa05.txt'
fileDict1 = '../../git_ignore/0expDataPEPT/dictionaries/a01.txt'
fileDict2 = '../../git_ignore/0expDataPEPT/dictionaries/aa05.txt'

d1 = fh.readDictFile(fileDict1)
for key in d1:
    d1[key] = float(d1[key])
d1['Nr'] = int(d1['Nr'])
d1['Ny'] = int(d1['Ny'])

d2 = fh.readDictFile(fileDict2)
for key in d2:
    d2[key] = float(d2[key])
d2['Nr'] = int(d2['Nr'])
d2['Ny'] = int(d2['Ny'])

x1, y1, z1, t1 = de.createPEPTtrajectory (fileData1, d1)
x2, y2, z2, t2 = de.createPEPTtrajectory (fileData2, d2)

x = np.concatenate((x1, x2))
y = np.concatenate((y1, y2))
z = np.concatenate((z1, z2))
t = np.concatenate((t1, t2))

xx, yy, Ur, Uy, Umag = de.createPEPTfields( x, y, z, t, d1 )

pt.plotPlane(fileOut, xx, yy, Ur, Uy, Umag, d1['uLegend'])



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
