import sys
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

import pyResources.dataElaboration as de
import pyResources.filesHandling as fh
import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC
sys.stdout = open('pyOut_gridDep_vortexCoords.txt', 'a+') # Redirecting stdout
sys.stderr = open('pyErr_gridDep_vortexCoords.txt', 'a+') # Redirecting stderr


##############################################################################################

#---------- Checking the number of arguments. ------------------------------------------------
fh.checkArgs([6, 7])

#---------- input data: common. --------------------------------------------------------------
xmlRoot = ET.parse( str(sys.argv[1]) ).getroot() # parsing input.xml
io = {}
io['inDir']         = str(sys.argv[2]) # Input folder containing x-y data files
io['outDir']        = str(sys.argv[3]) # Output folder where to create the pictures
io['outPrefixFile'] = str(sys.argv[4]) # Output picture filename
io['parity']        = int(sys.argv[5]) # 0: all data; -1: odd only; 1: even only
io['polyOrder']     = int(sys.argv[6]) if len(sys.argv) == 7 else 0 # Butterworth low-pass cutting frequency: default value=0 (viz., do not filter)

inData = {}
inData['printBubbleOverLattice'] = bool(float( xmlRoot.find('simul').find('printBubbleOverLattice').text )) # print bubble-to-lattice-size ratio?
inData['dBubble']   = float( xmlRoot.find('data').find('dynamics').find('d').text ) * 1e-3 # bubble's diameter
inData['dExt']      = float( xmlRoot.find('data').find('geometry').find('dExt').text ) #External cylinder diameter
geoType = xmlRoot.find('simul').find('geoType').text

parity_prefix = 'E' if io['parity']==1 else ( 'O' if io['parity']==-1 else 'A' )
outFileX      = io['outDir']+'/' + io['outPrefixFile'] + ('F' + str(io['polyOrder']) if len(sys.argv) == 7 else '') + '_x_' + parity_prefix + '.pdf'
outFileY      = io['outDir']+'/' + io['outPrefixFile'] + ('F' + str(io['polyOrder']) if len(sys.argv) == 7 else '') + '_y_' + parity_prefix + '.pdf'
outFileXdelta = io['outDir']+'/' + io['outPrefixFile'] + '_deltaX_F' + str(io['polyOrder']) +'_x_'+ parity_prefix + '.pdf' if len(sys.argv) == 7 else ''

#---------- input data: specific. ------------------------------------------------------------
if io['outPrefixFile'] == 'vortexCoords_': # vortexCoords_
    titleX = "Vortex's x coordinate (m)"
    titleY = "Vortex's y coordinate (m)"
    if geoType == ' lab ':
        inData['xExpCoord'] = float( xmlRoot.find('data').find('dynamics').find('vortexX').text ) # Experimental vortex's x position
        inData['yExpCoord'] = float( xmlRoot.find('data').find('dynamics').find('vortexY').text ) # Experimental vortex's y position
    if len(sys.argv) == 7:
        titleXdelta = "Vortex's x coordinate: Delta (m)"

elif io['outPrefixFile'] == 'risingVel_particles_': # risingVel_particles_
    title = "Particle rising velocity (m/s)"

elif io['outPrefixFile'] == 'risingVel_fluid_': # risingVel_fluid_
    title = "Fluid rising velocity (m/s)"

else:
    raise ValueError('outPrefixFile=' + io['outPrefixFile'] + ' but only vortexCoords_, risingVel_particles_, risingVel_fluid_ are allowed.')

#---------- elaborating the data. ------------------------------------------------------------
if inData['printBubbleOverLattice']:
#if geoType == ' pilot ':
    dataGlobal, data1, data2 = de.elabGridDependence(io['inDir'], io['parity'], dExt=inData['dExt'], dBubble=inData['dBubble']);
else:
    dataGlobal, data1, data2 = de.elabGridDependence(io['inDir'], io['parity'], dExt=inData['dExt']);
if len(sys.argv) == 7:
    dataGlobal['polyOrder'] = io['polyOrder']
    dataGlobal['dataFitted'], dataGlobal['fitCoeffs'] = de.fitGridDependence ( \
                                                       dataGlobal, \
                                                       data1, \
                                                       io['outDir'] + '/../' + io['outPrefixFile'] + 'fitCoeffs' + str(io['polyOrder']) + '.txt' )
else:
    dataGlobal['dataFitted'] = []

#---------- initializing the figure. ---------------------------------------------------------
plt.rcParams.update({'font.size': 14})
fig = plt.figure() # create a figure object

#---------- Printing. ------------------------------------------------------------------------
if io['outPrefixFile'] == 'vortexCoords_': # vortexCoords_
    pt.plotGrideDep_resolution ( fig, outFileX, titleX, dataGlobal['resolution'], data1, \
                                 yExp = inData['xExpCoord'] if geoType==' lab ' else None, \
                                 yFit = dataGlobal['dataFitted'] if len(sys.argv)==7 else [] \
                               )
    pt.plotGrideDep_resolution ( fig, outFileY, titleY, dataGlobal['resolution'], data2, \
                                 yExp = inData['xExpCoord'] if geoType==' lab ' else None, \
                               )
    if len(sys.argv) == 7:
        pt.plotGrideDep_deltaX(fig, outFileXdelta, titleXdelta, dataGlobal, data1)
else: # all the others
    pt.plotGrideDep_resolution(fig, outFileY, title, dataGlobal['resolution'], data2)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
