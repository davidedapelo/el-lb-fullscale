import os
import sys

import code.inOut as inOut
from code.dragModels import dragMorsi as drag
import code.bases.d_g_rhoL.d_g_rhoL as conv



########### MAIN. ############################################################################
option1 = '(OPTION 1): Input data ---> Dimensionless numbers:'
option2 = '(OPTION 2): Dimensionless numbers & Starting-guess input data ---{ Scaling recurring variable [optional] }---> Input data'
option3 = '(OPTION 3): Dimensionless numbers & Starting-guess input data ---{ Scaling recurring variable & Scaling dimensionless number }---> Input data'
option4 = '(OPTION 4): Dimensionless numbers & Starting-guess input data ---{ Scaling recurring variable & Scaling dimensionless number\n' \
        + '                                  & Alterable dimensionless number & Fixed other dimensionless number}---> Input data'
guideString = \
        sys.argv[0] + ' <target_dir>\n' \
        + 'Apply Buckingham-Pi theorem to process input/calculated data, dimensionless numbers and other dimensionless numbers of a physical system.\n\n' \
        + 'Input data (== physical quantities): ' + str(conv.Conv_d_g_rhoL().listIn) + '\n' \
        + 'Calculated data (== physical quantities, but not within Buckingham-Pi computing): ' + str(conv.Conv_d_g_rhoL().listCa) + '\n' \
        + 'Numbers (== dimensionless numbers): ' + str(conv.Conv_d_g_rhoL().listNu) + '\n' \
        + 'Other numbers (== dimensionless numbers, but not within Buckingham-Pi computing): ' + str(conv.Conv_d_g_rhoL().listCa) + '\n\n' \
        + '<target_dir> (dir)\n' \
        + '    |\n' \
        + '    |___________ input (dir)\n'  \
        + '    |               |\n' \
        + '    |               |___________ inputData.txt\n'  \
        + '    |               |___________ numbers.txt [optional]\n'  \
        + '    |\n' \
        + '    |___________ output (dir) [authomatically created]\n' \
        + '    |               |\n' \
        + '    |               |___________ calcData.txt [authomatically created]\n'  \
        + '    |               |___________ inputData.txt [authomatically created]\n'  \
        + '    |               |___________ numbers.txt [authomatically created]\n'  \
        + '    |               |___________ otherNumbers.txt [authomatically created]\n'  \
        + '    |\n' \
        + '    |___________ parameters (dir) [optional]\n' \
        + '                    |\n' \
        + '                    |___________ alterableVar.txt [optional]\n'  \
        + '                    |___________ fixedOtherNum.txt [optional]\n'  \
        + '                    |___________ scalingData.txt [optional]\n'  \
        + '                    |___________ scalingNumber.txt [optional]\n\n'  \
        + option1 + '\n' \
        + '          -> Input data in input/inputData.txt in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + option2 + ' \n' \
        + '          -> Starting-guess input data in input/inputData.txt (NOTE: only the recurring variables are used) in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Dimensionless numbers in input/numbers.txt in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Repeating variable to be scaled in parameters/scalingData.txt in the format:\n' \
        + '                           <variable> <multiplying_factor>\n' \
        + option3 + '\n' \
        + '          -> Starting-guess input data in input/inputData.txt (NOTE: only the recurring variables are used) in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Dimensionless numbers in input/numbers.txt in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Repeating variable to be scaled in parameters/scalingData.txt in the format:\n' \
        + '                           <variable> <multiplying_factor>\n' \
        + '          -> Dimensionless number to be scaled in parameters/scalingNumber.txt in the format:\n' \
        + '                           <variable> <multiplying_factor>\n' \
        + option4 + '\n' \
        + '          -> Starting-guess input data in input/inputData.txt (NOTE: only the recurring variables are used) in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Dimensionless numbers in input/numbers.txt in the format:\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           <key> <value> <comment>\n' \
        + '                           ...   ...     ...\n' \
        + '          -> Dimensionless number to be altered in parameters/alterableNum.txt in the format:\n' \
        + '                           <variable>\n' \
        + '          -> Other dimensionless number to be kept constant in parameters/fixedOtherNumber.txt in the format:\n' \
        + '                           <variable>\n' \
        + '          -> Repeating variable to be scaled in parameters/scalingData.txt in the format:\n' \
        + '                           <variable> <multiplying_factor>\n' \
        + '          -> Dimensionless number to be scaled in parameters/scalingNumber.txt in the format:\n' \
        + '                           <variable> <multiplying_factor>\n' \

#---------- Housekeeping. --------------------------------------------------------------------
if (len(sys.argv) == 1) or (sys.argv[1] == '-h') or (sys.argv[1] == '--help') or (sys.argv[1] == 'help'):
    sys.exit(guideString)

if not len(sys.argv) == 2:
    print(len(sys.argv))
    raise ValueError( 'Wrong argv list. Type python3 ' + sys.argv[0] + ' -h for guide.')

#---------- OPTION 1. ------------------------------------------------------------------------
if not os.path.exists(sys.argv[1] + '/input/numbers.txt'):
    print(option1)
    converter = conv.Conv_d_g_rhoL( \
                drag, \
                inOut.readDict(sys.argv[1], 'input/inputData.txt' ) \
                )

#---------- All other options. ---------------------------------------------------------------
else:
    converter = conv.Conv_d_g_rhoL( \
                drag, \
                inOut.readDict( sys.argv[1], 'input/inputData.txt' ), \
                inOut.readDict( sys.argv[1], 'input/numbers.txt'   ), \
                scaleRv = inOut.readParameter( sys.argv[1], 'parameters/scalingData.txt',   conv.Conv_d_g_rhoL().listRv ), \
                scaleNu = inOut.readParameter( sys.argv[1], 'parameters/scalingNumber.txt', conv.Conv_d_g_rhoL().listNu ), \
                alterNu = inOut.readParameter( sys.argv[1], 'parameters/alterableNum.txt',  conv.Conv_d_g_rhoL().listNu ), \
                fixedOn = inOut.readParameter( sys.argv[1], 'parameters/fixedOtherNum.txt', conv.Conv_d_g_rhoL().listOn )  \
                )

    if not converter.scaleNu:
        print(option2)

    else:

        if converter.scaleRv and converter.scaleNu and converter.alterNu and converter.fixedOn:
            print(option4)

        else:
            print(option3)

#---------- Output on files. -----------------------------------------------------------------
inOut.writeDict(converter.In, sys.argv[1], 'output/inputData.txt')
inOut.writeDict(converter.Ca, sys.argv[1], 'output/calcData.txt')
inOut.writeDict(converter.Nu, sys.argv[1], 'output/numbers.txt')
inOut.writeDict(converter.On, sys.argv[1], 'output/otherNumbers.txt')
inOut.writeDict( \
        ( {**converter.In[0], '------------------------------': '------------------------------', \
           **converter.Ca[0], '-------------------------------': '------------------------------', \
           **converter.Nu[0], '--------------------------------': '------------------------------', \
           **converter.On[0]}, \
          {**converter.In[1], '------------------------------': '------------------------------', \
           **converter.Ca[1], '-------------------------------': '------------------------------', \
           **converter.Nu[1], '--------------------------------': '------------------------------', \
           **converter.On[1]} ), \
      sys.argv[1], 'output/all.txt' )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
