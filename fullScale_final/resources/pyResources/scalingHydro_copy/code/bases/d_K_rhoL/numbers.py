import math



##############################################################################################
def Pi1 ( q1, d, nNoz, K, n ):                                                               #
##############################################################################################
    """d**(3*n - 4) * (nNoz * q1)**(2 - n) / K
    Args:
        float: q1:   flow rate per nozzle
        fload: d:    particle diameter
        float: nNoz: number of nozzles
        float: K:    power-law consistency coefficient
        float: n:    power-law index
    Returns:
        float: Pi1: dimensionless number
    """
    return d**(3*n - 4) * (nNoz * q1)**(2 - n) / K, \
           "d**(3*n - 4) * (nNoz * q1)**(2 - n) / K"
##############################################################################################
def Pi1_inv ( Pi1, d, nNoz, K, n ):                                                          #
##############################################################################################
    """d**(3*n - 4) * (nNoz * q1)**(2 - n) / K: inverse relative to q1
    Args:
        Pi1: dimensionless number
        fload: d:    particle diameter
        float: nNoz: number of nozzles
        float: K:    power-law consistency coefficient
        float: n:    power-law index
    Returns: float:
        float: q1:   flow rate per nozzle
    """
    return ( Pi1 * K * d**(4 - 3*n) )**( 1 / (2 - n) ) / nNoz, \
           "flow rate per nozzle"



##############################################################################################
def Pi2 ( g, d, K, n ):                                                                      #
##############################################################################################
    """d**(n + 2) * g**(2 - n) / K**2
    Args:
        float: g:    acceleration of gravity
        fload: d:    particle diameter
        float: K:    power-law consistency coefficient
        float: n:    power-law index
    Returns:
        float: Pi2: dimensionless number
    """
    return d**(n + 2) * g**(2 - n) / K**2, \
           "d**(n + 2) * g**(2 - n) / K**2"
##############################################################################################
def Pi2_inv ( Pi2, d, K, n ):                                                                #
##############################################################################################
    """d**(n + 2) * g**(2 - n) / K**2: inverse relative to g
    Args:
        float: Pi2: dimensionless number
        fload: d:    particle diameter
        float: K:    power-law consistency coefficient
        float: n:    power-law index
    Returns:
        float: g: acceleration of gravity
    """
    return ( Pi2 * K**2 / d**(n + 2) )**( 1 / (2 - n) ), \
           "acceleration of gravity"



##############################################################################################
def Pi3 ( rhoP, rhoL ):                                                                      #
##############################################################################################
    """rhoL / rhoP
    Args:
        float: rhoP: particle density
        float: rhoL: liquid phase density
    Returns:
        float: Pi3: dimensionless number
    """
    return rhoL / rhoP, \
           "rhoL / rhoP"
##############################################################################################
def Pi3_inv ( Pi3, rhoL ):                                                                   #
##############################################################################################
    """rhoL / rhoP: inverse relative to rhoP
    Args:
        float: Pi3: dimensionless number
        float: rhoL: liquid phase density
    Returns:
        float: rhoP: particle density
    """
    return rhoL / Pi3, \
           "particle density"



##############################################################################################
def Pi4 ( Dext, d ):                                                                         #
##############################################################################################
    """Dext / d
    Args:
        float: Dext: cylinder diameter
        fload: d:    particle diameter
    Returns:
        float: Pi4: dimensionless number
    """
    return Dext / d, \
           "Dext / d"
##############################################################################################
def Pi4_inv ( Pi4, d ):                                                                      #
##############################################################################################
    """Dext / d
    Args:
        float: Pi4: dimensionless number
        fload: d:    particle diameter
    Returns:
        float: Dext: cylinder diameter
    """
    return Pi4 * d, \
           "cylinder diameter"



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
