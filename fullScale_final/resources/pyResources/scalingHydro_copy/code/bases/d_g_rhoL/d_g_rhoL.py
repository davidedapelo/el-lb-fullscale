import math
import numpy as np

import code.convBase as cb
import code.f_common.f_geometryNumbers as fGe
import code.bases.d_g_rhoL.numbers as fNu



class Conv_d_g_rhoL ( cb.ConvBase ):
    """Convertor using [d, K, rhoL] as repeating variables.
    """



##############################################################################################
    def __init__ ( self, drag=None, *args, **kwargs ):
##############################################################################################
        """Initializing class-specific lists.
        """

#---------- Base-dependent lists. ------------------------------------------------------------
        self.listIn = [ 'g', 'Hcyl', 'Hfrust', 'Dext', 'Dint', 'hNoz', 'rNoz', 'nNoz', 'q1', 'd', 'K', 'n', 'rhoL', 'rhoP' ]
        self.listCa = [ 'hCol', 'V', 'Vcol', 'FB', 'u', 'nu', 'uAvg', 'n1Bub', 'Nbub', 'p1D', 'PD', 'Ptheo' ]
        self.listRv = [ 'd', 'nNoz', 'g', 'n', 'rhoL' ]
        self.listNu = [ 'gDint', 'gHcyl', 'gHfrust', 'gHnoz', 'gRnoz', 'n', 'nNoz', 'Pi1', 'Pi2', 'Pi3', 'Pi4' ]

#---------- Parent class constructor. --------------------------------------------------------
        super().__init__(drag, *args, **kwargs)



##############################################################################################
    def buildInFromNu ( self ):
##############################################################################################
        """Builds In.
        """

#---------- Housekeeping. --------------------------------------------------------------------
        dIn = self.In[0]
        cIn = self.In[1]
        dNu = self.Nu[0]
        cNu = self.Nu[1]
        dRv = self.Rv[0]
        cRv = self.Rv[1]
        super().checkDict(dNu, self.listNu)
        super().checkDict(dRv, self.listRv)

#---------- Computing input data. ------------------------------------------------------------
        #particle diameter
        dIn['d'],      cIn['d']      = dRv['d'], cRv['d']

        #acceleration of gravity
        dIn['g'],      cIn['g']      = dRv['g'], cRv['g']

        #power-law index
        dIn['n'],      cIn['n']      = dRv['n'], cRv['n']

        #liquid phase density
        dIn['rhoL'],   cIn['rhoL']   = dRv['rhoL'], cRv['rhoL']

        #number of nozzles
        dIn['nNoz'],   cIn['nNoz']   = dRv['nNoz'], cRv['nNoz']

        #power-law consistency coefficient
        dIn['K'],      cIn['K']      = fNu.Pi2_inv( dNu['Pi2'], dRv['d'], dRv['g'], dRv['n'] )

        #cylinder diameter
        dIn['Dext'],   cIn['Dext']   = fNu.Pi4_inv( dNu['Pi4'], dRv['d'] )

        #low frustum diameter
        dIn['Dint'],   cIn['Dint']   = fGe.gDint_inv( dNu['gDint'], dIn['Dext'] )

        #cylinder height
        dIn['Hcyl'],   cIn['Hcyl']   = fGe.gHcyl_inv( dNu['gHcyl'], dIn['Dext'] )

        #frustum height
        dIn['Hfrust'], cIn['Hfrust'] = fGe.gHFrust_inv( dNu['gHfrust'], dIn['Dext'] )

        #Height of nozzle from bottom
        dIn['hNoz'],   cIn['hNoz']   = fGe.gHnoz_inv( dNu['gHnoz'], dIn['Dext'] )

        #Distance of nozzle from centre
        dIn['rNoz'],   cIn['rNoz']   = fGe.gRnoz_inv( dNu['gRnoz'], dIn['Dext'] )

        #flow rate per nozzle
        dIn['q1'],     cIn['q1']     = fNu.Pi1_inv( dNu['Pi1'], dRv['d'], dRv['nNoz'], dRv['g'], dRv['n'] )

        #particle density
        dIn['rhoP'],   cIn['rhoP']   = fNu.Pi3_inv( dNu['Pi3'], dRv['rhoL'] )



##############################################################################################
    def buildNuFromIn ( self ):
##############################################################################################
        """Builds a dictionary containing all the dimensionless numbers.
        """

#---------- Housekeeping. --------------------------------------------------------------------
        dIn = self.In[0]
        cIn = self.In[1]
        dNu = self.Nu[0]
        cNu = self.Nu[1]
        super().checkDict(dIn, self.listIn)

#---------- Computing dimensionless numbers. -------------------------------------------------
        #How many nozzles (reported)
        dNu['nNoz'],    cNu['nNoz']    = dIn['nNoz'], cIn['nNoz']
   
        #Power-law index (reported)
        dNu['n'],       cNu['n']       = dIn['n'],  cIn['n']

        #Dint / Dext
        dNu['gDint'],   cNu['gDint']   = fGe.gDint( dIn['Dint'], dIn['Dext'] )

        #Hcyl / Dext
        dNu['gHcyl'],   cNu['gHcyl']   = fGe.gHcyl( dIn['Hcyl'], dIn['Dext'] )

        #Hfrust / Dext
        dNu['gHfrust'], cNu['gHfrust'] = fGe.gHfrust( dIn['Hfrust'], dIn['Dext'] )

        #hNoz / Dext
        dNu['gHnoz'],   cNu['gHnoz']   = fGe.gHnoz( dIn['hNoz'], dIn['Dext'] )

        #2 * rNoz / Dext
        dNu['gRnoz'],   cNu['gRnoz']   = fGe.gRnoz( dIn['rNoz'], dIn['Dext'] )

        #d**5 * g / q1**2
        dNu['Pi1'],     cNu['Pi1']     = fNu.Pi1( dIn['q1'], dIn['d'], dIn['nNoz'], dIn['g'], dIn['n'] )

        #g**(n - 2) * K**2 / d**(n + 2)
        dNu['Pi2'],     cNu['Pi2']     = fNu.Pi2( dIn['K'], dIn['d'], dIn['g'], dIn['n'] )

        #rhoL / rhoP
        dNu['Pi3'],     cNu['Pi3']     = fNu.Pi3( dIn['rhoP'], dIn['rhoL'] )

        #Dext / d
        dNu['Pi4'],     cNu['Pi4']     = fNu.Pi4( dIn['Dext'], dIn['d'] )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
