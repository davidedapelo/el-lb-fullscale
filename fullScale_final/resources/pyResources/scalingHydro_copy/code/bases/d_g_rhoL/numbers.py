import math



##############################################################################################
def Pi1 ( q1, d, nNoz, g, n ):                                                               #
##############################################################################################
    """d**5 * g / q1**2
    Args:
        float: q1:   flow rate per nozzle
        fload: d:    particle diameter
        float: nNoz: number of nozzles
        float: g:    acceleration of gravity
        float: n:    power-law index
    Returns:
        float: Pi1: dimensionless number
    """
    return d**5 * g / q1**2, \
           "d**5 * g / q1**2"
##############################################################################################
def Pi1_inv ( Pi1, d, nNoz, g, n ):                                                          #
##############################################################################################
    """math.sqrt( d**5 * g / Pi1 ): inverse relative to q1
    Args:
        Pi1: dimensionless number
        fload: d:    particle diameter
        float: nNoz: number of nozzles
        float: g:    acceleration of gravity
        float: n:    power-law index
    Returns: float:
        float: q1:   flow rate per nozzle
    """
    return math.sqrt( d**5 * g / Pi1 ), \
           "flow rate per nozzle"



##############################################################################################
def Pi2 ( K, d, g, n ):                                                                      #
##############################################################################################
    """d**(n + 2) / ( g**(n - 2) * K**2 )
    Args:
        float: K:    power-law consistency coefficient
        fload: d:    particle diameter
        float: g:    acceleration of gravity
        float: n:    power-law index
    Returns:
        float: Pi2: dimensionless number
    """
    return d**(n + 2) / ( g**(n - 2) * K**2 ), \
           "d**(n + 2) / ( g**(n - 2) * K**2 )"
##############################################################################################
def Pi2_inv ( Pi2, d, g, n ):                                                                #
##############################################################################################
    """math.sqrt( d**(n + 2) / ( g**(n - 2) * Pi2 ) ): inverse relative to K
    Args:
        float: Pi2: dimensionless number
        fload: d:   particle diameter
        float: g:   acceleration of gravity
        float: n:   power-law index
    Returns:
        float: K: power-law consistency coefficient
    """
    return math.sqrt( d**(n + 2) / ( g**(n - 2) * Pi2 ) ), \
           "power-law consistency coefficient"



##############################################################################################
def Pi3 ( rhoP, rhoL ):                                                                      #
##############################################################################################
    """rhoL / rhoP
    Args:
        float: rhoP: particle density
        float: rhoL: liquid phase density
    Returns:
        float: Pi3: dimensionless number
    """
    return rhoL / rhoP, \
           "rhoL / rhoP"
##############################################################################################
def Pi3_inv ( Pi3, rhoL ):                                                                   #
##############################################################################################
    """rhoL / rhoP: inverse relative to rhoP
    Args:
        float: Pi3: dimensionless number
        float: rhoL: liquid phase density
    Returns:
        float: rhoP: particle density
    """
    return rhoL / Pi3, \
           "particle density"



##############################################################################################
def Pi4 ( Dext, d ):                                                                         #
##############################################################################################
    """Dext / d
    Args:
        float: Dext: cylinder diameter
        fload: d:    particle diameter
    Returns:
        float: Pi4: dimensionless number
    """
    return Dext / d, \
           "Dext / d"
##############################################################################################
def Pi4_inv ( Pi4, d ):                                                                      #
##############################################################################################
    """Dext / d
    Args:
        float: Pi4: dimensionless number
        fload: d:    particle diameter
    Returns:
        float: Dext: cylinder diameter
    """
    return Pi4 * d, \
           "cylinder diameter"



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
