import math
import numpy as np
from scipy.optimize import fsolve
from scipy.optimize import root
from abc import ABC, abstractmethod

import code.f_common.f_calcData as fCa
import code.f_common.f_otherNumbers as fOn



class ConvBase ( ABC ):
    """Abstract base class for convertors.
    Gets a set of dimensionless numbers from a set of input data, and vice-versa starting from an initial guess.
    """



##############################################################################################
    def __init__ ( self, drag=None, *args, **kwargs ):
##############################################################################################
        """Initializing generic lists and the data containers.
        Then, making the computations depending on *args and **kwargs.
        """

#========== Preliminary operations. ==========================================================
        self.listOn = [ 'Ar', 'Nq', 'Re', 'ReP'] # Now all the lists are complete

#---------- No args: no computations, only lists. --------------------------------------------
        if len(args) == 0:
            return

#---------- Initializing the data containers. ------------------------------------------------
        self.Rv = ( dict(), dict() ) # Repeating dimensional variables
        self.In = ( dict(), dict() ) # Input dimensional variables
        self.Ca = ( dict(), dict() ) # Calculated dimensional variables
        self.Nu = ( dict(), dict() ) # Dimensionless numbers for Buckingham theorem
        self.On = ( dict(), dict() ) # Other dimensionless numbers

#---------- kwargs flags: True if (par,value); false if (None,None) or non-existent. ---------
        self.scaleRv = not kwargs['scaleRv'] == (None,None) if 'scaleRv' in kwargs.keys() else False # scaleRv \in Rv to be scaled?
        self.scaleNu = not kwargs['scaleNu'] == (None,None) if 'scaleNu' in kwargs.keys() else False # scaleNu \in Nu to be scaled?
        self.alterNu = not kwargs['alterNu'] == (None,None) if 'alterNu' in kwargs.keys() else False # alterNu \in Nu to be altered...
        self.fixedOn = not kwargs['fixedOn'] == (None,None) if 'fixedOn' in kwargs.keys() else False # ...while keeping fixedOn \in On constant?

#---------- Preliminary computations, common to all the cases. -------------------------------
        self.In = args[0] # In is read anyway. Following code below will ascertain whether are proper input vars or only preliminary (guess) data.
        self.drag = drag


#========== Case-dependent computations. =====================================================

#---------- From In to all the others. -------------------------------------------------------
        if len(args) == 1: # Nu not given as input: In are proper input vars. All the rest is compute from In.
            self.buildNuFromIn()

#---------- From Nu to all the others, with the help of a preliminary-data In. ---------------
        elif len(args) == 2: # Nu given as input: In are preliminary input vars.
            self.Nu = args[1]    # Nu is read as input. All is computed from Nu...
            self.buildRvFromIn() # ...but first, Rv are extracted from preliminary In.

            if self.scaleRv: # Scaling scaleRv
                self.scaleEntry( self.Rv, kwargs['scaleRv'] )

                if self.scaleNu:
                    if self.alterNu and self.fixedOn: # Scaling scaleNu AND iteratively modifying alterNu while keeping fixedOn constant
                        self.buildInFromNu()          # Starting guess needed for iterations
                        self.buildCaFromIn(self.drag) # Starting guess needed for iterations
                        self.buildOn()                # Starting guess needed for iterations
                        self.scaleEntry( self.Nu, kwargs['scaleNu'] ) # this does not modify On!
                        self.parIteration( kwargs['alterNu'], kwargs['fixedOn'] ) # Iterations

                    else: # Scaling scaleNu only
                        self.scaleEntry( self.Nu, kwargs['scaleNu'] )

            self.buildInFromNu()

#---------- Exception raised if wrong input. -------------------------------------------------
        else:
            raise ValueError('Wrong number of arguments for ConvBase constructor.')


#========== Post-computations, common to all the cases. ======================================
        self.buildCaFromIn(self.drag)
        self.buildOn()



##############################################################################################
    def buildCaFromIn ( self, drag ):
##############################################################################################
        """Builds Ca.
        Args:
            callable f(ReP): drag:  function for drag coefficient 
        """

#---------- Housekeeping. --------------------------------------------------------------------
        dIn = self.In[0]
        cIn = self.In[1]
        dCa = self.Ca[0]
        cCa = self.Ca[1]
        self.checkDict(dIn, self.listIn)

#---------- Computing calculated data. -------------------------------------------------------
        #Height of the bubble column
        dCa['hCol'],  cCa['hCol']  = fCa.hCol( dIn['Hcyl'], dIn['Hfrust'], dIn['Dext'], dIn['Dint'], dIn['hNoz'], dIn['rNoz'] ) 

        #Volume of the tank
        dCa['V'],     cCa['V']     = fCa.V( dIn['Hcyl'], dIn['Hfrust'], dIn['Dext'], dIn['Dint'] )

        #Volume of the bubble column
        dCa['Vcol'],  cCa['Vcol']  = fCa.Vcol( dIn['d'], dCa['hCol'] )

        #buoyancy force
        dCa['FB'],    cCa['FB']    = fCa.FB( dIn['g'], dIn['rhoL'], dIn['rhoP'], dIn['d'] )

        #particle asymptotic velocity
        dCa['u'],     cCa['u']     = fCa.u( drag, fOn.ReP, dCa['FB'], dIn['K'], dIn['n'], dIn['d'], dIn['rhoL'] )

        #Characteristic kinematic viscosity (viz., around the bubble column).
        dCa['nu'],    cCa['nu']    = fCa.nu( dIn['K'], dIn['n'], dIn['d'], dCa['u'] )

        #Liquid phase average velocity
        dCa['uAvg'],  cCa['uAvg']  = fCa.uAvg( dCa['u'], dCa['V'], dCa['Vcol'] )

        #Bubbles per nozzle per second
        dCa['n1Bub'], cCa['n1Bub'] = fCa.n1Bub( dIn['q1'], dIn['d'] )

        #Bubbles in the tank at any given time
        dCa['Nbub'],  cCa['Nbub']  = fCa.Nbub( dCa['n1Bub'], dIn['nNoz'], dCa['hCol'], dCa['u'] )

        # Drag power per particle
        dCa['p1D'],   cCa['p1D']   = fCa.p1D( dCa['u'], dCa['FB'] )

        #Total drag power
        dCa['PD'],    cCa['PD']    = fCa.PD( dCa['p1D'], dCa['Nbub'] )

        #Total theoretical nozzle power at atmospheric pressure
        dCa['Ptheo'], cCa['Ptheo'] = fCa.Ptheo( dIn['g'], dIn['nNoz'], dIn['q1'], dIn['rhoL'], dCa['hCol'] ) 



##############################################################################################
    @abstractmethod
    def buildInFromNu ( self, scaledNum=(None,None) ):
##############################################################################################
        """Builds In.
        Args:
            tuple(varName, factor): tuple: scaledNum
                str:   varName: name of the scaled dimensionless number
                float: factor:  factor multiplying the initial value of the dimensionless number to be scaled
        """
        pass



##############################################################################################
    @abstractmethod
    def buildNuFromIn ( self ):
##############################################################################################
        """Builds a dictionary containing all the dimensionless numbers.
        """
        pass



##############################################################################################
    def buildOn ( self ):
##############################################################################################
        """Build On.
        """

#---------- Housekeeping. --------------------------------------------------------------------
        dIn = self.In[0]
        cIn = self.In[1]
        dCa = self.Ca[0]
        cCa = self.Ca[1]
        dOn = self.On[0]
        cOn = self.On[1]
        self.checkDict(dIn, self.listIn)
        self.checkDict(dCa, self.listCa)

#---------- Computing other dimensionless numbers. -------------------------------------------
        #Archimedes number: g * d**3 * (rhoL - rhoP) / (nu**2 * rhoL)
        dOn['Ar'],    cOn['Ar']    = fOn.Ar( dIn['g'], dCa['nu'], dIn['rhoL'], dIn['rhoP'], dIn['d'] )

        #Froude number: u / math.sqrt(g * d)
        dOn['Fr'],    cOn['Fr']    = fOn.Fr( dIn['g'], dIn['d'], dCa['u'] )

        #Flow-rate number: nNoz * q1 / (nu *d)
        dOn['Nq'],    cOn['Nq']    = fOn.Nq( dIn['nNoz'], dIn['q1'], dCa['nu'], dIn['d'] )

        #Power number: PD * d / (rhoL * nu**3)
        dOn['PdNu'],  cOn['PdNu']  = fOn.PdNu( dCa['PD'], dIn['d'], dCa['nu'], dIn['rhoL'] )

        #Power number: PD / (rhoL * d * nu * u**2)
        dOn['PdNuU'], cOn['PdNuU'] = fOn.PdNuU( dCa['PD'], dIn['d'], dCa['nu'], dCa['u'], dIn['rhoL'] )

        #Power number: PD / (rhoL * d**2 * u**3)
        dOn['PdU'],   cOn['PdU']   = fOn.PdU( dCa['PD'], dIn['d'], dCa['u'], dIn['rhoL'] )

        #Reynolds number: uAvg * Dext / nu
        dOn['Re'],    cOn['Re']    = fOn.Re( dCa['uAvg'], dIn['Dext'], dCa['nu'] )

        #Particle Reynolds number: u * d / nu
        dOn['ReP'],   cOn['ReP']   = fOn.ReP( dCa['u'], dIn['d'], dCa['nu'] )



##############################################################################################
    def buildRvFromIn ( self ):
##############################################################################################
        """Extracts Rv from In0
        """

#---------- Housekeeping. --------------------------------------------------------------------
        self.checkDict(self.In[0], self.listIn)

#---------- Initializing. --------------------------------------------------------------------
        dRv = self.Rv[0]
        cRv = self.Rv[1]

#---------- Building repeating variables. ----------------------------------------------------
        for key in self.listRv:
            dRv[key] = self.In[0][key]
            cRv[key] = self.In[1][key]



##############################################################################################
    def checkDict ( self,  d, keys ):
##############################################################################################
        """Check a dict keys are the same (regardless of the order) of the entries of a list.
            Args:
            dict:      d:    dictionary
            list[str]: keys: list of keys
        """
        for key in keys: # Checking all the required keys are present in d.
            if not key in d.keys():
                raise ValueError('Key ' + key + ' not present in dictionary ' + str(d))

        for key in d.keys(): # Checking d does not have wrong keys.
            if not key in keys:
                raise ValueError('Wrong key ' + key + ' in dictionary ' + str(d))



##############################################################################################
    def parIteration ( self, alterNu, fixedOn ):
##############################################################################################
        """ Modifies Nu through iteratively modifying <alterNu> to keep <fixedOn> constant.
        Args:
            list[str]: alterNu:  Nu elements to be altered recursively
            list[str]: fixedOn:  On elements to be kept constant
        """

        if not len(alterNu) == len(fixedOn):
            raise ValueError( 'alterNu=' + str(alterNu) + ' does not have the size of fixedOn =' + str(fixedOn) )

#---------- Value of fixedOn \in on set as a target for iterations. --------------------------
        fixedOn_target = [ self.On[0][item] for item in fixedOn ]


#========== Recursive function to get the altered dimensionless number. ======================
        def iterAlterNu ( guess ): # guess is the previous value of alterNu \in Nu

#---------- Guess values for all the data dicts are computed through conv_guess. -------------
            iteratedNu = self.Nu[0].copy()
            for i in np.arange( len(alterNu) ):
                iteratedNu[alterNu[i]] = guess[i]

            conv_guess = type(self)( \
                         self.drag, \
                         ( self.In[0].copy(), self.In[1].copy() ), \
                         ( iteratedNu, self.Nu[1].copy() ) \
                         )

            #print(conv_guess.Nu[0])
            #print(conv_guess.In[0])
            #print(conv_guess.Ca[0])
            #print(conv_guess.On[0])

#---------- Guess value for fixedOn \in On extracted from conv_guess's internal field. -------
            fixedOn_guess = [ conv_guess.On[0][item] for item in fixedOn ]

#---------- Printing for diagnostics. --------------------------------------------------------
            print('Iteration:')
            for i in np.arange( len(alterNu) ):
                print( '   guess(' + alterNu[i] + ')=' + str(guess[i]) \
                    + '    computed(' + fixedOn[i] + ')=' + str(fixedOn_guess[i]) \
                    + '    target(' + fixedOn[i] + ')=' + str(fixedOn_target[i]) )

#---------- Returning. In this way, relative error is minimized. -----------------------------
            return [ 1 - fixedOn_guess[i] / fixedOn_target[i] for i in np.arange( len(alterNu) ) ]


#========== Performing the iteration. ========================================================
        iterNu = [ self.Nu[0][item] for item in alterNu ]
        iterNu = fsolve( iterAlterNu, iterNu )
        #iterNu = root( iterAlterNu, iterNu, method='lm' ).x

        for i in np.arange( len(alterNu) ):
            self.Nu[0][alterNu[i]] = iterNu[i]



##############################################################################################
    def scaleEntry ( self, d, entry ):
##############################################################################################
        """Scales the variable entry[0] inside the data container d
        by a multiplicative factor entry[1].
        Args:
            tuple(dict, dict): d:                data container: (data, comments)
            list[str]:         entry:  entry[0]: name of the entry in d to be scaled
                                       entry[1]: multiplicative factor (to be converted to float below)
        """
        if not entry[0] in d[0].keys():
            raise ValueError( 'entry ' + entry[0] + ' not present in variable list ' + str(d[0].keys()) )

        d[0][entry[0]] = float(entry[1]) * d[0][entry[0]]



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
