import sys



##############################################################################################
def dragMorsi ( ReP ):                                                                       #
##############################################################################################
    """Drag model as in Morsi and Alexander (1972).

    Args:
        float: ReP: particle Reynolds number

    Returns:
        float:      drag coefficient
    """

    testPositive(ReP)

    if ReP < 0.1:
        a = [ 0.0,     24.0,     0.0       ]

    elif ReP < 1.0:
        a = [ 3.69,    22.73,     0.0903   ]

    elif ReP < 10.0:
        a = [ 1.222,   29.16667, -3.8889   ]

    elif ReP < 100.0:
        a = [ 0.6167,  46.5,     -116.67   ]

    elif ReP < 1000.0:
        a = [ 0.3644,  98.33,    -2778     ]

    elif ReP < 5000.0:
        a = [ 0.357,   148.62,   -4.75e4   ]

    elif ReP < 10000.0:
        a = [ 0.46,   -490.546,   57.87e4  ]

    else:
        a = [ 0.5191, -1662.5,    5.4167e6 ]

    return ( a[0] + a[1]/ReP + a[2]/(ReP*ReP) )



##############################################################################################
def testPositive(num):                                                                       #
##############################################################################################
    """Test to check if an input number is strictly positive.
    If not, raises a ValueError exception.
    """

    if num <=0:
        raise ValueError('Non-positive number (' + str(num) + ') where only strictly postive numer allowed.')



########### MAIN. ############################################################################
if __name__ == '__main__':                                                                   #
##############################################################################################

    if len(sys.argv) == 1:
        arg = 1
    else:
        arg = float(sys.argv[1])

    print( 'Drag coefficient for ReP=' + str(arg) )
    print( 'Morsi and Alexander (1972): ' + str(dragMorsi(arg)) )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
