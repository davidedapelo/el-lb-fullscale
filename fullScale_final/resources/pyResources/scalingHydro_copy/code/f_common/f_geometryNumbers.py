import math



##############################################################################################
def gDint ( Dint, Dext ):                                                                    #
##############################################################################################
    """Dint / Dext
    Args:
        float: Dint: low frustum diameter
        float: Dext: cylinder diameter
    Returns:
        float: gDint: dimensionless number
    """
    return Dint / Dext, \
           "Dint / Dext"
##############################################################################################
def gDint_inv ( gDint, Dext ):                                                               #
##############################################################################################
    """Dint / Dext: inverse relative to Dint
    Args:
        float: gDint: dimensionless number
        float: Dext:  cylinder diameter
    Returns:
        float: Dint: low frustum diameter
    """
    return gDint * Dext, \
           "low frustum diameter"



##############################################################################################
def gHcyl ( Hcyl, Dext ):                                                                    #
##############################################################################################
    """Hcyl / Dext
    Args:
        float: Hcyl: cylinder height
        float: Dext: cylinder diameter
    Returns:
        float: gHcyl: dimensionless number
    """
    return Hcyl / Dext, \
           "Hcyl / Dext"
##############################################################################################
def gHcyl_inv ( gHcyl, Dext ):                                                               #
##############################################################################################
    """Hcyl / Dext: inverse relative to Hcyl
    Args:
        float: gHcyl: dimensionless number
        float: Dext:  cylinder diameter
    Returns:
        float: Hcyl: cylinder height
    """
    return gHcyl * Dext, \
           "cylinder height"



##############################################################################################
def gHfrust ( Hfrust, Dext ):                                                                #
##############################################################################################
    """Hfrust / Dext
    Args:
        float: Hfrust: frustum height
        float: Dext:   cylinder diameter
    Returns:
        float: gHfrust: dimensionless number
    """
    return Hfrust / Dext, \
           "Hfrust / Dext"
##############################################################################################
def gHFrust_inv ( gHfrust, Dext ):                                                           #
##############################################################################################
    """Hfrust / Dext: inverse relative to Hfrust
    Args:
        float: gHfrust: dimensionless number
        float: Dext:    cylinder diameter
    Returns:
        float: Hfrust: cylinder height
    """
    return gHfrust * Dext, \
           "cylinder height"



##############################################################################################
def gHnoz ( hNoz, Dext ):                                                                    #
##############################################################################################
    """hNoz / Dext
    Args:
        float: hNoz: Height of nozzle from bottom
        float: Dext: cylinder diameter
    Returns:
        float: gHnoz: dimensionless number
    """
    return hNoz / Dext, \
           "hNoz / Dext"
##############################################################################################
def gHnoz_inv ( gHnoz, Dext ):                                                               #
##############################################################################################
    """hNoz / Dext: inverse relative to hNoz
    Args:
        float: gHnoz: low frustum diameter
        float: Dext:  cylinder diameter
    Returns:
        float: hNoz: Height of nozzle from bottom
    """
    return gHnoz * Dext, \
           "Height of nozzle from bottom"



##############################################################################################
def gRnoz ( rNoz, Dext ):                                                                    #
##############################################################################################
    """2 * rNoz / Dext
    Args:
        float: rNoz: Distance of nozzle from centre
        float: Dext: cylinder diameter
    Returns:
        float: gRnoz: dimensionless number
    """
    return 2 * rNoz / Dext, \
           "2 * rNoz / Dext"
##############################################################################################
def gRnoz_inv ( gRnoz, Dext ):                                                               #
##############################################################################################
    """2 * rNoz / Dext: inverse relative to rNoz
    Args:
        float: gRnoz: dimensionless number
        float: Dext: cylinder diameter
    Returns:
        float: rNoz: Distance of nozzle from centre
    """
    return gRnoz / (2 * Dext), \
           "Distance of nozzle from centre"



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
