import math
from scipy.optimize import fsolve



##############################################################################################
def FB ( g, rhoL, rhoP, d ):                                                                 #
##############################################################################################
    """Buoyancy force.
        float: g:    acceleration of gravity
        float: rhoL: liquid phase density
        float: rhoP: particle density
        float: d:    particle diameter
    """
    return math.pi * d**3 * abs(rhoL - rhoP) * g / 6, \
           "Buoyancy force."



##############################################################################################
def FD ( CD, rhoL, d, u ):                                                                   #
##############################################################################################
    """Asymptotic drag force.
        float: CD:   drag coefficient
        float: rhoL: liquid phase density
        float: d:    particle diameter
        float: u:    particle asymptotic velocity
    """
    return math.pi * CD * rhoL * d**2 * u**2 / 8, \
           "Asymptotic drag force."



##############################################################################################
def hCol ( Hcyl, Hfrust, Dext, Dint, hNoz, rNoz ):                                           #
##############################################################################################
    """Height of the bubble column
        float: Hcyl:   cylinder height
        float: Hfrust: frustum height
        float: Dext:   cylinder diameter
        float: Dint:   low frustum diameter
        float: hNoz:   height of nozzle from bottom
        float: rNoz:   distance of nozzle from centre
    """
    return Hcyl - hNoz + ( Hfrust*(Dext-2*rNoz)/(Dext-Dint) if rNoz>=Dint/2 else Hfrust ), \
           "Height of the bubble column"



##############################################################################################
def n1Bub ( q1, d ):                                                                         #
##############################################################################################
    """Bubbles per nozzle per second
        float: q1: flow rate per nozzle
        float: d:  particle diameter
    """
    return q1 / (math.pi * d**3 / 6), \
           "Bubbles per nozzle per second"



##############################################################################################
def Nbub ( n1Bbub, nNoz, hCol, u ):                                                          #
##############################################################################################
    """Bubbles in the tank at any given time
        float: n1Bbub: bubbles per nozzle per second
        float: nNoz:   number of nozzles
        float: hCol:   height of the bubble column
        float: u:      particle asymptotic velocity
    """
    return n1Bbub * nNoz * hCol / u, \
           "Bubbles in the tank at any given time"



##############################################################################################
def nu ( K, n, d, u ):                                                                       #
##############################################################################################
    """Characteristic kinematic viscosity (viz., around the bubble column).
        float: K: power-law consistency coefficient
        float: n: power-law index
        float: d: particle diameter
        float: u: particle asymptotic velocity
    """
    return K*(2*u/d)**(n-1), \
           "Characteristic kinematic viscosity (viz., around the bubble column)"



##############################################################################################
def p1D ( u, FD ):                                                                           #
##############################################################################################
    """ Drag power per particle
        float: u:  particle asymptotic velocity
        float: FD: drag force
    """
    return u * FD, \
           "Drag power per particle"



##############################################################################################
def PD ( p1D, Nbub ):                                                                        #
##############################################################################################
    """Total drag power
        float: p1D:  drag power per particle
        float: Nbub: bubbles in the tank at any given time
    """
    return p1D * Nbub, \
           "Total drag power"



##############################################################################################
def Ptheo ( g, nNoz, q1, rhoL, hCol ):                                                       #
##############################################################################################
    """Total theoretical nozzle power at atmospheric pressure
        float: g:      acceleration of gravity
        float: nNoz:   number of nozzles
        float: q1:     flow rate per nozzle
        float: rhoL: liquid phase density
        float: hCol:   height of the bubble column
    """
    pAtm = 103000
    return nNoz * pAtm * q1 * math.log(1 + rhoL * g * hCol / pAtm), \
           "Total theoretical nozzle power at atmospheric pressure"



##############################################################################################
def u ( drag, ReP, FB, K, n, d, rhoL, uGuessIn=0.3 ):                                        #
##############################################################################################
    """Particle asymptotic velocity
        callable f( uGuess, d, nuGuess ): ReP:      particle Reynolds number
        callable f(ReP):                  drag:     function for drag coefficient 
        float:                            FB:       buoyancy force
        float:                            K:        power-law consistency coefficient
        float:                            n:        power-law index
        float:                            d:        particle diameter
        float:                            rhoL:     liquid phase density
        float:                            uGuessIn: initial guess for particle asymptotic velocity
    """
    def getU(uGuess): # returns zero when asymptotic drag equates buoyancy
        nuGuess, dummy = nu( K, n, d, uGuess )
        RePGuess, dummy = ReP( uGuess, d, nuGuess )
        FDGuess, dummy = FD( drag(RePGuess), rhoL, d, uGuess )
        return 1 - FDGuess / FB

    return fsolve(getU, uGuessIn)[0], \
           "Particle asymptotic velocity"



##############################################################################################
def uAvg ( u, V, Vcol ):                                                                     #
##############################################################################################
    """Liquid phase average velocity
        float: u:    particle asymptotic velocity
        float: V:    tank volume
        float: Vcol: bubble column volume
    """
    return u * Vcol / V, \
           "Liquid phase average velocity"



##############################################################################################
def V ( Hcyl, Hfrust, Dext, Dint ):                                                          #
##############################################################################################
    """Volume of the tank
        float: Hcyl:   cylinder height
        float: Hfrust: frustum height
        float: Dext:   cylinder diameter
        float: Dint:   low frustum diameter
    """
    return math.pi * Dext**2 * Hcyl / 4 + math.pi * Hfrust * (Dext**2 + Dext * Dint + Dint**2) / 12, \
           "Volume of the tank"



##############################################################################################
def Vcol ( d, hCol ):                                                                        #
##############################################################################################
    """Volume of the bubble column
       float: d:    particle diameter
       float: hCol: bubble column height
    """
    return math.pi * d**2 * hCol / 4, \
           "Volume of the bubble column"



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
