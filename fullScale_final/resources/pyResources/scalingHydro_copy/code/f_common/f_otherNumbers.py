import math



##############################################################################################
def Ar ( g, nu, rhoL, rhoP, d ):                                                             #
##############################################################################################
    """Archimedes number: g * d**3 * (rhoL - rhoP) / (nu**2 * rhoL)
        float: g:    acceleration of gravity
        float: nu:   liquid phase characteristic kinematic viscosity
        float: rhoL: liquid phase density
        float: rhoP: particle density
        fload: d:    particle diameter
    """
    return g * d**3 * (rhoL - rhoP) / (nu**2 * rhoL), \
           "Archimedes number: g * d**3 * (rhoL - rhoP) / (nu**2 * rhoL)"



##############################################################################################
def Fr ( g, d, u ):                                                                          #
##############################################################################################
    """Froude number: u / math.sqrt(g * d)
        float: g: acceleration of gravity
        fload: d: particle diameter
        float: u: particle asymptotic velocity
    """
    return u / math.sqrt(g * d), \
           "Froude number: u / math.sqrt(g * d)"



##############################################################################################
def Nq ( nNoz, q1, nu, d ):                                                                  #
##############################################################################################
    """Flow-rate number: nNoz * q1 / (nu *d)
        float: nNoz: number of nozzles
        float: q1:   flow rate per nozzle
        float: nu:   liquid phase characteristic kinematic viscosity
        fload: d:    particle diameter
    """
    return nNoz * q1 / (nu *d), \
           "Flow-rate number: nNoz * q1 / (nu *d)"



##############################################################################################
def PdNu ( PD, d, nu, rhoL ):
##############################################################################################
    """Power number: PD * d / (rhoL * nu**3)
        float: PD:   total drag power
        fload: d:    particle diameter
        float: nu:   liquid phase characteristic kinematic viscosity
        float: rhoL: liquid phase density
    """
    return PD * d / (rhoL * nu**3), \
           "Power number: PD * d / (rhoL * nu**3)"



##############################################################################################
def PdNuU ( PD, d, nu, u, rhoL ):
##############################################################################################
    """Power number: PD / (rhoL * d * nu * u**2)
        float: PD:   total drag power
        fload: d:    particle diameter
        float: nu:   liquid phase characteristic kinematic viscosity
        float: u:    particle asymptotic velocity
        float: rhoL: liquid phase density
    """
    return PD / (rhoL * d * nu * u**2), \
           "Power number: PD / (rhoL * d * nu * u**2)"



##############################################################################################
def PdU ( PD, d, u, rhoL ):
##############################################################################################
    """Power number: PD / (rhoL * d**2 * u**3)
        float: PD:   total drag power
        fload: d:    particle diameter
        float: u:    particle asymptotic velocity
        float: rhoL: liquid phase density
    """
    return PD / (rhoL * d**2 * u**3), \
           "Power number: PD / (rhoL * d**2 * u**3)"



##############################################################################################
def Re ( uAvg, Dext, nu ):                                                                   #
##############################################################################################
    """Reynolds number: uAvg * Dext / nu
        float: uAvg: liquid phase average velocity
        float: Dext: cylinder diameter
        float: nu:   liquid phase characteristic kinematic viscosity
    """
    return uAvg * Dext / nu, \
           "Reynolds number: uAvg * Dext / nu"



##############################################################################################
def ReP ( u, d, nu ):                                                                        #
##############################################################################################
    """Particle Reynolds number: u * d / nu
        float: u:  particle asymptotic velocity
        fload: d:  particle diameter
        float: nu: liquid phase characteristic kinematic viscosity
    """
    return u * d / nu, \
           "Particle Reynolds number: u * d / nu"



##############################################################################################
def rhoRatio ( rhoL, rhoP ):                                                                 #
##############################################################################################
    """Liquid-over-particle density ratio: rhoL / rhoP
        float: rhoL: liquid phase density
        float: rhoP: particle density
    """
    return rhoL / rhoP, \
           "Liquid-over-particle density ratio: rhoL / rhoP"



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
