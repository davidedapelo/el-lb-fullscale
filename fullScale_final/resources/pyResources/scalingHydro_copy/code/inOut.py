import os



##############################################################################################
def readDict ( folder, name ):                                                               #
##############################################################################################
    """Reading a generic dictionary from a folder and a file.
    
    Returns a dictionary variable from a file. Input file is structured as:
    key1 value1
    key2 value2
    ...  ...

    Args:
        str:       folder: folder containing the file
        str:       name:   filename

    Returns:
        dict: d:        dictionary
        dict: comments: comments to d's entries
    """

#---------- Housekeeping. --------------------------------------------------------------------
    if not os.path.isdir(folder):
        raise ValueError('Folder ' + str(folder) + ' does not exist or is not a folder.')

    if not os.path.exists(folder + '/' + name):
        raise ValueError('Folder ' + str(folder) + ' does not contain file ' + str(name) + ' .')

#---------- Scrolls file and creates float entries. ------------------------------------------
    d = dict() # Output dictionary variable initialized as empty
    comments = dict()

    for line in open(folder + '/' + name, 'r'):
        if len(line.split()) >= 2:
            d[line.split()[0]] = float(line.split()[1])
            comments[line.split()[0]] = ' '.join([ line.split()[i] for i in range( 2, len(line.split()) ) ])

    return d, comments



##############################################################################################
def readParameter ( folder, name, keys ):                                                    #
##############################################################################################
    """Read single-line parameter file.
    <key> <factor> -> the <value> parameter is scaled by a <value> multiplicative factor.
    <key> -> the <key> parameter is set for being kept constant / being alterated
    in iterative procedure (depending on the usage it is done in user code).
    If no file is present, returns (None, None).

    Args:
        str:       folder: folder containing the file
        str:       name:   filename
        list[str]: keys:   list of all and only the keys the file should have

    Returns:
        std:   varName: recurring variable to be scaled
        float: factor:  scaling factor (only if <value> is present in the input file
    """

#---------- Housekeeping. --------------------------------------------------------------------
    if not os.path.isdir(folder):
        raise ValueError('Folder ' + str(folder) + ' does not exist or is not a folder.')

#---------- Returns (None, None if file does not exist. --------------------------------------
    if not os.path.exists(folder + '/' + name):
        return None, None

    output = []

#---------- Reads the file. ------------------------------------------------------------------
    for line in open(folder + '/' + name, 'r'):
        for item in line.split():
            output.append(item)

#---------- Returning. -----------------------------------------------------------------------
    return output



##############################################################################################
def writeDict (t, folder, name ):                                                            #
##############################################################################################
    """Writes a generic dictionary to file in the format:
    key1 value1
    key2 value2
    ...  ...

    Args:
        tuple(t[0], t[1]): t
            dict: t[0]: dictionary
            dict: t[1]: comments to t[0]'s entries
        str:  folder: folder containing the target file
        str:  name:   target filename
    """

    nameFull = folder + '/' + name
    os.makedirs(os.path.dirname(nameFull), exist_ok=True)
    with open(nameFull, 'w') as f:
        for key in t[0].keys():
            f.write( f'{key:10}  {t[0][key]:22}  {t[1][key]}\n' )



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
