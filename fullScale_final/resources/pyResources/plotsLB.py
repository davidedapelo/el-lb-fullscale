import sys
import matplotlib.pyplot as plt
import numpy as np

import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC
sys.stdout = open('pyOut_plotsLB.txt', 'a+') # Redirecting stdout
sys.stderr = open('pyErr_plotsLB.txt', 'a+') # Redirecting stderr



##############################################################################################



if len(sys.argv) >= 9:
    # Defining variables from the whole set of argv.
    file_out = str(sys.argv[1])
    x1       = np.genfromtxt( str(sys.argv[2]) ) # x or y
    x2       = np.genfromtxt( str(sys.argv[3]) ) # y or z
    ux       = np.genfromtxt( str(sys.argv[4]) )
    uy       = np.genfromtxt( str(sys.argv[5]) )
    uz       = np.genfromtxt( str(sys.argv[6]) )
    uLegend  = float(sys.argv[7])
    axisFlag = str(sys.argv[8])

    if len(sys.argv) == 10:
        normFactor = ord(sys.argv[9]) if sys.argv[9] == '\x01' else float(sys.argv[9]) # makes funny things if sys.argv[9] == 1
    else:
        normFactor = 1.

    for i in list(range(9)):
        print ( 'argv[' + str(i) + '] = ' + str(sys.argv[i]) )
    print('normFactor = ' + str(normFactor) + '\n')

else:
    raise ValueError( "plotLB.py: error in sys.argv. 9 or 10 allowed, but input was " + str(len(sys.argv)) )
    # Defining variables from only folder containing the run
    # and the maximum value of velocity magnitude.
    dirName = str(sys.argv[1])
    file_out = dirName + "/tmp/dataPlot.png"
    x1       = np.genfromtxt( dirName + "/tmp/data_rawData/coordsY_0.000000__x.dat" )
    x2       = np.genfromtxt( dirName + "/tmp/data_rawData/coordsY_0.000000__z.dat" )
    ux       = np.genfromtxt( dirName + "/tmp/data_rawData/0x.txt" )
    uy       = np.genfromtxt( dirName + "/tmp/data_rawData/0z.txt" )
    uz       = np.genfromtxt( dirName + "/tmp/data_rawData/0z.txt" )
    uLegend  = float(sys.argv[2])
    axisFlag = 'y'

#---------- Plane orthogonal to axis Y. ------------------------------------------------------
# x1=x, x2=z, u1=ux, u2=uz, uMag=sqrt(ux**2+uz**2)
if axisFlag == 'y':
    uMag = np.sqrt(ux**2 + uz**2) * normFactor
    pt.plotPlane(file_out, x1, x2, ux, uz, uMag, 0, uLegend)

#---------- Plane orthogonal to axis Z. ------------------------------------------------------
# x1=x, x2=y, u1=ux, u2=uy, uMag=sqrt(ux**2+uy**2+uz**2)
elif axisFlag == 'z':
    uMag = np.sqrt(ux**2 + uy**2 + uz**2) * normFactor
    pt.plotPlane(file_out, x1, x2, ux, uy, uMag, 0, uLegend)

#---------- Default. -------------------------------------------------------------------------
else :
    raise ValueError('plotLB.py: axixFlag allowed values are y or z but instead it was' + axisFlag)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
