import sys
import matplotlib.pyplot as plt
import numpy as np

import pyResources.dataElaboration as de
import pyResources.filesHandling as fh
import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC
sys.stdout = open('pyOut_vortexLB_3cm.txt', 'a+') # Redirecting stdout
sys.stderr = open('pyErr_vortexLB_3cm.txt', 'a+') # Redirecting stderr



##############################################################################################

fh.checkArgs([14])

if len(sys.argv) == 14:
    # Defining variables from the whole set of argv.
    file_out = str(sys.argv[1])
    x1       = np.genfromtxt( str(sys.argv[2]) )
    x2       = np.genfromtxt( str(sys.argv[3]) )
    ux       = np.genfromtxt( str(sys.argv[4]) )
    uz       = np.genfromtxt( str(sys.argv[5]) )
    uLegend  = float(sys.argv[6])
    uFactor  = float(sys.argv[7])
    xMin     = float(sys.argv[8])
    xMax     = float(sys.argv[9])
    yMin     = float(sys.argv[10])
    yMax     = float(sys.argv[11])
    t        = float(sys.argv[12])
    flag     = str(sys.argv[13])
    for i in list(range(14)):
        print ( 'argv[' + str(i) + '] = ' + str(sys.argv[i]) )
    print('')

x, y = de.computeVortexLB(x1, x2, np.sqrt(ux**2 + uz**2), xMin, xMax, yMin, yMax)
with open(file_out+'.txt', 'a') as fileWriter:
    fileWriter.write( str(t) + ' ' + str(x) + ' ' + str(y) + '\n' )

if flag == 'y':
    plt.plot(x, y, 'r+', markersize=30)
    pt.plotPlane(file_out+'.pdf', x1, x2, ux, uz,  np.sqrt(ux**2 + uz**2), uLegend)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
