
import sys
import matplotlib.pyplot as plt
import numpy as np

import pyResources.dataElaboration as de
import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC



##############################################################################################



if len(sys.argv) == 12:
    # Defining variables from the whole set of argv.
    file_out = str(sys.argv[1])
    x1       = np.genfromtxt( str(sys.argv[2]) )
    x2       = np.genfromtxt( str(sys.argv[3]) )
    ux       = np.genfromtxt( str(sys.argv[4]) )
    uz       = np.genfromtxt( str(sys.argv[5]) )
    uLegend  = float(sys.argv[6])
    xMin     = float(sys.argv[7])
    xMax     = float(sys.argv[8])
    yMin     = float(sys.argv[9])
    yMax     = float(sys.argv[10])
    flag     = str(sys.argv[11])

else:
    if len(sys.argv) == 8:
        dirName = str(sys.argv[1])
        uLegend = float(sys.argv[2])
        xMin    = float(sys.argv[3])
        xMax    = float(sys.argv[4])
        yMin    = float(sys.argv[5])
        yMax    = float(sys.argv[6])
        flag    = str(sys.argv[7])
    else:
        if len(sys.argv) == 2:
            flag = str(sys.argv[1])
        else:
            flag = 'n'

        dirName = "../../git_ignore/data_rawData"
        uLegend = 0.02
        xMin    = 0.02
        xMax    = 0.09
        yMin    = 0.0
        yMax    = 0.09

    file_out = dirName + "/uAvgSelection2s_40.png"
    x1       = np.genfromtxt( dirName + "/coordsY_0.029041__x.dat" )
    x2       = np.genfromtxt( dirName + "/coordsY_0.029041__z.dat" )
    ux       = np.genfromtxt( dirName + "/velY_0.029041__6360__x.dat" )
    uz       = np.genfromtxt( dirName + "/velY_0.029041__6360__z.dat" )

u = np.sqrt(ux**2 + uz**2)

uAvgTot, uAvgPart = de.computeAvgVelocitiesLB(x1, x2, u, xMin, xMax, yMin, yMax)
print( str(uAvgTot) + ' ' + str(uAvgPart) )

if flag == 'y':
    plt.plot(np.array([xMin, xMin]), np.array([yMin, yMax]), 'r')
    plt.plot(np.array([xMax, xMax]), np.array([yMin, yMax]), 'r')
    plt.plot(np.array([xMin, xMax]), np.array([yMin, yMin]), 'r')
    plt.plot(np.array([xMin, xMax]), np.array([yMax, yMax]), 'r')
    pt.plotPlane(file_out, x1, x2, ux, uz,  u, uLegend)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
