import sys
import matplotlib.pyplot as plt
import numpy as np

import pyResources.dataElaboration as de
import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC



##############################################################################################



if len(sys.argv) == 12:
    # Defining variables from the whole set of argv.
    file_out = str(sys.argv[1])
    x1       = np.genfromtxt( str(sys.argv[2]) )
    x2       = np.genfromtxt( str(sys.argv[3]) )
    ux       = np.genfromtxt( str(sys.argv[4]) )
    uz       = np.genfromtxt( str(sys.argv[5]) )
    uLegend  = float(sys.argv[6])
    xMin     = float(sys.argv[7])
    xMax     = float(sys.argv[8])
    yMin     = float(sys.argv[9])
    yMax     = float(sys.argv[10])
    flag     = str(sys.argv[11])

else:
    if len(sys.argv) == 8:
        dirName = str(sys.argv[1])
        uLegend = float(sys.argv[2])
        xMin    = float(sys.argv[3])
        xMax    = float(sys.argv[4])
        yMin    = float(sys.argv[5])
        yMax    = float(sys.argv[6])
        flag    = str(sys.argv[7])
    else:
        if len(sys.argv) == 2:
            flag = str(sys.argv[1])
        else:
            flag = 'n'

        dirName = "../../git_ignore/cvA1_1_200-0"
        # 4s_40
        uLegend = 0.08
        xMin    = 0.
        xMax    = 0.075
        yMin    = 0.08
        yMax    = 0.12

    file_out = dirName + "/tmp/vortexPlot.png"
    x1       = np.genfromtxt( dirName + "/tmp/data_rawData/coordsY_0.000000__x.dat" )
    x2       = np.genfromtxt( dirName + "/tmp/data_rawData/coordsY_0.000000__z.dat" )
    ux       = np.genfromtxt( dirName + "/tmp/data_rawData/velY_0.000000__32070__x.dat" )
    uz       = np.genfromtxt( dirName + "/tmp/data_rawData/velY_0.000000__32070__z.dat" )

x, y = de.computeVortexPIV(x1, x2, np.sqrt(ux**2 + uz**2), xMin, xMax, yMin, yMax)
print( str(x) + ' ' + str(y) )

if flag == 'y':
    plt.plot(x, y, 'r+')
    pt.plotPlane(file_out, x1, x2, ux, uz,  np.sqrt(ux**2 + uz**2), uLegend)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
