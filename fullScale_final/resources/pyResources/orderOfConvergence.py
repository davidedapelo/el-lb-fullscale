import os
import sys
import matplotlib.pyplot as plt

import pyResources.dataElaboration as de
import pyResources.filesHandling as fh
import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC
#sys.stdout = open('pyOut_orderOfConvergence.txt', 'a+') # Redirecting stdout
#sys.stderr = open('pyErr_orderOfConvergence.txt', 'a+') # Redirecting stderr


##############################################################################################

#---------- Checking the number of arguments. ------------------------------------------------
fh.checkArgs([2])

#---------- input data. ----------------------------------------------------------------------
dataFolder = str(sys.argv[1]) # folder containing all the input - output data

maxN = int(open(dataFolder + "/maxN.txt", 'r').read()) # maximum value of N
k  = float(open(dataFolder + "/eoc.txt", 'r').read().split(' ')[0]) # Convergence sample line: x = k*(1/N)^n
n  = float(open(dataFolder + "/eoc.txt", 'r').read().split(' ')[1]) # Convergence sample line: x = k*(1/N)^n

#---------- Building list of grid numbers. ---------------------------------------------------
N = [ int(item.split('.')[0]) for item in os.listdir(dataFolder + "/vortexCoords") if int(item.split('.')[0]) < maxN ]
N.sort()

#---------- Building list of vortex's x coordinates. -----------------------------------------
x =  [ float(open(dataFolder + "/vortexCoords/" + str(item) + ".txt", 'r').read().split(' ')[0]) for item in N ]
maxX = float(open(dataFolder + "/vortexCoords/" + str(maxN) + ".txt", 'r').read().split(' ')[0])

#---------- Errors. --------------------------------------------------------------------------
err = [ abs(item-maxX) for item in x ]



#========== Plotting. ========================================================================

#---------- initializing the figure. ---------------------------------------------------------
plt.rcParams.update({'font.size': 14})
fig = plt.figure() # create a figure object

#---------- cleaning from possible previous stuff on fig. ------------------------------------
fig.clf()

#---------- x-y figure. ----------------------------------------------------------------------
ax1 = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax1.set_xlabel('Voxels per unit length')
ax1.set_ylabel('Error (m)')
ax1.loglog( N, err, '-X' );
ax1.loglog( N, [ k*(1./item)**n for item in N ], '--' );

fig.savefig(dataFolder + "/test.pdf", bbox_inches='tight', format='pdf')
