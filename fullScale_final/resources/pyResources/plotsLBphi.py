import sys
import matplotlib.pyplot as plt
import numpy as np

import pyResources.plotting as pt

plt.switch_backend('agg') # Added to solve issues on HPC
sys.stdout = open('pyOut_plotsLBphi.txt', 'a+') # Redirecting stdout
sys.stderr = open('pyErr_plotsLBphi.txt', 'a+') # Redirecting stderr



##############################################################################################



if len(sys.argv) == 7:
    # Defining variables from the whole set of argv.
    file_out = str(sys.argv[1])
    x1       = np.genfromtxt( str(sys.argv[2]) ) # x
    x2       = np.genfromtxt( str(sys.argv[3]) ) # z
    phi      = np.genfromtxt( str(sys.argv[4]) )
    vInf     = float(sys.argv[5])
    vSup     = float(sys.argv[6])
else:
    raise ValueError( "plotLB.py: error in sys.argv: 7 allowed, but input was " + str(len(sys.argv)) )

for i in list(range(7)):
    print ( 'argv[' + str(i) + '] = ' + str(sys.argv[i]) )
print('')

pt.plotPhiPlane(file_out, x1, x2, phi, vInf, vSup)



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
