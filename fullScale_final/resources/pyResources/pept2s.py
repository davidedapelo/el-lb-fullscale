import sys
import numpy as np

import pyResources.plotting as pt
import pyResources.dataElaboration as de
import pyResources.filesHandling as fh



##############################################################################################

fileOut  = '../../git_ignore/0expDataPEPT/figures/2s_40.pdf'
fileData = '../../git_ignore/0expDataPEPT/data/aa01.txt'
fileDict = '../../git_ignore/0expDataPEPT/dictionaries/aa01.txt'

d = fh.readDictFile(fileDict)
for key in d:
    d[key] = float(d[key])
d['Nr'] = int(d['Nr'])
d['Ny'] = int(d['Ny'])

x, y, z, t = de.createPEPTtrajectory (fileData, d)

xx, yy, Ur, Uy, Umag = de.createPEPTfields( x, y, z, t, d )

pt.plotPlane(fileOut, xx, yy, Ur, Uy, Umag, d['uLegend'])



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
