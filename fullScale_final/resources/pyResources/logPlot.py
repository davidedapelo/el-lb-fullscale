import sys
import numpy as np
import matplotlib.pyplot as plt



##############################################################################################

if len(sys.argv) == 6:
    matrixInput = np.genfromtxt( str(sys.argv[1]) )
    fileOutX  = str(sys.argv[2])
    fileOutY  = str(sys.argv[3])
    xRef      = float(sys.argv[4])
    yRef      = float(sys.argv[5])
    variation = 'y'

elif len(sys.argv) == 4:
    matrixInput = np.genfromtxt( str(sys.argv[1]) )
    fileOutX  = str(sys.argv[2])
    fileOutY  = str(sys.argv[3])
    variation = 'n'

else:
    matrixInput = np.genfromtxt( '../../git_ignore/cvA1_1_images/vortexCoords.txt' )
    fileOutX  = '../../git_ignore/cvA1_1_images/vortexCoordsX.png'
    fileOutY  = '../../git_ignore/cvA1_1_images/vortexCoordsY.png'
    variation = 'n'

N = matrixInput[:,0]
x = matrixInput[:,1]
y = matrixInput[:,2]

if variation == 'y':
    x = abs(x-xRef) / xRef
    y = abs(y-yRef) / yRef
    vx = np.polyfit( 1/N, x, 1)
    vy = np.polyfit( 1/N, y, 1)
    print(vx)
    print(vy)
    px = np.poly1d( np.polyfit( 1/N, x, 1) )
    py = np.poly1d( np.polyfit( 1/N, y, 1) )

plt.clf()
plt.loglog(N, x,)
plt.loglog(N, x, '+')
if variation == 'y':
    plt.loglog(N, px(1/N))
plt.savefig(fileOutX, format='png')

plt.clf()
plt.loglog(N, y)
plt.loglog(N, y, '+')
if variation == 'y':
    plt.loglog(N, px(1/N))
plt.savefig(fileOutY, format='png')



##############################################################################################
################################################################   End of file.   ############
##############################################################################################
