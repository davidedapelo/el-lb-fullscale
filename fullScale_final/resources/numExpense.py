#! /usr/bin/python3

import matplotlib.pyplot as plt
import pandas as pd
import sys
from glob import glob
from pathlib import Path

###############################################################################################################################
def extractDataFromLogFile (logFile):
###############################################################################################################################
    """
    Extracts the data from a logfile.
    Args:
        logFile: str: Log file name
    Returns:
        Pandas DataFrame with the following entries:
            [index]  timestep: int:   Timestep
            [column] Actual:   float: Actual passed time (s)
            [column] Ideal:    float: Ideal passed time (s)
    """
    data = {'timestep': [], 'Actual': []}
    particles = []
    for line in open(logFile, 'r'):
        content = line.rstrip()
        if '[Timer] step' in content:
            splittedContent = content.split(';')
            data['timestep'].append(int( splittedContent[0].split('=')[1] ))
            data['Actual'].append(float( splittedContent[2].split('=')[1] ))
        if 'Global number of active particles' in content:
            particles.append(int( content.split(':')[1] ))

    df = pd.DataFrame(data=data).set_index('timestep')

    def idealLinearPassedTime(x):
        t0, t1 = 1, 2
        m = (df.Actual[df.index[t1]] - df.Actual[df.index[t0]])/(df.index[t1] - df.index[t0])
        return m*(x - df.index[t0]) + df.Actual[df.index[t0]]
    df['Ideal'] = idealLinearPassedTime(df.index)

    df['Active particles'] = df.index*0 if particles == [] else particles

    return df


###############################################################################################################################
def getLastFile (listOfFiles):
###############################################################################################################################
    """
    Returns the last occurrence of a list of file names following the convention <parentFolder>/<base>-<NoOfOccurrences>.<ext>
    Args:
        listOfFiles: str: Partial file names [ <parentFolder> / <base> ]
    Returns:
        (...):       str: Folder name of the last occurrence following the convention <parentFolder> / <index>-<lastOccurrence>.<ext>
    """
    return sorted(
            glob(listOfFiles+'*'),
            key = lambda item: int( Path(item).name.split('-')[1].split('.')[0] )
            )[-1]


###############################################################################################################################
def plotStuff(df, filename, alter1stLine, titleX, titleY, location):
###############################################################################################################################
    """
    Plots the passed time against the timestep
    Args:
        df: Pandas DataFrame with the following entries:
            [index]  timestep: int:   Timestep
            [column] Actual:   float: Actual passed time (s)
            [column] Ideal:    float: Ideal passed time (s)
        filename:     str: Output filename (full path)
        alter1stLine: bool: True  -> Passed time: the 1st line ('Actual') gets altered marks
                            False -> Particle number: No action required
        titleX:       str: Title of the X axis
        titleY:       str: Title of the Y axis
        location:     str: Legend location
    """
    print("Plotting time sequences titled '" + titleY + "' ...")
    ax = df.plot()
    if alter1stLine:
        ax.get_lines()[0].set_marker('X')
        ax.get_lines()[0].set_linestyle('None')
    plt.legend(frameon=False, loc=location)
    plt.xlabel(titleX)
    plt.ylabel(titleY)
    plt.tight_layout()
    plt.savefig(filename, format='pdf')
    plt.close()


###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 2:
        raise ValueError('Exactly ONE argv argument (==path of the run folder) must by inputted. Quitting.')
    if not Path(sys.argv[1]).is_dir():
        raise ValueError('Folder ' + sys.argv[1] + ' does not exist or is not a directory. Quitting.')

    logFile = getLastFile(sys.argv[1]+'/log')
    df = extractDataFromLogFile(logFile)
    plotStuff(df[['Actual', 'Ideal' ]], sys.argv[1]+'/passedTime.pdf', True,  'Timestep', 'Passed time (s)',            'upper left' )
    plotStuff(df[['Active particles']], sys.argv[1]+'/particleNo.pdf', False, 'Timestep', 'Number of active particles', 'lower right')
