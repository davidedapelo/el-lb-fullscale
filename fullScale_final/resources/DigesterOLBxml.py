import math as m
from enum import Enum
from scipy.optimize import fsolve
from xml.dom import minidom

from resources.OLBxml import OLBxml

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class DigesterOLBxml(OLBxml):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Extension to the OLBxml class to anaerobic digesters with geometry described through an xml document
    and power-law geometry for sludge.
    """
    _flowRateMax = 1.4151e-2
    _plIndex        = [ 0.710, 0.562, 0.533 ]
    _plConsistCoeff = [ 0.042e-3, 0.192e-3, 0.525e-3 ]

###############################################################################################################################
    def __init__(self, data, **kwargs):
###############################################################################################################################
        """
        Initializes the class and imports parameters corresponding to geometry, dynamics and setup.
        """
        super().__init__(**kwargs)
        if 'phi_diffusivity' in data:
            kwargs['phi_diffusivity'] = data['phi_diffusivity']
        if 'phi1' in data:
            kwargs['phi1'] = data['phi1']
        self.__importGeometryAndPostProcessing(data['seedingType'], **kwargs)
        self.__defineDynamics(
                n               = DigesterOLBxml._plIndex[data['rheoIndex']],
                k               = DigesterOLBxml._plConsistCoeff[data['rheoIndex']],
                viscoCorrection = data['viscoCorrection'],
                diamStr         = data['diamStr'],
                **kwargs
                )
        self.__defineSetup(
                nx                  = data['nx'],
                nx0                 = data['nx0'],
                subcycles           = data['subcycles'],
                uLB0                = data['uLB0'],
                physViscosity       = float(self.getNodeContent('physViscosity')),
                viscoHalfSpan       = data['viscoHalfSpan'],
                antiDiffusionTuning = data['antiDiffusionTuning'] if 'antiDiffusionTuning' in data else 0.,
                **kwargs
                )

###############################################################################################################################
    def __importGeometryAndPostProcessing(self, seedingType='seed', **kwargs):
###############################################################################################################################
        """
        Imports the geometry and post-processing parameters to the xml document.
        Args:
            seedingType: str: 'seed' -> small seed uniformly distributed
                              'ball' -> single ball near the inlet
        """
        self._importFromFile(
                kwargs['input_geometry'] if 'input_geometry'in kwargs
                else (self.dir_resources + 'input_geometry.xml')
                )
        self._importFromFile(
                kwargs['input_postProcessing'] if 'input_postProcessing' in kwargs
                else (self.dir_resources + 'input_postProcessing.xml')
                )
        self._defineNode( 'seedingType', self._root.getElementsByTagName('postProcessing')[0], seedingType )

###############################################################################################################################
    def __defineDynamics(self, n, k, viscoCorrection, diamStr, **kwargs):
###############################################################################################################################
        """
        Defines the dynamics, consisting of rheology and bubble size, and imports it to the XMl document.
        Args:
            n:               float:  Power-law coefficient
            k:               float:  Power-law consistency coefficient
            viscoCorrection: float:  Relative correction to char viscosity as computed from bubble column shear rate
            diamStr:         str:    Bubble diameter in mm, string
        """
        self._importFromFile(
                kwargs['input_dynamics'] if 'input_dynamics' in kwargs
                else (self.dir_resources + 'input_dynamics.xml')
                )
        diam_mm  = float(diamStr)
        diam_m   = diam_mm/1000
        g        = float( self.getNodeContent('g') )
        rhoL     = float( self.getNodeContent('rhoL') )
        rhoP     = float( self.getNodeContent('rhoPnominal') )
        uLegend0 = float( self.getNodeContent('uLegend0') )

#==============================================================================================================================
        def charNu ( k, n, d, u ):
#==============================================================================================================================
            """
            Computes the characteristic viscosity.
            Args:
                k:    float: Power-law consistency coefficient
                n:    float: Power-law index
                d:    float: Bubble diameter (m)
                u:    float: Rising bubble velocity
            Returns:
                (...) float: Characteristic viscosity
            """
            return k * (2 * u/ d)**(n - 1)

#==============================================================================================================================
        def getU(uGuess):
#==============================================================================================================================
            """
            returns rising bubble velocity when asymptotic drag equates buoyancy.
            Args:
                uGuess: float: initial guess for velocity
            Returns:
                (...):  float: asymptotic bubble velocity
            """

#------------------------------------------------------------------------------------------------------------------------------
            def dragCoeffMorsi(ReP):
#------------------------------------------------------------------------------------------------------------------------------
                """
                Drag coefficient as in Morsi and Alexander (1972).
                Args:
                    ReP:   float: Particle Reynolds number
            Returns:
                    (...): float: Drag coefficient
                """
                if ReP <= 0:
                    raise ValueError('Non-positive ReP (' + str(ReP) + ').')
                if ReP < 0.1:
                    a = [ 0.0,     24.0,     0.0       ]
                elif ReP < 1.0:
                    a = [ 3.69,    22.73,     0.0903   ]
                elif ReP < 10.0:
                    a = [ 1.222,   29.16667, -3.8889   ]
                elif ReP < 100.0:
                    a = [ 0.6167,  46.5,     -116.67   ]
                elif ReP < 1000.0:
                    a = [ 0.3644,  98.33,    -2778     ]
                elif ReP < 5000.0:
                    a = [ 0.357,   148.62,   -4.75e4   ]
                elif ReP < 10000.0:
                    a = [ 0.46,   -490.546,   57.87e4  ]
                else:
                    a = [ 0.5191, -1662.5,    5.4167e6 ]
                return ( a[0] + a[1]/ReP + a[2]/(ReP*ReP) )

            RePGuess = uGuess * diam_m / charNu(k, n, diam_m, uGuess)
            fDragGuess = m.pi * dragCoeffMorsi(RePGuess) * rhoL * diam_m**2 * uGuess**2 / 8
            fBuoyancy = m.pi * diam_m**3 * abs(rhoL - rhoP) * g / 6
            return 1 - fDragGuess / fBuoyancy

        charU = fsolve(getU, uLegend0)[0]
        physViscosity = charNu(k, n, diam_m, charU) * viscoCorrection**(n-1) # inclusive of heuristic correction

        dynamics = self._root.getElementsByTagName('dynamics')[0]
        self._defineNode( 'n',              dynamics, str(n)             )
        self._defineNode( 'm',              dynamics, str(k)             )
        self._defineNode( 'd',              dynamics, str(diam_mm)       )
        self._defineNode( 'charU',          dynamics, str(charU)         )
        self._defineNode( 'uRef',           dynamics, str(charU)         )
        self._defineNode( 'physViscosity',  dynamics, str(physViscosity) )
        if 'phi_diffusivity' in kwargs:
            self._defineNode( 'phi_diffusivity', dynamics, str(kwargs['phi_diffusivity']) )
        if 'phi1' in kwargs:
            self._defineNode( 'phi1', dynamics, str(kwargs['phi1']) )

###############################################################################################################################
    def _defineInletOutlet(self, node, residenceTime, feedingInterval, feedingTime, Tmax=None, prefix=''):
###############################################################################################################################
        """
        Defines the inlet and outlet.
        <feedingInterval> hours feed in <feedingTime> minutes.
        In other words: <nominalFlowRate> =  <volume> / <residenceTime>;
        <inletOutletFlowRate = <nominalFlowRate> * <feedingInterval> / <feedingTime>;
        <inletOutletFlowRate> is injected for the first <feedingTime>, and then 0 for <feedingInterval> - <feedingTime>.
        Args:
            node:            xml.dom.minidom.Element:  Created node
            residenceTime:   float:                    Hydraulic residence time (days)
            feedingInterval: float:                    Feeding Interval (hours)
            feedingTime:     float:                    Feeding time (minutes)
            Tmax:            float:                    Maximum time cutoff (relative)
            prefix:          str:                      Prefix to subnode names to avoid repetitions
        """
        rExt = float(self.getNodeContent("dExt")) / 2.
        rInt = float(self.getNodeContent("dInt")) / 2.
        h    = float(self.getNodeContent("h"))
        h0   = float(self.getNodeContent("h0"))

        volume = m.pi * rExt**2 * h + m.pi * h0 * (rExt**2 + rInt**2 + rExt*rInt) / 3.
        nominalFlowRate = volume / (residenceTime * 24 * 3600)
        actualFlowRate = nominalFlowRate * (feedingInterval * 3600) / (feedingTime * 60)

        self._defineNode( prefix+'feedingInterval',     node, str(feedingInterval*3600) )
        self._defineNode( prefix+'feedingTime',         node, str( feedingTime*60 if Tmax==None else (min(feedingTime*60, Tmax)) ) )
        self._defineNode( prefix+'inletOutletFlowRate', node, str(actualFlowRate)       )

###############################################################################################################################
    def __defineBaseRingInjectors(self, node, nNozzles, r1, posZ, equivD, scaling, prefix):
###############################################################################################################################
        """
        Defines the position of a generic set of injectors places in ring around the digester's central axis
        and imports them to the XMl document at the desired node.
        Args:
            node:     xml.dom.minidom.Element:  Created node
            nNozzles: int:                      Number of nozzles
            r1:       float:                    Distance of nozzles from central axis
            posZ:     float:                    Vertical distance of nozzle from floor
            equivD:   float:                    Nozzle's equivalent diameter
            scaling:  float:                    Scaling value for bubble diameter
            prefix:   str:                      Prefix to subnode names to avoid repetitions
        """
        if nNozzles < 1:
            raise ValueError('There must be at least 1 nozzle.')
        self._defineNode( prefix+'injEquivD',  node, str(equivD) )
        self._defineNode( prefix+'injPosX',    node, ' '.join([str( m.sin(2*i*m.pi/nNozzles)*r1) for i in range(nNozzles)]) )
        self._defineNode( prefix+'injPosY',    node, ' '.join([str( m.cos(2*i*m.pi/nNozzles)*r1) for i in range(nNozzles)]) )
        self._defineNode( prefix+'injPosZ',    node, ' '.join([str(posZ)                         for i in range(nNozzles)]) )
        self._defineNode( prefix+'injScaling', node, ' '.join([str(scaling)                      for i in range(nNozzles)]) )

###############################################################################################################################
    def _defineOrigInjectors(self, node, prefix):
###############################################################################################################################
        """
        Defines the position of the 'Orig', injectors for mixing,
        and imports them to the XMl document at the desired node.
        Args:
            node:    xml.dom.minidom.Element:  Created node
            prefix:  str:                      Prefix to subnode names to avoid repetitions
        Returns:
            (...): int: Number of nozzles
        """
        nNozzles = 12
        r1 = 1.83
        self.__defineBaseRingInjectors (
                node     = node,
                nNozzles = nNozzles,
                r1       = r1,
                posZ     = 0.3,
                equivD   = 0.09351261515560935,
                scaling  = 1.0,
                prefix   = prefix
                )
        return nNozzles

###############################################################################################################################
    def _defineMixing(self, node, injectorFunction, flowRateRatioStr, t0, t1, t2, prefix=''):
###############################################################################################################################
        """
        Defines the mixing protocol, and imports it to the XMl document.
        This is the original version, when all the nozzles are switched on at zero, and switched off at the end of mixing time.
        Args:
            node:             xml.dom.minidom.Element:  Created node
            injectorFunction: callable:                 Function defining the injector positions
            flowRateRatioStr: str:                      Fraction of _flowRateMax gas flow rate per nozzle: '05' -> 0.5 etc.
            t0:               float:                    Injection starting time (relative)
            t1:               float:                    Injection stopping time (relative)
            t2:               float:                    Simulation end time (relative)
            prefix:           str:                      Prefix to subnode names to avoid repetitions
        """
        nNozzles = injectorFunction(node, prefix)
        self._defineNode( prefix+'flowRateValue', node, str(0.1 * float(flowRateRatioStr) * self._flowRateMax) )
        if not prefix == '':
            self._defineNode( prefix+'Tmax', node, str(t2) )
        self._defineNode( prefix+'injT0',         node, ' '.join([str(t0) for i in range(nNozzles)]) )
        self._defineNode( prefix+'injT1',         node, ' '.join([str(t1) for i in range(nNozzles)]) )

###############################################################################################################################
    def __defineSetup(self, nx, nx0, subcycles, uLB0, physViscosity, viscoHalfSpan, antiDiffusionTuning=0., **kwargs):
###############################################################################################################################
        """
        Defines the numerical setup, and imports it to the XMl document.
        Args:
            nx:                  int:   Number of lattice spaces through X direction
            nx0:                 int:   Number of lattice spaces through X direction: unscaled value
            subcycles:           int:   Number of Lagrangian substeps
            uLB0:                float: Velocity in lattice units: unscaled value
            physViscosity:       float: Characteristic viscosity
            viscoHalfSpan:       float: nuMin = physViscosity/viscoHalfSpan; nuMax = physViscosity*viscoHalfSpan
            antiDiffusionTuning: float: anti-diffusion tuning constant: 0 -> no anti-diffusion; 1 -> full anti-diffusion
        """
        self._importFromFile(
                kwargs['input_setup'] if 'input_setup' in kwargs
                else (self.dir_resources + 'input_setup.xml')
                )

        setup = self._root.getElementsByTagName('setup')[0]
        self._defineNode( 'Nx',                  setup, str(nx)                          )
        self._defineNode( 'subCycles',           setup, str(subcycles)                   )
        self._defineNode( 'latticeU',            setup, str(uLB0 * nx0 / nx)             )
        self._defineNode( 'nuMax',               setup, str(physViscosity*viscoHalfSpan) )
        self._defineNode( 'nuMin',               setup, str(physViscosity/viscoHalfSpan) )
        self._defineNode( 'antiDiffusionTuning', setup, str(antiDiffusionTuning)         )

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class OrigDigesterOLBxml(DigesterOLBxml):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Extension of the DigesterOLBxml class to the original mixing system:
    viz.: continuous injection through all the nozzles
    """

###############################################################################################################################
    def __init__(self, data, **kwargs):
###############################################################################################################################
        """
        Initializes the class and imports parameters corresponding to mixing.
        """
        super().__init__(data, **kwargs)
        self._defineMixing(
                node             = self._defineNode('injectors'),
                injectorFunction = self._defineOrigInjectors,
                flowRateRatioStr = data['flowRateRatioStr'],
                t0               = 0.,
                t1               = float(self.getNodeContent('Tmax')),
                t2               = float(self.getNodeContent('Tmax'))
                )

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class OrigIODigesterOLBxml(DigesterOLBxml):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Extension of the DigesterOLBxml class to the original mixing system for inlet-outler:
    viz.: dummy continuous injection through all the nozzles (starts after maximum simulation time),
    and inlet-outlet for sludge throughout the simulation time
    """

###############################################################################################################################
    def __init__(self, data, **kwargs):
###############################################################################################################################
        """
        Initializes the class and imports parameters corresponding to mixing.
        """
        super().__init__(data, **kwargs)
        self._defineInletOutlet(
                node            = self._defineNode('recirculation'),
                residenceTime   = data['residenceTime'],
                feedingInterval = data['feedingInterval'],
                feedingTime     = data['feedingTime']
                )
        self._defineMixing(
                node             = self._defineNode('injectors'),
                injectorFunction = self._defineOrigInjectors,
                flowRateRatioStr = data['flowRateRatioStr'],
                t0               = 0.,
                t1               = float(self.getNodeContent('Tmax')),
                t2               = float(self.getNodeContent('Tmax'))
                )

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class CyclicOperationDigesterOLBxml(DigesterOLBxml):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Extension of the DigesterOLBxml class to digesters undertaking cyclic operations as per in Operation class
    """

###############################################################################################################################
    def __init__(self, data, **kwargs):
###############################################################################################################################
        """
        Initializes the class and imports parameters corresponding to mixing.
        """
        super().__init__(data, **kwargs)

        # Creating node containing all the operations
        operations = self._defineNode('operations')
        self._defineNode('runType', operations, (str(data['runType']) if 'runType' in data else '1') )
        self._defineNode('cyclesOfOperations', operations, (str(data['cyclesOfOperations']) if 'cyclesOfOperations' in data else '1') )
        self._defineNode('noOfOperations', operations, str(len(data['operations'])))

        # Unpacking operation list
        for i in range(len(data['operations'])):
            # Define a subnode per operation
            prefix = 'operation_'+str(i+1)+'_'
            node = self._defineNode(prefix[:-1], operations)
            self._defineNode(prefix+'type', node, data['operations'][i]['type'])

            # quiet: just set Tmax (relative)
            if data['operations'][i]['type'] == 'quiet':
                self._defineNode(prefix+'Tmax', node, str(data['operations'][i]['Tmax']))

            # recirculating: inlet/outlet
            elif data['operations'][i]['type'] == 'recirculating':
                self._defineInletOutlet(
                        node            = node,
                        residenceTime   = data['operations'][i]['residenceTime'],
                        feedingInterval = data['operations'][i]['feedingInterval'],
                        feedingTime     = data['operations'][i]['feedingTime'],
                        Tmax            = data['operations'][i]['Tmax'] if 'Tmax' in data['operations'][i] else None,
                        prefix          = prefix
                        )

            # mixingOrig: original nozzle mixing system
            elif data['operations'][i]['type'] == 'mixingOrig':
                self._defineMixing(
                        node             = node,
                        injectorFunction = self._defineOrigInjectors,
                        flowRateRatioStr = data['flowRateRatioStr'],
                        t0               = data['operations'][i]['Tinit'],
                        t1               = data['operations'][i]['Tstop'],
                        t2               = data['operations'][i]['Tmax'],
                        prefix           = prefix
                        )

            # error if other choices
            else:
                raise ValueError('Operation number ' + str(i) + ' not permitted.')
