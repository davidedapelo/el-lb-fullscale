import pandas as pd

###############################################################################################################################
def extractData_UI(baseFolder, index, sortingAlgorithm):
###############################################################################################################################
    """
    Extracts the plotting data from the output of the UI process.
    Args:
        baseFolder:            str:      Folder containing all the runs.
        index:                 ...:      Index label
        sortingAlgorithm(str): callable: Algorithm to sort the folder names
    Returns:
        Pandas DataFrame with the following entries:
            [index]   ...:     ...:   Index: Linear grid size, ...
            [columns] <float>: float: list of UI values over the index, against time snapshot
    """
    data = {index: []}

    for lastFolder, lastFolder_index in sortingAlgorithm.process(baseFolder):
        data[index].append(lastFolder_index)

        for line in open(lastFolder+'/tmp/uniformityIndexOverTime/UIsnapshots.txt', 'r'):
            content = line.rstrip().split(' ')
            snapshot = float(content[0])
            if not snapshot in data.keys():
                data[snapshot] = []
            data[snapshot].append(float(content[1]))

    # Checking that all the snapshots contain the same number of elements
    listOfKeys = list(data.keys())
    for item in listOfKeys[1:]:
        if not len(data[listOfKeys[0]]) == len(data[item]):
            raise ValueError( 'Snapshots ' + str(listOfKeys[0]) + ' does not contain the same number of points as '
                    + str(listOfKeys[item]) + ': the values are respectively:\n'
                    + str(data[listOfKeys[0]]) + '\nand:' + str(data[item]) )

    dataframe = pd.DataFrame(data=data).set_index(index)
    dataframe.columns = [str(int(item))+' s' for item in dataframe.columns]
    return dataframe

###############################################################################################################################
def extractData_vortices(baseFolder, index, sortingAlgorithm):
###############################################################################################################################
    """
    Extracts the plotting data from the output of the vortexOverTime process.
    Args:
        baseFolder:            str:      Folder containing all the runs.
        index:                 ...:      Index label
        sortingAlgorithm(str): callable: Algorithm to sort the folder names
    Returns:
        Pandas DataFrame with the following entries:
            [index]   ...:  ...:   Index: Linear grid size, ...
            [column] x_avg: float: Average across the index, vortex's X coordinate
            [column] x_std: float: Standard deviation across the index, vortex's X coordinate
            [column] y_avg: float: Average across the index, vortex's Y coordinate
            [column] y_std: float: Standard deviation across the index, vortex's Y coordinate
    """
    data = {index: [], 'x_avg': [], 'x_std': [], 'y_avg': [], 'y_std': []}

    for lastFolder, lastFolder_index in sortingAlgorithm.process(baseFolder):
        data[index].append(lastFolder_index)

        with open(lastFolder+'/tmp/vortexOverTime/coordinatesAvg.txt', 'r') as f:
            content = f.read().rstrip('\n').split(' ')
            data['x_avg'].append(float(content[0]))
            data['x_std'].append(float(content[1]))
            data['y_avg'].append(float(content[2]))
            data['y_std'].append(float(content[3]))

    return pd.DataFrame(data=data).set_index(index)
