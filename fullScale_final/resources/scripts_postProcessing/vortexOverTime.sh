#!/bin/bash

##############################################################################################
scrollFiles () {
##############################################################################################

		  PREFIX="velY_"$PREFIX_INIT
		  
		  FILE_X=$DIR_INPUT"/coordsY_"$1"__x.dat"
		  FILE_Z=$DIR_INPUT"/coordsY_"$1"__z.dat"
		  FILE_OUT=$DIR_PATH"/vortex3cm"


#========== Making the stuff. ================================================================
		  for FILE_UX in $DIR_INPUT"/"$PREFIX*"x.dat"; do # Iterating through the Ux files

#---------- Iteration's environmental variables. ---------------------------------------------
		          FILE_UZ=$DIR_INPUT"/"`basename $FILE_UX | cut -d'x' -f1`"z.dat"
					 TIMESTEP=`basename $FILE_UX | cut -d'_' -f4`
					 echo fileUx=$FILE_UX
					 echo timestep=$TIMESTEP
					 echo deltaT=$DELTAT

#---------- Python run. ----------------------------------------------------------------------
					 python3 $DIR_RESOURCES"/pyResources/vortexLB_3cm.py" \
				            $FILE_OUT \
								$FILE_X \
								$FILE_Z \
								$FILE_UX \
								$FILE_UZ \
								$uLegend \
								$uMult \
								$xMin \
								$xMax \
								$yMin \
								$yMax \
		                  `echo $TIMESTEP \* $DELTAT | bc -l` \
					         y
		  done

#========== Final operations. ================================================================
		  sort -k1 -n $FILE_OUT".txt" > $DIR_PATH"/a"
		  mv $DIR_PATH"/a" $FILE_OUT".txt"

}


echo Running vortexOverTime.sh_new ...


#========== Housekeeping. ====================================================================

#---------- Input args: called from launching script. ----------------------------------------
if [ $# -eq 1 ]; then
		  echo "Assuming I am wortking from app's home folder."
		  DIR_PATH=$1
		  DIR_RESOURCES="0resources"

#---------- No input args: called manually. --------------------------------------------------
else
		  echo "Assuming I am wortking from run's folder."
		  DIR_PATH="."
		  DIR_RESOURCES="."
fi


#========== Building the environmental variables. ============================================

#---------- Paths & prefixes. ----------------------------------------------------------------
DIR_INPUT=$DIR_PATH"/tmp/data_rawData"
FILE_INPUT=$DIR_PATH'/input.xml'
FILE_LOG=$DIR_PATH"/log0.txt"

#---------- From logfile. --------------------------------------------------------------------
DELTAT=`awk '/physDeltaT/ {print $5}' $FILE_LOG`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining deltaT. Quitting.'; echo; exit 1; fi

#---------- Environmental variables from python. ---------------------------------------------
xMin=`python3 - <<END
import xml.etree.ElementTree as ET
print( float( ET.parse('$FILE_INPUT').getroot().find('data').find('dynamics').find('xMin').text ) )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining xMin. Quitting.'; echo; exit 1; fi

xMax=`python3 - <<END
import xml.etree.ElementTree as ET
print( float( ET.parse('$FILE_INPUT').getroot().find('data').find('dynamics').find('xMax').text ) )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining xMax. Quitting.'; echo; exit 1; fi

yMin=`python3 - <<END
import xml.etree.ElementTree as ET
print( float( ET.parse('$FILE_INPUT').getroot().find('data').find('dynamics').find('yMin').text ) )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining yMin. Quitting.'; echo; exit 1; fi

yMax=`python3 - <<END
import xml.etree.ElementTree as ET
print( float( ET.parse('$FILE_INPUT').getroot().find('data').find('dynamics').find('yMax').text ) )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining yMax. Quitting.'; echo; exit 1; fi

which_uLegend=`python3 - <<END
import xml.etree.ElementTree as ET
geoType = ET.parse('$FILE_INPUT').getroot().find('simul').find('geoType').text 
if geoType == 'lab' or geoType == ' lab' or geoType == 'lab ' or geoType == ' lab ':
    print('uLegendOffs')
elif geoType == 'pilot' or geoType == ' pilot' or geoType == 'pilot ' or geoType == ' pilot ':
    print('uLegend0')
else:
    raise ValueError("input.simul.geoType value is '" + geoType + "' whilst should be 'lab' or 'pilot'.")
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining which_uLegend. Quitting.'; echo; exit 1; fi

uLegend=`python3 - <<END
import xml.etree.ElementTree as ET
print( ET.parse('$FILE_INPUT').getroot().find('data').find('dynamics').find('$which_uLegend').text )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining uLegend. Quitting.'; echo; exit 1; fi

# u_simulated / u_LB = u_target / u_exp
# => u_target = u_simulated * uMult:
#    uMult := u_exp / u_LB
uMult=`python3 - <<END
import xml.etree.ElementTree as ET
root = ET.parse('$FILE_INPUT').getroot()
print( float( root.find('data').find('dynamics').find('uRef').text ) / \
       float( root.find('data').find('dynamics').find('charU').text ) \
)
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining uMult. Quitting.'; echo; exit 1; fi

#---------- Building the prefix. -------------------------------------------------------------
if [ $which_uLegend == "uLegend0" ] ; then
        PREFIX_INIT=`basename $DIR_INPUT"/coordsY_0.00"*"__x.dat" | cut -d'_' -f2`
else
        PREFIX_INIT=`basename $DIR_INPUT"/coordsY_0.03"*"__x.dat" | cut -d'_' -f2`
fi


#========== Scrolling over the data files. ===================================================
scrollFiles $PREFIX_INIT

echo vortexOverTime completed.
