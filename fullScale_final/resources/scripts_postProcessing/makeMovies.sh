#! /bin/bash

#---------- Checks an input folder has been specified. ---------------------------------------
if [ ! -d $1 ] || [ ! $# -eq 1 ]; then
		  echo "Input folder missing."
		  echo "Stopping."
		  exit 1
fi


#---------- Gets into the input folder. ------------------------------------------------------
pushd $1


#---------- Builds folders for single frames. ------------------------------------------------
if [ ! -d velocity ];then mkdir velocity; fi
if [ ! -d pressure ];then mkdir pressure; fi
if [ ! -d bubbles  ];then mkdir bubbles;  fi
if [ ! -d composed ];then mkdir composed; fi


#---------- Renames & reorders the single frames. --------------------------------------------
N=0
#for F in plotOrthoY0.000*; do
for F in _Eu*; do
		  cp $F velocity/$N".jpeg"
		  let N=$N+1
done

N=0
for F in _ph*; do
		  cp $F pressure/$N".jpeg"
		  let N=$N+1
done

N=0
for F in _Ex*; do
		  cp $F bubbles/$N".jpeg"
		  let N=$N+1
done


#---------- Builds the composed frames though ImageMagicK ("compose" command). ---------------
for F in velocity/*; do
		  N=`basename $F|cut -d'.' -f1`
		  convert velocity/$N".jpeg" pressure/$N".jpeg" +append zzz$N".jpeg"
		  convert zzz$N".jpeg" bubbles/$N".jpeg" -append composed/frame_$N".jpeg"
        rm zzz$N".jpeg" velocity/$N".jpeg" pressure/$N".jpeg"
done


#---------- Creates the video through ffmpeg. ------------------------------------------------
#ffmpeg -r 10 -f image2 -s 1920x1080 -i pressure_%d.jpeg -vcodec libx264 -crf 25  -pix_fmt yuv420p pressure.mp4
ffmpeg -r 10 -f image2 -s 1920x1080 -i composed/frame_%d.jpeg -vcodec libx264 -crf 25  -pix_fmt yuv420p video.mp4

rmdir velocity pressure


#---------- Returns back to original folder. -------------------------------------------------
popd
