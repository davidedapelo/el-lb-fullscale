#!/bin/bash




#========== Preliminary work. ================================================================

#---------- Folder where to search. ----------------------------------------------------------
DIR="logs"
VISCODATA="viscoData"
VISCOPLOTS="viscoPlots"

#---------- Checks an input folder has been specified. ---------------------------------------
if [ ! -d $1 ] || [ ! $# -eq 1 ]; then
		  echo "Input folder missing."
		  echo "Stopping."
		  exit 1
fi

#---------- Getting into working directory. --------------------------------------------------
pushd $1

#---------- Creates $VISCODATA and $VISCOPLOTS folder if does not exist, and cleans it. ------
if [ ! -f $VISCODATA ]; then
		  mkdir $VISCODATA
fi
rm $VISCODATA/*

if [ ! -f $VISCOPLOTS ]; then
		  mkdir $VISCOPLOTS
fi
rm $VISCOPLOTS/*




#========== Doing the job. ===================================================================
for F in $DIR/*; do

#---------- Building all the variables. ------------------------------------------------------
        CMC=`grep -m1 "cmc" $F | awk '{print $5}'`
        FLOWRATE=`grep -m1 "flowRate" $F | awk '{print $6}'`
        NUMIN=`grep -m1 "nuMin" $F | awk '{print $5}'`
        NUMAX=`grep -m1 "nuMax" $F | awk '{print $5}'`
        CHARU=`grep -m1 "charU" $F | awk '{print $5}'`
        UCENTRE=`tail -n20 $F | tac | grep -m1 "BUFFER" | awk '{print $5}'`
		  
		  DIFF=`echo $UCENTRE - $CHARU | bc`

#---------- Storig them on files. ------------------------------------------------------------
		  echo $NUMIN $NUMAX $DIFF >> $VISCODATA"/data_cmc"$CMC"_flowRate"$FLOWRATE".txt"
		  echo $CMC $FLOWRATE $NUMIN $NUMAX $UCENTRE $CHARU $DIFF

#---------- Running gnuplot to create graphs. ------------------------------------------------
		  gnuplot -e "\
					     set terminal png; \
					     set output '$VISCOPLOTS/plot_cmc${CMC}_flowRate${FLOWRATE}.png'; \
						  set logscale xy; \
						  set xlabel 'nuMin' offset -3 -2; \
						  set ylabel 'nuMax' offset 5 -1; \
						  set zlabel 'exp-num'rotate by 90; \
						  set grid xtics ytics mxtics mytics; \
						  set xyplane at 0; \
						  set format x '%.0e'; \
						  set format y '%.0e'; \
						  splot '$VISCODATA/data_cmc${CMC}_flowRate${FLOWRATE}.txt' using 1:2:(0):(0):(0):3 with vectors; \
						 "
done

#---------- Back into original directory. ----------------------------------------------------
popd
