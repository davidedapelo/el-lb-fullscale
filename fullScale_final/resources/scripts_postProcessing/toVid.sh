#!/bin/bash

#---------- Check input. ---------------------------------------------------------------------
if [ ! $# == 1 ] ; then
  echo "Usage: toVid <target_folder_run>"
  exit 1
fi

#---------- Working folders & check. ---------------------------------------------------------
DIR_TMP=$1/tmp
DIR_PLOTS=$DIR_TMP/data_plots
DIR_PROVV=$DIR_TMP/0provv
PREF_PHI="phiY"
PREF_UY="plotY"
PREF_UZ="plotZ"
PREF_VISCO="viscoY"

if [ ! -d $DIR_TMP ] ; then
  echo Folder not found: $DIR_TMP
  exit 1
fi

if [ ! -d $DIR_PLOTS ] ; then
  echo Folder not found: $DIR_PLOTS
  exit 1
fi

if [ -d $DIR_PROVV ] ; then rm -fr $DIR_PROVV ; fi
mkdir $DIR_PROVV


SUFF_PHI=$(basename $(echo $DIR_PLOTS/$PREF_PHI*__0.pdf) | cut -d'_' -f2)
SUFF_UY=$(basename $(echo $DIR_PLOTS/$PREF_UY*__0.pdf) | cut -d'_' -f2)
SUFF_UZ=$(basename $(echo $DIR_PLOTS/$PREF_UZ*__0.pdf) | cut -d'_' -f2)
SUFF_VISCO=$(basename $(echo $DIR_PLOTS/$PREF_VISCO*__0.pdf) | cut -d'_' -f2)


#---------- Cycling the timestep and creating a combined picture. ----------------------------
for FILE_PHI_SCROLL in $DIR_PLOTS/$PREF_PHI"_"$SUFF_PHI"__"*; do
  N=$(basename $FILE_PHI_SCROLL | cut -d'_' -f4 | cut -d'.' -f1)
  printf -v NPAD "%08d" $N 

  FILE_PHI=$DIR_PLOTS/$PREF_PHI"_"$SUFF_PHI"__"$N".pdf"
  FILE_UY=$DIR_PLOTS/$PREF_UY"_"$SUFF_UY"__"$N".pdf"
  FILE_UZ=$DIR_PLOTS/$PREF_UZ"_"$SUFF_UZ"__"$N".pdf"
  FILE_VISCO=$DIR_PLOTS/$PREF_VISCO"_"$SUFF_VISCO"__"$N".pdf"
  convert $FILE_UY $FILE_UZ +append $DIR_PROVV/line1.png
  convert $FILE_VISCO $FILE_PHI +append $DIR_PROVV/line2.png
  convert $DIR_PROVV/line1.png  $DIR_PROVV/line2.png -append  $DIR_PROVV/pad$NPAD".png"
  rm $DIR_PROVV/line1.png  $DIR_PROVV/line2.png
done

N=0
for PAD in $DIR_PROVV/*.png; do
  mv $PAD $DIR_PROVV/$N.png
  let N=$N+1
done

#---------- Concluding. ----------------------------------------------------------------------
ffmpeg -r 30 -f image2 -i $DIR_PROVV/%d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" $DIR_TMP/video.mp4
#rm -fr $DIR_PROVV
