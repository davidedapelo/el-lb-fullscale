#!/bin/bash


##############################################################################################
pickLastFile () {
##############################################################################################
		  PREFIX=$1
		  SUFFIX=$2
		  FILE_PATH_AND_PRE_SUFF=$DIR_PLOTS/$PREFIX"_"$SUFFIX

		  LIST=$(echo '[' $(for FILE in $FILE_PATH_AND_PRE_SUFF*; do echo $(basename $FILE | cut -d'_' -f4 | cut -d'.' -f1)','; done) '-1 ]')
		  echo $FILE_PATH_AND_PRE_SUFF"__"$(python3 -c "print(max(  $LIST  ))").pdf
}



#========== Housekeeping. ====================================================================

if [ ! $# -eq 1 ]; then
		  echo "Only ONE input parameter allowed; "$#" inserted instead."
		  echo Quitting.
		  exit 1
fi


#========== Building the environmental variables. ============================================

#---------- Folder paths & prefixes. ---------------------------------------------------------
DIR_PATH=$1
DIR_DATA=$DIR_PATH"/tmp/data_rawData"
DIR_PLOTS=$DIR_PATH"/tmp/data_plots"
DIR_RESOURCES="0resources"

PREFIX1="plotY"
PREFIX2="viscoY"
SUFFIX_INIT1=`basename $DIR_DATA"/coordsY_0.0"*"__x.dat" | cut -d'_' -f2` # all the Y files
SUFFIX_INIT2=`basename $DIR_DATA"/coordsY_0.03"*"__x.dat" | cut -d'_' -f2` # only the plane_0.03 Y files


#========== Picking the last file of the series. =============================================
cp `pickLastFile $PREFIX1 $SUFFIX_INIT1` $DIR_PATH/lastT_$PREFIX1'_'$SUFFIX_INIT1.pdf
cp `pickLastFile $PREFIX1 $SUFFIX_INIT2` $DIR_PATH/lastT_$PREFIX1'_'$SUFFIX_INIT2.pdf
cp `pickLastFile $PREFIX2 $SUFFIX_INIT1` $DIR_PATH/lastT_$PREFIX2'_'$SUFFIX_INIT1.pdf
cp `pickLastFile $PREFIX2 $SUFFIX_INIT2` $DIR_PATH/lastT_$PREFIX2'_'$SUFFIX_INIT2.pdf
