#!/bin/bash

#---------- Check input. ---------------------------------------------------------------------
if [ ! $# == 1 ] ; then
  echo "Usage: toVid <folder containing the numbered photograms>"
  exit 1
fi

DIR_PROVV=$1/000

if [ -d $DIR_PROVV ]; then rm -fr $DIR_PROVV; fi
mkdir $DIR_PROVV

echo
echo Converting pdf images to png ...
for F in $1/*.pdf; do
  TARGET_FILENAME=$(basename $F | cut -d'.' -f1)
  TARGET_FILENAME=`printf %08d $TARGET_FILENAME` # padding with zeros
  TARGET_FILENAME=pad$TARGET_FILENAME
  pdftoppm -png -rx 300 -ry 300 $F $DIR_PROVV/$TARGET_FILENAME
  echo "--->" $DIR_PROVV/$TARGET_FILENAME generated
done
echo Image conversion completed.

echo
echo Renaming images ...
N=0
for F in $DIR_PROVV/pad*; do
  let N=$N+1
  mv $F $DIR_PROVV/$N.png
  echo $F converted into $N.png
done
echo All the images have been renamed.

#---------- Concluding. ----------------------------------------------------------------------
ffmpeg -r 30 -f image2 -i $DIR_PROVV/%d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" $1.mp4
#rm -fr $DIR_PROVV
