#!/bin/bash

##############################################################################################
scrollFiles () {
##############################################################################################

        PREFIX="velY_"$1

        FILE_X=$DIR_INPUT"/coordsY_"$1"__x.dat"
        FILE_Z=$DIR_INPUT"/coordsY_"$1"__z.dat"


#========== Making the stuff. ================================================================
        for FILE_UX in $DIR_INPUT"/"$PREFIX*"x.dat"; do # Iterating through the Ux files
        #for FILE_UX in $DIR_INPUT"/"{${PREFIX1},${PREFIX2}}*"x.dat"; do # Iterating through the Ux files

#---------- Iteration's environmental variables. ---------------------------------------------
		          FILE_UY=$DIR_INPUT"/"`basename $FILE_UX | cut -d'x' -f1`"y.dat"
		          FILE_UZ=$DIR_INPUT"/"`basename $FILE_UX | cut -d'x' -f1`"z.dat"
		          PROVV="/viscoY"`basename $FILE_UX | cut -d'Y' -f2`
		          FILE_VISCO=$DIR_INPUT"/"`echo $PROVV | cut -d'_' -f1`"_"`echo $PROVV | cut -d'_' -f2`"__"`echo $PROVV | cut -d'_' -f4`".dat"
		          #FILE_VISCO=$DIR_INPUT"/viscoY"`basename $FILE_UX | cut -d'Y' -f2`
                FILE_OUT_VEL=$DIR_OUTPUT"/plotY_"`basename $FILE_UX | cut -d'_' -f2`"_"`basename $FILE_UX | cut -d'_' -f4`".pdf"
                FILE_OUT_VISCO=$DIR_OUTPUT"/viscoY_"`basename $FILE_UX | cut -d'_' -f2`"_"`basename $FILE_UX | cut -d'_' -f4`".pdf"
                echo "Timestep: FILE_OUT_VEL=   "$FILE_OUT_VEL
                echo "          FILE_OUT_VISCO= "$FILE_OUT_VISCO
                echo "          File_X=         "$FILE_X
                echo "          File_Z=         "$FILE_Z
                echo "          File_UX=        "$FILE_UX
                echo "          File_UY=        "$FILE_UY
                echo "          File_UZ=        "$FILE_UZ
                echo "          File_VISCO=     "$FILE_VISCO

		          if [ $(python3 -c "print( $(basename $FILE_VISCO | cut -d'.' -f1 | cut -d'_' -f2) * $DELTAT < $Tmax1)") == "True" ] ; then
                        DIR_OUTPUT_OLD=$DIR_OUTPUT"-"$N
                        viscoInf=$logNuMin1
                        viscoSup=$logNuMax1
                else
                        viscoInf=$logNuMinM
                        viscoSup=$logNuMaxM
                fi

#---------- Python run. ----------------------------------------------------------------------
		          python3 $DIR_RESOURCES"/pyResources/plotsLB.py" \
				            $FILE_OUT_VEL \
                        $FILE_X \
                        $FILE_Z \
                        $FILE_UX \
                        $FILE_UY \
                        $FILE_UZ \
                        "0.03" \
                        "y" \
					         $uMult

		          python3 $DIR_RESOURCES"/pyResources/plotsLBvisco.py" \
				            $FILE_OUT_VISCO \
                        $FILE_X \
                        $FILE_Z \
                        $FILE_VISCO \
                        $viscoInf \
                        $viscoSup

        done
}

#========== Housekeeping. ====================================================================

if [ ! $# -eq 1 ]; then
		  echo "Only ONE input parameter allowed; "$#" inserted instead."
		  echo Quitting.
		  exit 1
fi

#========== Building the environmental variables. ============================================

#---------- Folder paths & prefixes. ---------------------------------------------------------
DIR_PATH=$1
DIR_INPUT=$DIR_PATH"/tmp/data_rawData"
DIR_OUTPUT=$DIR_PATH"/tmp/data_plots"
DIR_RESOURCES="0resources"
#PREFIX_INIT=`basename $DIR_INPUT"/coordsY_0.*__x.dat" | cut -d'_' -f2` # all the Y files # DOES NOT WORK ON CIRRUS !!!
PREFIX_INIT1=`basename $DIR_INPUT"/coordsY_0.0"*"__x.dat" | cut -d'_' -f2` # all the Y files
PREFIX_INIT2=`basename $DIR_INPUT"/coordsY_0.03"*"__x.dat" | cut -d'_' -f2` # only the plane_0.03 Y files
#REFIX1="velY_"`basename $DIR_INPUT"/coordsY_0.0"*"__x.dat" | cut -d'_' -f2` # only the plane_0.0 Y files
#PREFIX2="velY_"`basename $DIR_INPUT"/coordsY_0.03"*"__x.dat" | cut -d'_' -f2` # only the plane_0.03 Y files

#---------- File paths. ----------------------------------------------------------------------
FILE_INPUT=$DIR_PATH'/input.xml'

#---------- From logfile. --------------------------------------------------------------------
DELTAT=`awk '/physDeltaT/ {print $5}' $DIR_PATH/log0.txt`

#---------- Environmental variables from python. ---------------------------------------------
uMult=`python3 - <<END
import xml.etree.ElementTree as ET
root = ET.parse('$FILE_INPUT').getroot()
print( float( root.find('data').find('dynamics').find('uRef').text ) / \
       float( root.find('data').find('dynamics').find('charU').text ) \
     )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining uMult. Quitting.'; echo; exit 1; fi

logNuMin1=`python3 - <<END
import xml.etree.ElementTree as ET
import numpy as np
root = ET.parse('$FILE_INPUT').getroot()
print( np.log10 ( float( root.find('setup').find('nuMin1').text ) / \
                  float( root.find('data').find('dynamics').find('physViscosity').text ) \
                ) \
     )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining logNuMin1. Quitting.'; echo; exit 1; fi

logNuMinM=`python3 - <<END
import xml.etree.ElementTree as ET
import numpy as np
root = ET.parse('$FILE_INPUT').getroot()
print( np.log10 ( float( root.find('setup').find('nuMinM').text ) / \
                  float( root.find('data').find('dynamics').find('physViscosity').text ) \
                ) \
     )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining logNuMinM. Quitting.'; echo; exit 1; fi

logNuMax1=`python3 - <<END
import xml.etree.ElementTree as ET
import numpy as np
root = ET.parse('$FILE_INPUT').getroot()
print( np.log10 ( float( root.find('setup').find('nuMax1').text ) / \
                  float( root.find('data').find('dynamics').find('physViscosity').text ) \
                ) \
     )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining logNuMax1. Quitting.'; echo; exit 1; fi

logNuMaxM=`python3 - <<END
import xml.etree.ElementTree as ET
import numpy as np
root = ET.parse('$FILE_INPUT').getroot()
print( np.log10 ( float( root.find('setup').find('nuMaxM').text ) / \
                  float( root.find('data').find('dynamics').find('physViscosity').text ) \
                ) \
     )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining logNuMaxM. Quitting.'; echo; exit 1; fi

Tmax1=`python3 - <<END
import xml.etree.ElementTree as ET
root = ET.parse('$FILE_INPUT').getroot()
print( root.find('time').find('Tmax1').text )
END`
if [ ! $? -eq 0 ] ; then echo; echo 'Problem in determining Tmax1. Quitting.'; echo; exit 1; fi


#========== Relocating older image folders if present. =======================================
if [ -d $DIR_OUTPUT ] ; then # performing the job only if an image folder is already there

#---------- Building up the last "old" name iteratively. -------------------------------------
        N=0
        while
                DIR_OUTPUT_OLD=$DIR_OUTPUT"-"$N
                [ -d $DIR_OUTPUT_OLD ]
		  do
                let N=$N+1
		  done
        echo old=$DIR_OUTPUT_OLD

#---------- Moving the folder to the designed last "old" name. -------------------------------
        mv $DIR_OUTPUT $DIR_OUTPUT_OLD
fi

mkdir $DIR_OUTPUT # Now that the old folder is sorted, we make the new one


#========== Scrolling over the data files. ===================================================
scrollFiles $PREFIX_INIT1
scrollFiles $PREFIX_INIT2

