#!/bin/bash

if [ ! $# -eq 1 ]; then
		  echo "Input (viz., run's base folder path) missing."
		  echo "Quitting."
		  exit 1
fi

FOLDER_PATH=$1
FOLDER_DATA=$FOLDER_PATH"/tmp/data_rawData"
FILE_LOG=$FOLDER_PATH"/log.txt"

PREFIX_INIT=`basename $FOLDER_DATA"/coordsY_0.02"*"__x.dat" | cut -d'_' -f2`
FILE_X=$FOLDER_DATA"/coordsY_"$PREFIX_INIT"__x.dat"
FILE_Z=$FOLDER_DATA"/coordsY_"$PREFIX_INIT"__z.dat"
FILE_OUT=$FOLDER_PATH"/avgVelocities.txt"
PREFIX="velY_"$PREFIX_INIT

uLegend="0.02"
xMin="0.02"
xMax="0.09"
yMin="0.0"
yMax="0.09"

deltaT=`awk '/physDeltaT/ {print $5}' $FILE_LOG`

for FILE_UX in $FOLDER_DATA"/"$PREFIX*"x.dat"; do
		  FILE_UZ=$FOLDER_DATA"/"`basename $FILE_UX | cut -d'x' -f1`"z.dat"

		  TIMESTEP=`basename $FILE_UX | cut -d'_' -f4`
		  echo `echo $TIMESTEP '*' $deltaT | bc -l` \
				 `python3 0resources/pyResources/avgVelocitiesLB.py \
				      dummy $FILE_X $FILE_Z $FILE_UX $FILE_UZ \
					   $uLegend $xMin $xMax $yMin $yMax n \
				 `>> $FILE_OUT
done

sort -k1 -n $FILE_OUT > $FOLDER_PATH"/a"
mv $FOLDER_PATH"/a" $FILE_OUT
