#!/bin/bash

#---------- Folder where to search. ----------------------------------------------------------
DIR_SEARCHING_PATTERN=$1

#---------- Checks an input folder has been specified. ---------------------------------------
if [ ! -d $2 ] || [ ! $# -eq 2 ]; then
		  echo "Input folder missing."
		  echo "Stopping."
		  exit 1
fi

pushd $2

#---------- Search on specified folder and copies the wanted files. --------------------------
for ORIG_DIR in $DIR_SEARCHING_PATTERN*; do
		  IMG_DIR=img`echo $ORIG_DIR | cut -d'c' -f3`
		  cp -r $ORIG_DIR/tmp/imageData $IMG_DIR
		  for PYPLOTS in $ORIG_DIR/tmp/data_plots/*; do
					 cp $PYPLOTS $IMG_DIR/
		  done
		  cp $ORIG_DIR/tmp/gnuplotData/uCentre.png $IMG_DIR/
		  cp $ORIG_DIR/log* $IMG_DIR/
done

popd
