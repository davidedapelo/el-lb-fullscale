#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import IndexedGrampa_SortingAlgorithm as sortingAlgorithm
from helpers_data import extractData_vortices as extractData
from helpers_plot import plotVortexCoordinate

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 4:
        raise ValueError('Exactly THREE argv argument (== first part of the path of the folder containing the runs; Nx, tag) must by inputted. Quitting.')

    def funct (x, a, b, c):
        return a + b/x + c/x**2

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/vortices_' + str(sys.argv[2])).mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'd', sortingAlgorithm(int(sys.argv[2]), "d=", "__", [sys.argv[3], 'antiDiff', 'subC=100']))
    #data = extractData(sys.argv[1], 'd', sortingAlgorithm(int(sys.argv[2]), "d=", "__", ['ball', 'antiDiff', 'subC=100']))
    plotVortexCoordinate ( data[['x_avg', 'x_std']].rename(columns={'x_avg': 'avg', 'x_std': 'std'}, inplace=False),
                           sys.argv[1]+'/vortices_' + str(sys.argv[2])+'/vortexX_'+sys.argv[3]+'.pdf',
                           'Bubble diameter (mm)',
                           ['Average vortex X position (m)', 'Error on average vortex X position (m)'],
                           'upper left',
                           funct,
                           [['$x_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_x=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                           60
                         )
    plotVortexCoordinate ( data[['y_avg', 'y_std']].rename(columns={'y_avg': 'avg', 'y_std': 'std'}, inplace=False),
                           sys.argv[1]+'/vortices_' + str(sys.argv[2])+'/vortexY_'+sys.argv[3]+'.pdf',
                           'Bubble diameter (mm)',
                           ['Average vortex Y position (m)', 'Error on average vortex Y position (m)'],
                           'upper left',
                           funct,
                           [['$x_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_x=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                           60
                         )
