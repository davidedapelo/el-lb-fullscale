#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import IndexedParent_SortingAlgorithm as sortingAlgorithm
from helpers_data import extractData_UI as extractData
from helpers_plot import plotUI

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 4:
        raise ValueError('Exactly THREE argv argument (==path of the folder containing the runs; Nx; one tag) must by inputted. Quitting.')
    if not Path(sys.argv[1]).is_dir():
        raise ValueError('Folder ' + sys.argv[1] + ' does not exist or is not a directory. Quitting.')

    def funct (x, a, b, c):
        return a + b/x + c/x**2

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/subcycles_ui/').mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'subC', sortingAlgorithm(int(sys.argv[2]), "subC=", "__", [sys.argv[3], 'antiDiff']))
    plotUI ( data,
             sys.argv[1]+'/subcycles_ui/subcycles_'+sys.argv[3]+'_ui.pdf',
             'Number of Lagrangian subcycles',
             ['Uniformity index', 'Error on Uniformity Index'],
             'upper left',
             funct,
             10
           )
