from abc import ABC, abstractmethod
from glob import glob
from pathlib import Path

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class SortingAlgorithm(ABC):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Abstract base class for sorting algorithms.
    Sorting algotithms sort folder names within a parent folder following an <index>.
    The abstract method process performs the actual job of sorting, and needs to be implemented in the derived classes.
    The protected methods are functional in the internal working of the algorithms.
    """

###############################################################################################################################
    def __init__(self):
###############################################################################################################################
        """
        Constructor. To be overridden if arguments need passing into derived classes.
        """
        pass

###############################################################################################################################
    def _getIndexFromFoldername(self, item):
###############################################################################################################################
        """
        Extracts an index from a folder name following the convention <index>-<NoOfOccurrences>.
        Args:
            item:  str: Path
        Returns:
            (...): int: Index
        """
        return int( Path(item).name.split('-')[0] )

###############################################################################################################################
    def _getLastFolder(self, listOfFolders):
###############################################################################################################################
        """
        Returns the last occurrence of a list of folder names following the convention <parentFolder>/<index>-<NoOfOccurrences>.
        Args:
            listOfFolders: [str]: List of sorted partial folder names [ <parentFolder> / <index>- ]
        Returns:
            (...):         str:   Folder name of the last occurrence following the convention <parentFolder> / <index>-<lastOccurrence>.
            (...):         int:   Index of the above-defined last folder name
        """
        name = sorted(
                glob(listOfFolders+'*'),
                key = lambda item: int( Path(item).name.split('-')[1] )
                )[-1]
        return name, self._getIndexFromFoldername(name)

###############################################################################################################################
    @abstractmethod
    def process(self, parentFolder):
###############################################################################################################################
        """
        Abstract method containing the sorting algorithm, to be implemented in the derived classes.
        Args:
            parentFolder: str: Parent folder
        Returns:
            (...): [str]: List of sorted partial folder names [ <parentFolder> / <index>- ], one per occurrence
        """
        pass

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class Parent_Index_Occurences_SortingAlgorithm(SortingAlgorithm):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Derived class implementing a sorting algorithm for files
    following the convention <parentFolder> / <index>-<NoOfOccurrences>.
    """

###############################################################################################################################
    def __init__(self):
###############################################################################################################################
        """
        Constructor. Just calls the parent class's constructor.
        """
        super().__init__()

###############################################################################################################################
    def process(self, parentFolder):
###############################################################################################################################
        """
        Sorts folder names within a parent folder following an <index>.
        The files follow the convention <parentFolder> / <index>-<NoOfOccurrences>.
        Args:
            parentFolder: str: Parent folder
        Returns:
            [(<folderName>, <index>)]: [tuple]: tuple of elements as per in _getLastFolder
        """
        listOfFolders = sorted(
                list(set([
                    str(Path(item).parent) + '/' + Path(item).name.split('-')[0] + '-'
                    for item in glob(parentFolder + '/*-*')
                    ])),
                key = self._getIndexFromFoldername
                )
        return [ (self._getLastFolder(folders)) for folders in listOfFolders ]

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class IndexedParent_SortingAlgorithm(SortingAlgorithm):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Derived class implementing a sorting algorithm for files
    following the convention <parentFolder> / <...><lDelim><index><rDelim><...> / <nx>-<NoOfOccurrences>.
    The results of the sorting must include a list of input patterns.
    """

###############################################################################################################################
    def __init__(self, nx, lDelim, rDelim, patterns):
###############################################################################################################################
        """
        Constructor. Just calls the parent class's constructor.
        Args:
            nx:       int:
            lDelim:   str:
            rDelim:   str:
            patterns: str:
        """
        super().__init__()
        self.nx = nx
        self.lDelim = lDelim
        self.rDelim = rDelim
        self.patterns = patterns

###############################################################################################################################
    def _getIndexFromFoldername(self, item):
###############################################################################################################################
        """
        Extracts an index from a folder name following the convention <...><lDelim><index><rDelim><...>
        Args:
            item:  str: Path
        Returns:
            (...): int: Index
        """
        return int( Path(item).parts[-2].split(self.lDelim)[1].split(self.rDelim)[0] )

###############################################################################################################################
    def process(self, parentFolder):
###############################################################################################################################
        """
        Sorts folder names within a parent folder following an <index>.
        The files follow the convention <parentFolder> / <...><lDelim><index><rDelim><...> / <nx>-<NoOfOccurrences>.
        The results must include a list of input patterns.
        Args:
            parentFolder: str: Parent folder
        Returns:
            [(<folderName>, <index>)]: [tuple]: tuple of elements as per in _getLastFolder
        """
        listOfFolders = sorted(
                list(set([
                    item
                    for item in glob(parentFolder + '/*/*-*')
                    if all(i in item for i in self.patterns)
                    and super(IndexedParent_SortingAlgorithm, self)._getIndexFromFoldername(item) == self.nx
                    ])),
                key = self._getIndexFromFoldername
                )
        return [ (self._getLastFolder(folders)) for folders in listOfFolders ]

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class IndexedGrampa_SortingAlgorithm(IndexedParent_SortingAlgorithm):
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Derived class implementing a sorting algorithm for files
    following the convention <...><lDelim><index><rDelim><...> / <...> / <nx>-<NoOfOccurrences>.
    The results of the sorting must include a list of input patterns.
    """

###############################################################################################################################
    def __init__(self, nx, lDelim, rDelim, patterns):
###############################################################################################################################
        """
        Constructor. Just calls the parent class's constructor.
        Args:
            nx:       int:
            lDelim:   str:
            rDelim:   str:
            patterns: str:
        """
        super(IndexedGrampa_SortingAlgorithm, self).__init__(nx, lDelim, rDelim, patterns)

###############################################################################################################################
    def _getIndexFromFoldername(self, item):
###############################################################################################################################
        """
        Extracts an index from a folder name following the convention <...><lDelim><index><rDelim><...>
        Args:
            item:  str: Path
        Returns:
            (...): int: Index
        """
        return int( Path(item).parts[-3].split(self.lDelim)[1].split(self.rDelim)[0] )


###############################################################################################################################
    def process(self, parentFolder):
###############################################################################################################################
        """
        Sorts folder names within a parent folder following an <index>.
        The files follow the convention <parentFolder> / <...><lDelim><index><rDelim><...> / <...> / <nx>-<NoOfOccurrences>.
        The results must include a list of input patterns.
        Args:
            parentFolder: str: Parent folder
        Returns:
            [(<folderName>, <index>)]: [tuple]: tuple of elements as per in _getLastFolder
        """
        listOfFolders = sorted(
                list(set([
                    item
                    for item in glob(parentFolder + '/*/*/*-*')
                    if all(i in item for i in self.patterns)
                    and super(IndexedParent_SortingAlgorithm, self)._getIndexFromFoldername(item) == self.nx
                    ])),
                key = self._getIndexFromFoldername
                )
        return [ (self._getLastFolder(folders)) for folders in listOfFolders ]
