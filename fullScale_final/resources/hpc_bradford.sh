#!/usr/bin/env bash

# Pre- and post-processing scripts from argv
PYTHON_OPERATIONS_PRE=$1
PYTHON_OPERATIONS_POST=$2

# Set array task ID and job index
if [ -z ${SLURM_ARRAY_TASK_ID+x} ]; then
  export JOB_INDEX="0"
else
  export JOB_INDEX=${SLURM_ARRAY_TASK_ID}
fi

# Determines whether we are on HoreKa
if [ `whoami` == 'de5716' ] ; then
  echo We are on HoreKa.
  module load mpi/openmpi/4.0
  source ~/dav-env/bin/activate
  MPI_COMMAND="mpirun --bind-to core --map-by core"
elif [ `whoami` == 'dapelo' ] ; then
  echo We are on Barkla.
  module purge
  module load apps/anaconda3/2019.10-general
  module load compilers/gcc/11.2.0
  module load mpi/openmpi/4.1.1/gcc-11.2.0
    #module load GCC
    #module load OpenMPI
    #module load Miniconda3
    #module load gnuplot
    #module load imagemagick
    #source activate dav-conda-env
  MPI_COMMAND=mpirun
else
  echo We are on my laptop.
  # Need to override array task ID and job index
  export JOB_INDEX="0"
  export SLURM_JOB_NUM_NODES="1"
  MPI_COMMAND=mpirun
fi
  echo JOB_INDEX=$JOB_INDEX
  echo SLURM_JOB_NUM_NODES=$SLURM_JOB_NUM_NODES

TEST='default' # just to avoid stupid error messages
#TEST='test' # uncomment to make just postprocessing tests on the laptop, without running the OpenLB simulations
if [ $TEST == 'test' ] ; then # Test execution: MUST manually modify the env vars accordingly!! AND with a final '/' !!
  FOLDER_NAME='git_ignore/0beforeCorrection/TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__antiDiff=0.0/'
  FOLDER_RUN='80-0/'
  LOG_FILENAME='log-0.txt'

else # Normal authomatic run
  # Generating random filename to temporarily store job variables
  TMP_FILENAME=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1`.txt

  # Launching the pre-processing python script
  python3 $PYTHON_OPERATIONS_PRE $TMP_FILENAME $JOB_INDEX $SLURM_JOB_NUM_NODES
  echo Other flags:
  cat $TMP_FILENAME

  # Folder and logfile names from temporary file to env vars
  FOLDER_NAME=`sed '1q;d' $TMP_FILENAME`
  FOLDER_RUN=`sed '2q;d' $TMP_FILENAME`
  LOG_FILENAME=`sed '3q;d' $TMP_FILENAME`
  DEBUG=`sed '4q;d' $TMP_FILENAME`
  EXE_FILENAME=`sed '5q;d' $TMP_FILENAME`
  rm $TMP_FILENAME

  # Copying the python data resources to the run folder
  cp -r resources/pyResources $FOLDER_NAME$FOLDER_RUN

  if [ $DEBUG == 'DEBUG' ] ; then # GDB run: 1 attempt REMEMBER to switch CXXFLAGS from OPTIM to DEBUG in config.mk !
    gdb --args $EXE_FILENAME $FOLDER_NAME$FOLDER_RUN 2>&1 | tee -a $FOLDER_NAME$FOLDER_RUN$LOG_FILENAME
  elif [ $DEBUG == 'OPTIM' ] ; then # normal runs
    if [ $EXE_FILENAME == 'fullScale_final' ] || [ $EXE_FILENAME == 'fullScale_discharge' ] ; then # Particular cases: 3 attempts
      for ITER in `seq 1 3`; do
        if [ ! -f $FOLDER_NAME$FOLDER_RUN'allDone' ]; then
          LAUNCHING_COMMAND="$MPI_COMMAND $EXE_FILENAME $FOLDER_NAME$FOLDER_RUN 2>&1 | tee -a $FOLDER_NAME$FOLDER_RUN$LOG_FILENAME"
          echo
          echo "Launching command:"
          echo $LAUNCHING_COMMAND
          echo
          eval $LAUNCHING_COMMAND
        fi
      done
    else # Standard case: whyle cycle
      while [ ! -f $FOLDER_NAME$FOLDER_RUN'allDone' ] && [ ! -f $FOLDER_NAME$FOLDER_RUN'lastAttempt' ] ; do
        LAUNCHING_COMMAND="$MPI_COMMAND $EXE_FILENAME $FOLDER_NAME$FOLDER_RUN 2>&1 | tee -a $FOLDER_NAME$FOLDER_RUN$LOG_FILENAME"
        echo
        echo "Launching command:"
        echo $LAUNCHING_COMMAND
        echo
        eval $LAUNCHING_COMMAND
      done
    fi
  else
    echo Wrong DEDUG argument=$DEBUG
    exit 1
  fi
fi

# Launching the post-processing python script
python3 $PYTHON_OPERATIONS_POST $FOLDER_NAME $FOLDER_RUN $LOG_FILENAME
