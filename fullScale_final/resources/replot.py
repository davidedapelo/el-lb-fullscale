import numpy as np

from abc import ABC, abstractmethod
from glob import glob
import matplotlib.pyplot as plt
from pathlib import Path
#from scipy.optimize import minimize

import pyResources.pyResources.dataElaboration as de
import pyResources.pyResources.plotting as pt

#######################################################################################################################
class BaseReplottingAlgorithm(ABC):
#######################################################################################################################
    """
    Abstract base class for replotting algorithms.
    Replotting algorithms replot sets of plots from the outcome of a simulation, with different input plotting parameters.
    """


    def __init__(self, args, **kwargs):
        """
        Constructor containing all the basic operations to all the replotting algorithms.
        Defines all the class internal parameters and creates the folder containing the replots if not already existing.
        Args:
            args: [str]: input parameters, generally passed to the python3 caller file as an argv sequence and then
                         re-passed to this constructor as they are:
                         [0]: dummy variable (name of the python3 caller file)
                         [1]: path of the run
                         [2]: name of the folder containing the replots
                         [3]: colorbar label
                         [4]: background colour
                         [5]: minimum value in the colorbar
                         [6]: maximum value in the colorbar
                         [7]: slice orientation
                         [...]: strings to be contained in rawData filename, one element at a time.
                                Only the elements in rawData containing all the strings are processed.
                         [optional]: 'vortex' (if present, the following entries will give the corners of the window
                                               to compute vortex coordinates. Ignored if not plotting u.)
                         [optional]: xguess
                         [optional]: yguess
                         [optional]: xmin
                         [optional]: xmax
                         [optional]: ymin
                         [optional]: ymax
        """
        # Quitting if bad input
        if not len(args) > 7:
            raise ValueError('More than 7 arguments must by inputted, but {} were provided instead. Quitting.'.format(len(args)))
        if (not args[7] == 'Y') and (not args[7] == 'Z'):
            raise ValueError("The 7th argument must be either 'Y' or 'Z', but '{}' was inputted instead. Quitting.".format(args[7]))

        # Building up folder names and data from argv
        self.folder_run      = args[1] + '/'
        self.folder_plotsNew = args[2] + '/'
        self.plot_label      = args[3]
        self.plot_facecolor  = args[4]
        self.plot_val_inf    = float(args[5])
        self.plot_val_sup    = float(args[6])
        self.folder_tmp      = 'tmp/'
        self.folder_rawData  = 'data_rawData/'

        if 'vortex' in args:
            # This list contains all the string snippets that must be contained within the raw data filenames.
            # Its first element is either Y or Z and identifies the orientation of the slice to be plotted.
            # Stops when hitting 'vortex'.
            self.matching_strings = [args[n] for n in range( 7, len(args) ) if n < args.index('vortex')]
            self.xy_lims = [float(args[n]) for n in range(args.index('vortex')+1, args.index('vortex')+5)]
        else:
            # This list contains all the string snippets that must be contained within the raw data filenames.
            # Its first element is either Y or Z and identifies the orientation of the slice to be plotted.
            self.matching_strings = [args[n] for n in range( 7, len(args) )]
            self.xy_lims = None

        print('Plots performed perpendicular to axis', self.matching_strings[0])
        print('Superior value for the plot:', self.plot_val_sup)
        print('Parameters to be contained in rawData filename:', self.matching_strings)

        # Building the x1 and x2 grids
        file_x1 = [ item
                    for item in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' )
                    if all(snippet in item for snippet in ['coords', self.matching_strings[0], 'x.dat'])
                  ][0]
        file_x2 = [ item
                    for item in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' )
                    if all(snippet in item for snippet in ['coords', self.matching_strings[0], ('z' if self.matching_strings[0]=='Y' else 'y')+'.dat'])
                  ][0]
        self.x1 = np.genfromtxt( file_x1 )
        self.x2 = np.genfromtxt( file_x2 )

        # Creating output folder if does not exist
        Path( self.folder_run + self.folder_tmp + self.folder_plotsNew ).mkdir(parents=True, exist_ok=True)


    @abstractmethod
    def replot(self):
        """
        Abstract method to actually perform the replotting operations.
        To be implemented in the child classes.
        """
        pass


#######################################################################################################################
class GeoReplottingAlgorithm(BaseReplottingAlgorithm):
#######################################################################################################################
    """
    Replotting algorithm for the case of a scalar field concentration.
    """


    def __init__(self, args, **kwargs):
        """
        Constructor. Just calls the parent constructor without making anything else.
        """
        super(GeoReplottingAlgorithm, self).__init__(args, **kwargs)


    def replot(self):
        """
        Method to actually perform the replotting operations.
        """
        # Cycling all the raw data files containing matching_strings
        for file_rawData in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' ):
            if all( item in Path(file_rawData).name for item in self.matching_strings ):
                print('---> Processing', file_rawData, '...')
                plt.figure()

                # Retrieving output filename
                file_out = Path(file_rawData).name.split('.dat')[0].split('__')[1] + '.pdf'

                # Performing the plotting process
                pt.plotPhiPlane ( self.folder_run + self.folder_tmp + self.folder_plotsNew + file_out,
                                  self.x1,
                                  self.x2,
                                  np.genfromtxt( file_rawData ),
                                  self.plot_val_inf,
                                  self.plot_val_sup,
                                  self.plot_label,
                                  self.plot_facecolor,
                                  'cividis',
                                  True
                                )
                plt.close()

        print('Operations completed.\n')


#######################################################################################################################
class PhiReplottingAlgorithm(BaseReplottingAlgorithm):
#######################################################################################################################
    """
    Replotting algorithm for the case of a scalar field concentration.
    """


    def __init__(self, args, **kwargs):
        """
        Constructor. Just calls the parent constructor without making anything else.
        """
        super(PhiReplottingAlgorithm, self).__init__(args, **kwargs)


    def replot(self):
        """
        Method to actually perform the replotting operations.
        """
        # Cycling all the raw data files containing matching_strings
        for file_rawData in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' ):
            if all( item in Path(file_rawData).name for item in self.matching_strings ):
                print('---> Processing', file_rawData, '...')
                plt.figure()

                # Retrieving output filename
                file_out = Path(file_rawData).name.split('.dat')[0].split('__')[1] + '.pdf'

                # Performing the plotting process
                pt.plotPhiPlane ( self.folder_run + self.folder_tmp + self.folder_plotsNew + file_out,
                                  self.x1,
                                  self.x2,
                                  np.genfromtxt( file_rawData ),
                                  self.plot_val_inf,
                                  self.plot_val_sup,
                                  self.plot_label,
                                  self.plot_facecolor,
                                  'cividis'
                                )
                plt.close()

        print('Operations completed.\n')


#######################################################################################################################
class UReplottingAlgorithm(BaseReplottingAlgorithm):
#######################################################################################################################
    """
    Replotting algorithm for the case of the velocity field.
    """


    def __init__(self, args, **kwargs):
        """
        Constructor. Calls the parent constructor, and then alters matching_strings to make it single out velocity x files.
        """
        super(UReplottingAlgorithm, self).__init__(args, **kwargs)

        # Altering matching_strings
        self.matching_strings.append('vel')
        self.matching_strings.append('__x.dat')


    def replot(self):
        """
        Method to actually perform the replotting operations.
        """
        # Cycling all the raw data files containing matching_strings
        for fileX_rawData in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' ):
            if all( item in Path(fileX_rawData).name for item in self.matching_strings ):
                print('---> Processing', fileX_rawData, '...')
                plt.figure()

                # Retrieving output and y - z data filenames
                file_out = Path(fileX_rawData).name.split('__')[1] + '.pdf'
                fileY_rawData = fileX_rawData.split('x.dat')[0] + 'y.dat'
                fileZ_rawData = fileX_rawData.split('x.dat')[0] + 'z.dat'

                # Getting the velocity field
                ux = np.genfromtxt( fileX_rawData )
                uy = np.genfromtxt( fileY_rawData )
                uz = np.genfromtxt( fileZ_rawData )
                #uMag = np.sqrt(ux**2 + uz**2)  if self.matching_strings[0]=='Y' else np.sqrt(ux**2 + uy**2 + uz**2) // only for PIV
                uMag = np.sqrt(ux**2 + uy**2 + uz**2)
                
                # Plotting the vortex position if required
                if self.xy_lims is not None:
                    xV, yV = de.computeVortexLB(self.x1, self.x2, uMag, self.xy_lims[0], self.xy_lims[1], self.xy_lims[2], self.xy_lims[3])
                    plt.plot(xV, yV, 'r+', markersize=30)

                # Performing the plotting process
                pt.plotPlane ( self.folder_run + self.folder_tmp + self.folder_plotsNew + file_out,
                               self.x1,
                               self.x2,
                               ux,
                               (uz if self.matching_strings[0]=='Y' else uy),
                               uMag,
                               self.plot_val_inf,
                               self.plot_val_sup,
                               self.plot_label,
                               self.plot_facecolor,
                               'cividis'
                             )
                plt.close()

        print('Operations completed.\n')


#######################################################################################################################
class ViscoReplottingAlgorithm(BaseReplottingAlgorithm):
#######################################################################################################################
    """
    Replotting algorithm for the case of the scalar apparent viscosity field.
    """


    def __init__(self, args, **kwargs):
        """
        Constructor. Just calls the parent constructor without making anything else.
        """
        super(ViscoReplottingAlgorithm, self).__init__(args, **kwargs)

        # Altering matching_strings
        self.matching_strings.append('visco')


    def replot(self):
        """
        Method to actually perform the replotting operations.
        """
        # Cycling all the raw data files containing matching_strings
        for file_rawData in glob( self.folder_run + self.folder_tmp + self.folder_rawData + '*' ):
            if all( item in Path(file_rawData).name for item in self.matching_strings ):
                print('---> Processing', file_rawData, '...')
                plt.figure()

                # Retrieving output filename
                file_out = Path(file_rawData).name.split('.dat')[0].split('__')[1] + '.pdf'

                # Performing the plotting process
                pt.plotPhiPlane ( self.folder_run + self.folder_tmp + self.folder_plotsNew + file_out,
                                  self.x1,
                                  self.x2,
                                  np.genfromtxt( file_rawData ),
                                  self.plot_val_inf,
                                  self.plot_val_sup,
                                  self.plot_label,
                                  self.plot_facecolor,
                                  'cividis'
                                )
                plt.close()

        print('Operations completed.\n')


