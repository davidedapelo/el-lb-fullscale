from xml.dom import minidom

###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
class OLBxml:
###############################################################################################################################
###############################################################################################################################
###############################################################################################################################
    """
    Class to simplify usage of the minidom package, considering the specific requirements of xml input files for OpenLB (www.openlb.net).
    In particular:
    1. The root nome is called Param.
    2. Nodes do not have attributes.
    3. Child nodes can be either nodes or text elements with no attributes.
    Additional requirement:
    4. Multiple tags are not allowed, even if with different ancestors.
    """
    dir_resources = 'resources/'

###############################################################################################################################
    def __init__(self, **kwargs):
###############################################################################################################################
        """
        Initializes the class and imports parameters corresponding to coupling, simulation and time.
        """
        # Initializing the xml document 
        self._root = minidom.Document()
        # Creating the first node called Param
        self._defineNode('Param', self._root)
        if 'wholeInput' in kwargs: # If the whole set of parameters is passed as a single input XML file:
            self._importFromFile(kwargs['wholeInput'])
        else: # Else: importing pre-built XML files, either from kwargs, or from 0resources
            self._importFromFile(
                    kwargs['input_couplingSimul'] if 'input_couplingSimul' in kwargs
                    else (self.dir_resources + 'input_coupling-simul.xml')
                    )
            self._importFromFile(
                    kwargs['input_time'] if 'input_time' in kwargs
                    else (self.dir_resources + 'input_time.xml')
                    )
            self._importFromFile(
                    kwargs['input_biokinetics'] if 'input_biokinetics' in kwargs
                    else (self.dir_resources + 'input_biokinetics-Bernard.xml')
                    )

###############################################################################################################################
    def __checkIfAlreadyDefined(self, tag):
###############################################################################################################################
        """
        Checks if a tag is already present in the xml document, irrespective of its ancestors.
        Raises an error if it is.
        Args:
            tag: str: tag to be checked
        """
        if len( self._root.getElementsByTagName(tag) ) > 0:
            raise ValueError('Tag ' + tag + ' already is defined.')

###############################################################################################################################
    def __checkIfContainsOnly(self, item, values):
###############################################################################################################################
        """
        Checks if a string contains only a given set of characters.
        Args:
            item:   str:   Input string to check
            values: [str]: List of characters, which item is checked against
        Return:
            (...)  bool: True if item is made of value only; False otherwise
        """
        for element in item:
            if not element in values:
                return False
        return True

###############################################################################################################################
    def __scrollForAlreadyDefined(self, node):
###############################################################################################################################
        """
        Recursively scrolls all the offspring of a node to look for nodes being already defined in the document.
        Args:
            node: minidom.Element: Node to check
        """
        if isinstance(node, minidom.Element):
            self.__checkIfAlreadyDefined(node.tagName)
            for item in node.childNodes:
                self.__scrollForAlreadyDefined(item)

###############################################################################################################################
    def _defineNode(self, tag, parent=None, text=None):
###############################################################################################################################
        """
        Generalizes the operation of populating a minidom document.
        Args:
            tag:     str:                                Name of the created node
            parent:  xml.dom.minidom.(Document/Element): Where the created element is appended to
            text:    str:                                If not None, Text contained inside the created node
        Return:
            element: xml.dom.minidom.Element:            Created node
        """
        self.__checkIfAlreadyDefined(tag)
        if parent == None:
            parent = self._root.childNodes[0]
        element = self._root.createElement(tag)
        parent.appendChild(element)
        if text is not None:
            elementText = self._root.createTextNode(text)
            element.appendChild(elementText)
        return element

###############################################################################################################################
    def exportToFile(self, filename):
###############################################################################################################################
        """
        Exports an XML to file.
        Args:
            filename: str: Name of the output XML file
        """
        with open(filename, "w") as f:
            f.write(
                    '\n'.join( [
                        item
                        for item in self._root.toprettyxml(indent ="\t").split('\n')
                        if not self.__checkIfContainsOnly(item, ['\t', ' '])
                        ] )
                    )

###############################################################################################################################
    def getNodeContent(self, tag):
###############################################################################################################################
        """
        Gets the text content of a node, and raises an error if not present.
        Args:
            tag:  str: Name of the required node
        Returns:
            (...) str: Text content of the node
        """
        nodes = self._root.getElementsByTagName(tag)
        if len(nodes) == 0:
            raise ValueError('Node ' + tag + ' not defined.')
        content = nodes[0].childNodes[0].nodeValue
        if self.__checkIfContainsOnly(content, ['\t', ' ', '\n']):
            raise ValueError('blank content for node ' + tag)
        return content

###############################################################################################################################
    def _importFromFile(self, filename):
###############################################################################################################################
        """
        Imports an XML from a file to an existing XML document object.
        Args:
            filename: str: Name of the XML file to read from
        """
        for item in minidom.parse(filename).childNodes[0].childNodes:
            self.__scrollForAlreadyDefined(item)
            node = self._root.importNode(item, True)
            self._root.childNodes[0].appendChild(node)
