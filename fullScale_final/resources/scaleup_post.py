#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys
from glob import glob
from pathlib import Path

ncores = 40 # Number of cores per node

if len(sys.argv) < 2:
    raise ValueError('At leaset ONE argv argument (==nx) must by inputted. Quitting.')

def get_scaling_series(nx, scaling_series):
    """Builds a series [[x], [y]] of scaling-up tests.
    Args:
        ncores:         int:   number of cores per node
        nx:             int:   number of grid points per side
        scaling_series: [int]: scaling factors
    Returns:
        [[float], [float]]: [[numbers of cores], [CPUs]]
    """
    def get_xy(nx, scaling):
        """Reads the scaleup log files and extracts the scaleup data.
        Args:
            nx:      int: number of grid points per side
            scaling: int: scaling factor
        Returns:
            x: int:   number of cores
            y: float: CPUs
        """
        logfile = 'git_ignore/scaleup_' \
                + str(scaling) \
                + '_TS=0__flow=05__d=050__nuCorr=0.25/nx0=60__uLB0=0.15__subC=20__ball__antiDiff=0.0/' \
                + str(nx) \
                + '-0/log-0.txt'
    
        with open (logfile, 'r') as f:
            for line in f:
                if ('[Timer] measured time (cpu):' in line):
                    return [ ncores*scaling, float( line.split('(cpu): ')[1].split('s')[0] ) ]

        raise EOFError('File ' + logfile + " not containing  string '[Timer] measured time (cpu):'")
    
    # Building the data series
    data = []
    for scaling in scaling_series:
        data.append(get_xy(nx, scaling))

    # Normalizing the CPUs against the first entry
    for item in reversed(data): # need to be transversed in reverse order otherwise messes up items after the first one
        item[1] = data[0][1] / item[1]

    return np.array(data).T

folder_name = 'git_ignore/scaleup'
# Creating output folder if does not exist
Path(folder_name).mkdir(parents=True, exist_ok=True)

def processor_abs(dataX, dataY, multiplier=None):
    return dataY

def processor_rel(dataX, dataY, multiplier):
    return dataY/dataX*ncores*multiplier

def scaleup(processor, y_label, fig_basename, nx_list):
    fig_main = plt.figure() # initializing the main figure
    ax_main = fig_main.add_subplot(1,1,1) # adding axes to the convergence figure

    # Plotting the data serieses
    scaling_first, scaling_min, scaling_max = None, None, None
    for nx in nx_list:
        # Getting the scaling series (viz.: the series of number of nodes used in the simulations)
        scaling_list = []
        for item in glob(folder_name + '_*/*/*' + nx + '*'):
            scaling_list.append( int( item.split('/')[1].split('_')[1] ) )
        scaling_list.sort()

        # Coherence check
        if scaling_first == None:
            scaling_first = scaling_list[0]
        else:
            if not scaling_first == scaling_list[0]:
                raise ValueError('Processing two serieses with different initial scaling values.')

        # Gathering data and plotting hem
        data = get_scaling_series(int(nx), scaling_list)
        #plt.plot(data[0], data[1], '-X', label='nx = '+nx)
        plt.plot ( data[0], 
                   processor(data[0], data[1], scaling_first), 
                   '-X', label='nx = '+nx
                 )

        scaling_min = scaling_list[ 0] if scaling_min==None else (scaling_min if scaling_min<scaling_list[ 0] else scaling_list[ 0])
        scaling_max = scaling_list[-1] if scaling_max==None else (scaling_max if scaling_max>scaling_list[-1] else scaling_list[-1])

    # Plotting the theoretical scaleup
    plt.plot( np.array([scaling_min, scaling_max])*ncores,
              processor( np.array([scaling_min, scaling_max])*ncores, np.array([scaling_min, scaling_max])/scaling_min, scaling_first ),
              ':k', label='Theoretical'
            )

    # Stuff to do with the picture
    ax_main.legend(frameon=False, loc='upper left')
    ax_main.set_xlabel('Number of cores')
    ax_main.set_ylabel(y_label)
    fig_main.tight_layout()
    fig_main.savefig(folder_name + fig_basename + '_'.join(sys.argv[1:]) + '.pdf', format='pdf')

scaleup(processor_abs, 'Scaling', '/scaleupAbs_', sys.argv[1:])
scaleup(processor_rel, 'Scaling', '/scaleupRel_', sys.argv[1:])