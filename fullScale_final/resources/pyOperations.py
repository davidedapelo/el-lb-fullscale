import glob
import math
import matplotlib.pyplot as plt
import numpy as np
import re
import warnings
from glob import glob
from pathlib import Path
from scipy.interpolate import interp2d
from scipy.optimize import minimize

from resources.OLBxml import OLBxml

import resources.pyResources.pyResources.plotting as pt

###############################################################################################################################
def createFolderRun(folder_base, folderRun_prefix):
###############################################################################################################################
    """
    Creates the folder name for the current run, labelled with a progressive number.
    Args:
        folder_base:      str: Folder name containing all the run series
        folderRun_prefix: str: Prefix of the folder name for the current run. The progressive number follows.
    Returns:
        (...)             str: Folder name for the current run, completed with a progressive number
    """
    counter = 0
    while Path(folder_base + folderRun_prefix + str(counter) + '/allDone').exists():
        counter = counter + 1

    return folderRun_prefix + str(counter) + '/'

###############################################################################################################################
def createLogFilename(folderRun, logFile_prefix):
###############################################################################################################################
    """
    Creates the log filename for the current run, labelled with a progressive number.
    Args:
        folderRun:      str: Folder name containing the current run
        logFile_prefix: str: Prefix of the log filename. The progressive number follows.
    Returns:
        (...)           str: Log filename for the current run
    """
    counter = 0
    while Path(folderRun + logFile_prefix + str(counter) + '.txt').exists():
        counter = counter + 1

    return logFile_prefix + str(counter) + '.txt'

###############################################################################################################################
def createNames_base(olbXML, data, folder_base):
###############################################################################################################################
    """
    Creates the folder name containing all the run series, the folder name for the current run and the log filename.
    Generating sequence: git_ignore/<TS>__<flow>__<d>__<nuCorr>/<nx0>__<uLB0>__<subC>.
    Args:
        olbXML:      OLBxml: XML generator
        data:        dict:   Dictionary containing the custom input to the xml
        folder_base: str:    Folder name containing all the run series
    Returns:
        folder_base: str:    Folder name containing all the run series
        folderRun:   str:    Folder name for the current run
        logFilename: str:    Log filename for the current run
    """
    folderRun = createFolderRun(folder_base, str(data['nx']) + '-')
    logFilename = createLogFilename(folder_base+folderRun, 'log-')

    return folder_base, folderRun, logFilename

###############################################################################################################################
def createNames_async(olbXML, data, prefix=''):
###############################################################################################################################
    """
    Creates the folder name containing all the run series, the folder name for the current run and the log filename.
    Generating sequence: git_ignore/<TS>__<flow>__<d>__<nuCorr>/<nx0>__<uLB0>__<subC>__<runType>[__<antiDiff>[__<antiDiffusionTuning>]].
    Args:
        olbXML:      OLBxml: XML generator
        data:        dict:   Dictionary containing the custom input to the xml
        prefix:      str:    Prefix to folder filename
    Returns:
        folder_base: str:    Folder name containing all the run series
        folderRun:   str:    Folder name for the current run
        logFilename: str:    Log filename for the current run
    """
    folder_base = 'git_ignore/' + prefix + 'TS=' + str(data['rheoIndex']) + "__flow=" + data['flowRateRatioStr'] + "__d=" + data['diamStr'] \
            + "__nuCorr=" + str(data['viscoCorrection']) + "/nx0=" + str(data['nx0']) + "__uLB0=" + str(data['uLB0']) \
            + "__subC=" + str(data['subcycles']) + "__" + data['runType']
    #if data['runType'] == 'hydro':
        #folder_base += (("__antiDiff="+str(data['antiDiffusionTuning'])) if 'antiDiffusionTuning' in data else '')
    folder_base += '/'

    return createNames_base(olbXML, data, folder_base)

###############################################################################################################################
def createNames_std(olbXML, data, prefix=''):
###############################################################################################################################
    """
    Creates the folder name containing all the run series, the folder name for the current run and the log filename.
    Generating sequence: git_ignore/<TS>__<flow>__<d>__<nuCorr>/<nx0>__<uLB0>__<subC>__<seedingType>__<antiDiff>[__<antiDiffusionTuning>].
    Args:
        olbXML:      OLBxml: XML generator
        data:        dict:   Dictionary containing the custom input to the xml
        prefix:      str:    Prefix to folder filename
    Returns:
        folder_base: str:    Folder name containing all the run series
        folderRun:   str:    Folder name for the current run
        logFilename: str:    Log filename for the current run
    """
    folder_base = 'git_ignore/' + prefix + 'TS=' + str(data['rheoIndex']) + "__flow=" + data['flowRateRatioStr'] + "__d=" + data['diamStr'] \
            + "__nuCorr=" + str(data['viscoCorrection']) + "/nx0=" + str(data['nx0']) + "__uLB0=" + str(data['uLB0']) \
            + "__subC=" + str(data['subcycles']) + "__" + data['seedingType'] \
            + (("__antiDiff="+str(data['antiDiffusionTuning'])) if 'antiDiffusionTuning' in data else '') + '/'

    return createNames_base(olbXML, data, folder_base)

###############################################################################################################################
def standardOperations_pre(tmpFilename, data, olbXML, createNames):
###############################################################################################################################
    """
    Performs all the pre-processing standard operations consisting of:
    1. Creating the folders for the runs;
    2. Creating the input XML file.
    Args:
        tmpFilename:       str:      Filename of the temporary file storing the job data
        data:              dict:     Dictionary containing the custom input to the xml
        olbXML:            OLBxml:   XML generator
        createFolderNames: callable: function creating the folder names
    """

#==============================================================================================================================
    def getUncommentedFlagFromFile(fileName, flag):
#==============================================================================================================================
        """
        Returns the value of a flag standing right to the mark ':= ', from a file.
        The string must be uncommented. Raises exception if more than one valid values of the flag are found.
        Args:
            fileName: str: Name of the file to search the flag from
            flag:     str: String to be searched
        Returns:
            (...):    str: Value of the flag
        """
        value = []
        for line in open(fileName):
            if flag in line and ':=' in line and not '#' in line:
                value.append( line.rstrip().split(':= ')[1] )
                if len(value) > 1:
                    raise ValueError("More than one occurrence of '" + flag + " :=' found in file " + fileName)
        if len(value) == 0:
            raise ValueError("no occurrences of '" + flag + " :=' found in file " + fileName)
        return value[0]

#==============================================================================================================================
    def getUncommentedListOfFlagsFromFile(fileName, flag):
#==============================================================================================================================
        """
        Returns all the values of a flag from a file. The lines must be uncommented.
        Args:
            fileName: str:   Name of the file to search the flag from
            flag:     str:   String to be searched
        Returns:
            (...):    [str]: Values of the flag
        """
        values = []
        for line in open(fileName):
            if flag in line and '=' in line and not '#' in line:
                for item in line.rstrip().split('= ')[1].split(' '):
                    values.append(item)
        return values

    # Getting olb's root path from definitions.mk
    rootPath = getUncommentedFlagFromFile('Makefile', 'OLB_ROOT')
    # Getting the definition of CXXFLAGS: either OPTIM or DEBUG expected.
    cxxFlags = getUncommentedListOfFlagsFromFile(rootPath+'/config.mk', 'CXXFLAGS')
    # Checking that flag's value is correct
    if '-DOLB_DEBUG' in cxxFlags:
        debug = 'DEBUG'
        if '-O3' in cxxFlags:
            raise ValueError('Wrong value of CXXFLAGS=' + cxxFlags)
    elif '-O3' in cxxFlags:
        debug = 'OPTIM'
    else:
        raise ValueError('Wrong value of CXXFLAGS=' + cxxFlags)

    # GEtting the executable's name'
    cppFilesAtHome = glob("*.cpp")
    if not len(cppFilesAtHome) == 1:
        raise ValueError("There should be ONE cpp file in app's home folder; instead there are " + str(len(cppFilesAtHome)))
    executableFilename = cppFilesAtHome[0].split('.')[0]

    # Generating folder paths
    folderName, folderRun, logFilename = createNames(olbXML, data)
    # Creating them if not already existing
    Path(folderName).mkdir(parents=True, exist_ok=True)
    Path(folderName + folderRun).mkdir(parents=True, exist_ok=True)

    # Writing XML files
    olbXML.exportToFile(folderName + folderRun + 'input.xml') # this will be actually used
    olbXML.exportToFile(folderName + folderRun + 'input-backup.xml') # this is stored in case of manual tinkering later

    # Removing lastAttempt file if present, and reset attemptId in operation XML file by rewriting it
    if Path(folderName + folderRun + 'lastAttempt').is_file():
         Path(folderName + folderRun + 'lastAttempt').unlink()
         operationData = ''
         for line in open( Path(folderName + folderRun + 'operation'), 'r' ):
             stripped = line.strip()
             operationData += ('    <attemptId>0</attemptId>' if 'attemptId' in stripped else stripped) + '\n'
         with open( Path(folderName + folderRun + 'operation'), 'w' ) as f:
             f.write(operationData)

    # Writing temp file
    with open(tmpFilename, 'w') as f:
        f.write(
                folderName         + '\n' +
                folderRun          + '\n' +
                logFilename        + '\n' +
                debug              + '\n' +
                executableFilename + '\n'
                )

###############################################################################################################################
def standardOperations_post(folderName, folderRun, logFilename):
###############################################################################################################################
    """
    Performs all the post-processing standard operations consisting of:
    1.Hello
    Args:
        folder_base: str: Folder name containing all the run series
        folderRun:   str: Folder name for the current run
        logFilename: str: Log filename for the current run
    """
    olbXML = OLBxml(wholeInput = folderName + folderRun + 'input.xml')

#==============================================================================================================================
    def read_ylims(tag):
#==============================================================================================================================
        """
        Gets the content of a ylims node.
        The node must be in the form <tag>ymin ymax</tag>.
        Args:
            tag:   str:            Name of the node containing ylims
        Returns:
            (...): [float, float]: [ymin, ymax] values for the vortex's plot
        """
        try:
            ylims_str = olbXML.getNodeContent(tag)
            return [float(item) for item in ylims_str.split(' ')]
        except ValueError:
            return None

    vortexOverTime(olbXML, folderName, folderRun, logFilename, read_ylims('ylimsX'), read_ylims('ylimsY'))
    uniformityIndexOverTime(olbXML, folderName, folderRun, logFilename)

###############################################################################################################################
def uniformityIndexOverTime(olbXML, folderName, folderRun, logFilename):
###############################################################################################################################
    """
    Works out the uniformity index (Terashima, 2009) at each timestep.
    Args:
        olbXML:      OLBxml: XML generator
        folder_base: str:    Folder name containing all the run series
        folderRun:   str:    Folder name for the current run
        logFilename: str:    Log filename for the current run
    """
    # Ignore warnings
    warnings.filterwarnings("ignore")    

#==============================================================================================================================
    def readFromLog(logFile, pattern):
#==============================================================================================================================
        """
        Extracts a value from a log file.
        Gets the lines matching a pattern, and returns the lost of the corresponding value at the right of the '=' char.
        Args:
            logFile: str:     File the value is extracted from
            pattern: str:     Pattern identifying the line bringing the wanted value
        Returns:
            (...):   [float]: Returned values
        """
        values = []
        # Scrolling the file line by line
        for line in open(logFile, 'r'):
            # First line matching pattern
            if re.match("(.*)"+pattern+"(.*)", line):
                # Getting the value at the right of the '=' char, converted into float
                values.append( float (line.rstrip().split('=')[1] ) )
        # Raises exception if no line matches the pattern
        if values == []:
            raise ValueError('Pattern ' + pattern + ' not found in file ' + logFile)
        return values

    # Collecting the variables common to all the timesteps
    tVtm   = float(olbXML.getNodeContent('Tstatus'))
    outDir = folderName + folderRun + 'tmp/uniformityIndexOverTime/'
    # Getting the variables of this postprocessing
    snapshots = [ float(item) for item in olbXML.getNodeContent('UIsnapshots').split(' ') ]
    UI        = readFromLog(folderName+folderRun+logFilename, 'UI')
    t         = [ tVtm*item for item in range(len(UI)) ]
    # Preparing output
    Path(outDir).mkdir(parents=True, exist_ok=True)
    f = open(outDir+'UI.txt', 'w')
    g = open(outDir+'UIsnapshots.txt', 'w')
    for i in range(len(UI)):
        f.write(str(t[i]) + ' ' + str(UI[i]) + '\n')
        if t[i] in snapshots:
            g.write(str(t[i]) + ' ' + str(UI[i]) + '\n')
    f.close()
    g.close()
    # Plotting
    print('Plotting the Uniformity Index ... ')
    plt.plot(t, UI, 'k-')
    plt.xlabel('time (s)')
    plt.ylabel('Uniformity Index')
    plt.tight_layout()
    plt.savefig(outDir+'UI.pdf', format='pdf')
    plt.close()

###############################################################################################################################
def vortexOverTime(olbXML, folderName, folderRun, logFilename, ylimsX=None, ylimsY=None):
###############################################################################################################################
    """
    Works out the vortex position at each timestep.
    Args:
        olbXML:      OLBxml:      XML generator
        folder_base: str:         Folder name containing all the run series
        folderRun:   str:         Folder name for the current run
        logFilename: str:         Log filename for the current run
        ylimsX:   [float, float]: [ymin, ymax] values for the vortex's X plot
        ylimsY:   [float, float]: [ymin, ymax] values for the vortex's Y plot
    """
    # Ignore warnings
    warnings.filterwarnings("ignore")    

#==============================================================================================================================
    def readFromLog(logFile, pattern):
#==============================================================================================================================
        """
        Extracts a value from a log file.
        Gets the first line matching a pattern, and returns the corresponding value at the right of the '=' char.
        Args:
            logFile: str:   File the value is extracted from
            pattern: str:   Pattern identifying the line bringing the wanted value
        Returns:
            (...):   float: Returned value
        """
        # Scrolling the file line by line
        for line in open(logFile, 'r'):
            # First line matching pattern
            if re.match("(.*)"+pattern+"(.*)", line):
                # Getting the value at the right of the '=' char, converted into float
                return float (line.rstrip().split('=')[1] )
        # Raises exception if no line matches the pattern
        raise ValueError('Pattern ' + pattern + ' not found in file ' + logFile)

    # Collecting the variables common to all the timesteps
    deltaT  = readFromLog(folderName+folderRun+logFilename, 'physDeltaT')
    outDir  = folderName + folderRun + 'tmp/vortexOverTime/'
    rawData = folderName + folderRun + 'tmp/data_rawData/'
    tAvgVort= float(olbXML.getNodeContent('TavgVort'))
    uLegend = float(olbXML.getNodeContent('uLegendOffs')) if olbXML.getNodeContent('geoType')=='lab' else float(olbXML.getNodeContent('uLegend0'))
    x1      = np.genfromtxt( glob(rawData + 'coordsY_*x.dat')[0] )
    x2      = np.genfromtxt( glob(rawData + 'coordsY_*z.dat')[0] )
    xGuess  = float(olbXML.getNodeContent('xGuess'))
    yGuess  = float(olbXML.getNodeContent('yGuess'))
    xMax    = float(olbXML.getNodeContent('xMax'))
    xMin    = float(olbXML.getNodeContent('xMin'))
    yMax    = float(olbXML.getNodeContent('yMax'))
    yMin    = float(olbXML.getNodeContent('yMin'))
    # Preparing output
    Path(outDir).mkdir(parents=True, exist_ok=True)
    f = open(outDir+'coordinates.txt', 'w')
    # Preparing the time series and averages
    i = 0
    t = []
    x = { 'x': [], 'avg': 0.0, 'std': 0.0 }
    y = { 'x': [], 'avg': 0.0, 'std': 0.0 }

    # Scrolling the x datafiles, sorted according to number of timesteps
    for fileUx in sorted( glob(rawData + 'velY*x.dat'), key = lambda item: int(item.split('__x')[0].split('__')[-1]) ):
        # Variables relative to this timestep
        ux = np.genfromtxt( fileUx )
        uz = np.genfromtxt( fileUx.split('x.dat')[0] + 'z.dat' )
        timestep = fileUx.split('__x')[0].split('__')[-1]
        # 2D function, which interpolates the velocity magnitude field
        interpolated = interp2d( x1[0,:], x2[:,0], np.sqrt(ux**2 + uz**2) )
        # Minimum of the interpolated function, around input guess and within input boundary window
        minimized = minimize(lambda arg: interpolated(*arg), (xGuess, yGuess), bounds=((xMin, xMax), (yMin, yMax)))
        # Writing on file & updating time serieses
        print('Plotting vortex position for time ' + str(deltaT*int(timestep)) + ' ...')
        f.write(str(float(timestep)*deltaT) + '  ' + str(minimized.x[0]) + '  '  + str(minimized.x[1]) + '\n')
        if not int(timestep) == 0:
            t.append(float(timestep)*deltaT)
            x['x'].append(minimized.x[0])
            y['x'].append(minimized.x[1])
        # Updating the averages
        if float(timestep)*deltaT >= tAvgVort:
            i = i + 1
            x['avg'] = x['avg'] + minimized.x[0]
            y['avg'] = y['avg'] + minimized.x[1]
            x['std'] = x['std'] + minimized.x[0]**2
            y['std'] = y['std'] + minimized.x[1]**2
        # Plotting
        plt.plot(minimized.x[0], minimized.x[1], 'r+', markersize=30)
        pt.plotPlane(outDir+str(deltaT*int(timestep))+'.pdf', x1, x2, ux, uz,  np.sqrt(ux**2 + uz**2), uLegend)
        plt.close()

    # Normalizing the averages
    x['avg'] = x['avg'] / i
    y['avg'] = y['avg'] / i
    x['std'] = math.sqrt(x['std']/i - x['avg']**2) / math.sqrt(i-1)
    y['std'] = math.sqrt(y['std']/i - y['avg']**2) / math.sqrt(i-1)
    # Writing the averages to file:
    with open(outDir+'coordinatesAvg.txt', 'w') as g:
        g.write( str(x['avg']) + ' ' + str(x['std']) + ' ' + str(y['avg']) + ' ' + str(y['std']) )

#==============================================================================================================================
    def plotTimeSeries(series, titleY, filename, ylims=None):
#==============================================================================================================================
        """
        Plots a time series against the time coordinate t defined above.
        Args:
            series:   dict:           Series to be plotted:
                                      x:   [float]: Time series
                                      avg: float:   Average
                                      std: float:   Standard deviation
            titleY:   str:            Title of the Y axis
            filename: str:            Output filename (not the full path)
            ylims:    [float, float]: [ymin, ymax] values for the vortex's plot
        """
        print("Plotting time series titled '" + titleY + "' ...")
        plt.plot(t, series['x'], 'k-')
        plt.plot(t, [series['avg'] for item in t], 'k--')
        plt.plot(t, [series['avg']+series['std'] for item in t], 'k:')
        plt.plot(t, [series['avg']-series['std'] for item in t], 'k:')
        if not ylims == None:
            plt.ylim(ylims[0], ylims[1])
        plt.xlabel('time (s)')
        plt.ylabel(titleY)
        plt.tight_layout()
        plt.savefig(outDir+filename, format='pdf')
        plt.close()

#==============================================================================================================================
    def plotBothTimeSeries(seriesX, seriesY, titleX, titleY, filename):
#==============================================================================================================================
        """
        Plots x & y series against the time coordinate t defined above.
        Args:
            seriesX:  dict: Series to be plotted: X
                            x:   [float]: Time series
                            avg: float:   Average
                            std: float:   Standard deviation
            seriesY:  dict: Series to be plotted: Y
                            x:   [float]: Time series
                            avg: float:   Average
                            std: float:   Standard deviation
            titleX:   str:  Title of the X axis
            titleY:   str:  Title of the Y axis
            filename: str:  Output filename (not the full path)
        """
        print("Plotting the complete '" + titleX + "' and '" + titleY + "' ...")
        plt.plot(t, seriesX['x'], 'b-', label=titleX)
        plt.plot(t, seriesY['x'], 'g-', label=titleY)
        plt.plot(t, [seriesX['avg'] for item in t], 'b--')
        plt.plot(t, [seriesY['avg'] for item in t], 'g--')
        plt.plot(t, [seriesX['avg']+seriesX['std'] for item in t], 'b:')
        plt.plot(t, [seriesX['avg']-seriesX['std'] for item in t], 'b:')
        plt.plot(t, [seriesY['avg']+seriesY['std'] for item in t], 'g:')
        plt.plot(t, [seriesY['avg']-seriesY['std'] for item in t], 'g:')
        plt.xlabel('time (s)')
        plt.ylabel('Distance (m)')
        plt.legend(frameon=False, loc='upper right')
        plt.tight_layout()
        plt.savefig(outDir+filename, format='pdf')
        plt.close()

    plotTimeSeries(x, 'Vortex X position (m)', 'coordinatesX.pdf', ylimsX)
    plotTimeSeries(y, 'Vortex Y position (m)', 'coordinatesY.pdf', ylimsY)
    plotBothTimeSeries(x, y, 'Vortex X position', 'Vortex Y position', 'coordinates.pdf')
