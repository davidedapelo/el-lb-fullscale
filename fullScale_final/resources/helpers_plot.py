import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from scipy.optimize import curve_fit

###############################################################################################################################
def plotUI(data, filename, titleX, titleY, location, fitting_funct=None, mult_conv=0):
###############################################################################################################################
    """
    Plots the uniformity index across the linear grid size.
    Args:
        data: Pandas DataFrame with the following entries:
            [index]   <nx>:    int:   <Linear grid size>
            [columns] <float>: float: list of UI values over <nx>, against time snapshot
        filename:       str:      Output filename (full path)
        titleX:         str:      Title of the X axis
        titleY:         [str]:    Title of the Y axis: for the main plot, and for the convergence in case
        location:       str:      Legend location
        fitting_funct:  callable: function of the type y = f(x, par1, ..., parN)
    """
    print("Plotting UI sequences titled '" + titleY[0] + "\nlocated in " + filename  + "' ...")
    fig_main = plt.figure() # initializing the main figure
    fig_conv = plt.figure() # initializing the main figure
    ax_main = fig_main.add_subplot(1,1,1) # adding axes to the convergence figure
    ax_conv = fig_conv.add_subplot(1,1,1) # adding axes to the convergence figure

    if not fitting_funct==None:
        for snapshot in data:
            popt, _ = curve_fit(fitting_funct, data.index, data[snapshot])
            #chisqr = np.sum( ((data['avg']-fitting_funct(data.index, *popt))/data['std'])**2 )
            plottedData, = ax_main.plot ( data.index,
                                         data[snapshot],
                                         label=snapshot
                                       )
            ax_main.plot ( data.index,
                           fitting_funct(data.index, *popt),
                           color = plottedData.get_color()
                         )

            print("Plotting UI sequences titled '" + titleY[1] + "\nlocated in " + filename  + "' ...")
            lastValue = fitting_funct(data.index[-1], *popt)
            ax_conv.loglog ( data.index[:-1],
                             abs(fitting_funct(data.index[:-1], *popt)-lastValue),
                             label=snapshot
                           )
        ax_conv.loglog ( data.index[:-1],
                         mult_conv/(data.index[:-1]*data.index[:-1]),
                         linestyle=':',
                         color='k',
                         label='2nd order of convergence'
                       )

    else:
        data.plot(ax=ax_main)

    ax_main.legend(frameon=False, loc=location)
    ax_conv.legend(frameon=False, loc='lower left')
    ax_main.set_xlabel(titleX)
    ax_conv.set_xlabel(titleX)
    ax_main.set_ylabel(titleY[0])
    ax_conv.set_ylabel(titleY[1])
    fig_main.tight_layout()
    fig_conv.tight_layout()
    fig_main.savefig(filename, format='pdf')
    fig_conv.savefig(str(Path(filename).with_suffix(''))+'_conv.pdf', format='pdf')
    plt.close(fig_main)
    plt.close(fig_conv)


###############################################################################################################################
def plotVortexCoordinate(data, filename, titleX, titleY, location, fitting_funct=None, fitting_legend=None, mult_conv=0):
###############################################################################################################################
    """
    Plots the vortex average coordinates across the linear grid size.
    Args:
        data: Pandas DataFrame with the following entries:
            [index]  <nx>: int:   <Linear grid size>
            [column] avg:  float: Average across <nx>
            [column] std:  float: Standard deviation across <nx>
        filename:       str:      Output filename (full path)
        titleX:         str:      Title of the X axis
        titleY:         [str]:    Title of the Y axis: for the main plot, and for the convergence in case
        location:       str:      Legend location
        fitting_funct:  callable: function of the type y = f(x, par1, ..., parN)
        fitting_legend: [[str]]:  the legend for the fitting function, made up of pieces of the function itself.
                                  1st element for the main plot, and the second for the convergence in case
        mult_conv:      float:    Multiplicative constant to shift the 2nd-order line
    """
    print("Plotting time averages titled '" + titleY[0] + "\nlocated in " + filename + "' ...")
    fig_main = plt.figure() # initializing the main figure
    fig_conv = plt.figure() # initializing the main figure
    ax_main = fig_main.add_subplot(1,1,1) # adding axes to the convergence figure
    ax_conv = fig_conv.add_subplot(1,1,1) # adding axes to the convergence figure
    data.plot ( y='avg',
                yerr='std',
                linestyle=':',
                color='k',
                capsize=6,
                label='Simulated data',
                ax=ax_main
              )
    if not fitting_funct==None:
        popt, _ = curve_fit(fitting_funct, data.index, data['avg'], sigma=data['std'])
        chisqr = np.sum( ((data['avg']-fitting_funct(data.index, *popt))/data['std'])**2 )
        if not len(fitting_legend[0]) == len(popt) + 1:
            raise IndexError('The size of fitting_legend is not the size of the fitted function plus 1.')
        ax_main.plot ( data.index,
                       fitting_funct(data.index, *popt),
                       color = 'r',
                       label = fitting_legend[0][0] \
                             + ' '.join([ ('-' if popt[i]<0 else '+') \
                                          + '(' + '{:.2e}'.format(abs(popt[i])) + ')' \
                                          + fitting_legend[0][i+1] \
                                          for i in range(len(popt)) \
                                       ])
                     )
        print("Best fit parameters = " + str(popt))
        print("Chi square = " + str(chisqr) + '\n')
        lastValue = fitting_funct(data.index[-1], *popt)
        ax_conv.loglog ( data.index[:-1],
                         abs(fitting_funct(data.index[:-1], *popt)-lastValue),
                         color='k',
                         label = fitting_legend[1][0] \
                               + ' '.join([ ('-' if popt[i]<0 else '+') \
                                            + '(' + '{:.2e}'.format(abs(popt[i])) + ')' \
                                            + fitting_legend[1][i+1] \
                                            for i in range(len(popt)) \
                                         ])
                       )
        ax_conv.loglog ( data.index[:-1],
                         mult_conv/(data.index[:-1]*data.index[:-1]),
                         linestyle=':',
                         color='r',
                         label='2nd order of convergence'
                       )
    ax_main.set_xlabel(titleX)
    ax_conv.set_xlabel(titleX)
    ax_main.set_ylabel(titleY[0])
    ax_conv.set_ylabel(titleY[1])
    ax_main.legend(frameon=False, loc=location)
    ax_conv.legend(frameon=False, loc='upper right')
    fig_main.tight_layout()
    fig_conv.tight_layout()
    fig_main.savefig(filename, format='pdf')
    fig_conv.savefig(str(Path(filename).with_suffix(''))+'_conv.pdf', format='pdf')
    plt.close(fig_main)
    plt.close(fig_conv)

