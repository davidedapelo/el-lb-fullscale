#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import IndexedGrampa_SortingAlgorithm as sortingAlgorithm
from helpers_data import extractData_UI as extractData
from helpers_plot import plotUI

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 4:
        raise ValueError('Exactly THREE argv argument (== first part of the path of the folder containing the runs; Nx; tag) must by inputted. Quitting.')

    def funct (x, a, b, c):
        return a + b/x + c/x**2

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/ui_' + str(sys.argv[2])).mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'd', sortingAlgorithm(int(sys.argv[2]), "d=", "__", [sys.argv[3], 'antiDiff', 'subC=100']))
    plotUI ( data,
             sys.argv[1]+'/ui_' + str(sys.argv[2])+'/ui_'+sys.argv[3]+'.pdf',
             'Bubble diameter (mm)',
             ['Uniformity index', 'Error on Uniformity Index'],
             'upper left',
             funct,
             10
           )
