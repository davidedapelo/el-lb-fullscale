#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import Parent_Index_Occurences_SortingAlgorithm as SortingAlgorithm
from helpers_data import extractData_UI as extractData
from helpers_plot import plotUI

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 2:
        raise ValueError('Exactly ONE argv argument (==path of the folder containing the runs) must by inputted. Quitting.')
    if not Path(sys.argv[1]).is_dir():
        raise ValueError('Folder ' + sys.argv[1] + ' does not exist or is not a directory. Quitting.')

    def funct (x, a, b, c, d, e):
        return a + b/x + c/x**2 + d/x**3 + e/x**4

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/UI/').mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'nx', SortingAlgorithm())
    plotUI ( data,
             sys.argv[1]+'/UI/UI.pdf',
             'Lattice nodes across the X direction',
             ['Uniformity index', 'Error on Uniformity Index'],
             'upper left',
             funct,
             100
           )
