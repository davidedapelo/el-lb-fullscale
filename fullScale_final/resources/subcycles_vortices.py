#! /usr/bin/python3

import sys
from pathlib import Path

from helpers_folders import IndexedParent_SortingAlgorithm as sortingAlgorithm
from helpers_data import extractData_vortices as extractData
from helpers_plot import plotVortexCoordinate

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    # Quitting if bad input
    if not len(sys.argv) == 4:
        raise ValueError('Exactly THREE argv argument (==path of the folder containing the runs; Nx, tag) must by inputted. Quitting.')
    if not Path(sys.argv[1]).is_dir():
        raise ValueError('Folder ' + sys.argv[1] + ' does not exist or is not a directory. Quitting.')

    def funct (x, a, b, c):
        return a + b/x + c/x**2

    # Creating output folder if does not exist
    Path(sys.argv[1]+'/subcycles_vortices/').mkdir(parents=True, exist_ok=True)

    # Getting data & plotting them
    data = extractData(sys.argv[1], 'subC', sortingAlgorithm(int(sys.argv[2]), "subC=", "__", ['ball', 'antiDiff']))
    plotVortexCoordinate ( data[['x_avg', 'x_std']].rename(columns={'x_avg': 'avg', 'x_std': 'std'}, inplace=False),
                           sys.argv[1]+'/subcycles_vortices/subcycles_'+sys.argv[3]+'_vortexX.pdf',
                           'Number of Lagrangian subcycles',
                           'Average vortex X position (m)',
                           'upper left',
                           funct,
                           [['$x_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_x=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                            10
                         )
    plotVortexCoordinate ( data[['y_avg', 'y_std']].rename(columns={'y_avg': 'avg', 'y_std': 'std'}, inplace=False),
                           sys.argv[1]+'/subcycles_vortices/subcycles_'+sys.argv[3]+'_vortexY.pdf',
                           'Number of Lagrangian subcycles',
                           'Average vortex Y position (m)',
                           'upper left',
                           funct,
                           [['$x_{\mathrm{vort}}=f(n_x)$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$'],
                            ['$\mathrm{err}_x=|f(n_x)-f(n_x^{\mathrm{last}})|$\n$f(n_x)=$', '', '$/n_x$', '$/n_x^2$']],
                            10
                         )
