/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#include <stdexcept>
#define NOVTM // disable VTM output
//#define BOUNCEBACK // Fall-back bounce-back for the hydrodynamic wall conditions
//#define NOZPLOT // disable plots of planes perpendicular to z axis
#define QUIET // disable BC's verbose warnings
#define FD // enable finite-difference simulation

#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>

#include "0code/param.h"
#include "0code/classes.h"
#include "0code/particleInjection.h"
#include "0code/param.hh"
#include "0code/classes.hh"
#include "0code/particleInjection.hh"


using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;

#define PARTICLE Particle3D
typedef double T;
typedef D3Q27<FORCE,OMEGA,AD_FIELD,NORMAL_X,NORMAL_Y,NORMAL_Z> DESCRIPTOR;

#ifdef FD
using FdParams = meta::list<
  fd::fdParams::Timestep,
  fd::fdParams::AntiDiffusivityTuning,
  fd::fdParams::Diffusivity
>;

using MODEL               = FdAdvectionDiffusionModel<T, fd::AdvectionScheme<3,T,fd::tag::UPWIND>,
                                                         fd::DiffusionScheme<3,T,fd::tag::CENTRAL>>;
using MODEL_NEGATIVE_DIFF = FdAdvectionDiffusionModel<T, fd::AdvectionScheme<3,T,fd::tag::UPWIND>,
                                                         fd::DiffusionScheme<3,T,fd::tag::CENTRAL_WITH_ANTIDIFFUSIVITY>>;
#endif

/* latticeOverlap of geometry has to be larger than the one of the particles,
 particle overlap has to be 3 because of shadow particles and communication
 over boundary for force distribution to next two neighbor in EACH direction!!
 */


//============================================================================================================
//Global data
std::string rawDataDir = "./tmp/rawData";
std::string homeDir;
UnitConverter<T,DESCRIPTOR>* converter;
Param<T,DESCRIPTOR> param;


//============================================================================================================
// Stability check as per in Kruger et al., (2017), DOI: 10.1007/978-3-319-44649-3,
// Section. 7.2.2.2, following Niu et al., (2004), DOI: 10.1007/s10955-004-2264-x.
bool NiuStability()
{
  OstreamManager clout(std::cout, "NiuStability");

  // tau cannot be < .5
  if (converter->getLatticeRelaxationTime() < .5) {
    clout << "WARNING: Niu stability criterion NOT passed." << std::endl << "Present tau: " << converter->getLatticeRelaxationTime() << " tau cannot be lesser than 0.5" << std::endl;
     return false;
  }

  clout << "Grid Reynolds number="
          << converter->getCharLatticeVelocity() / converter->getLatticeViscosity()
          << std::endl;

  // For tau >= .55, we must have uLB <= .4
  if (converter->getLatticeRelaxationTime() >= .55) {
     T uLBmax = .4;
    if (converter->getCharLatticeVelocity() <= .4) {
      clout << "Niu stability criterion passed." << std::endl << "Present tau: " << converter->getLatticeRelaxationTime() << " Maximum latticeU for tau>.55: " << uLBmax << ". Present latticeU: " << converter->getCharLatticeVelocity() << std::endl;
       return true;
     } else {
     clout << "WARNING: Niu stability criterion NOT passed." << std::endl << "Present tau: " << converter->getLatticeRelaxationTime() << " Maximum latticeU for tau>.55: " << uLBmax << ". Present latticeU: " << converter->getCharLatticeVelocity() << std::endl;
     return false;
     }
  }

  // For tau < .55, we must have tau > .5 + .125*uLB
  T uLBmax = 8.*(converter->getLatticeRelaxationTime()-.5);
  if (converter->getLatticeRelaxationTime() > .5 + .125*converter->getCharLatticeVelocity()) {
    clout << "Niu stability criterion passed." << std::endl << "Present tau: " << converter->getLatticeRelaxationTime() << " Maximum latticeU for tau>.55: " << uLBmax << ". Present latticeU: " << converter->getCharLatticeVelocity() << std::endl;
    return true;
  }
  clout << "WARNING: Niu stability criterion NOT passed." << std::endl << "Present tau: " << converter->getLatticeRelaxationTime() << " Maximum latticeU for tau<.55: " << uLBmax << ". Present latticeU: " << converter->getCharLatticeVelocity() << std::endl;
  return false;
}


//============================================================================================================
//Preparing geometry
void prepareGeometry( IndicatorF3D<T>& domain,
                      SuperGeometry<T,3>& superGeometry)
{
  // MATERIAL # -> FLUID              | BIOKINETIC SPECIES
  // ---------------------------------|------------------------
  // Material 0 -> nothing            | nothing
  // Material 1 -> bulk               | bulk
  // Material 2 -> wall boundary      | no-penetration boundary
  // Material 3 -> free-slip boundary | no-penetration boundary
  OstreamManager clout(std::cout, "prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0, 2, domain);
  superGeometry.rename( 2,1,{1,1,1} );

  superGeometry.clean();

  std::vector<T> centre1(3,T());
  std::vector<T> centre2(3,T());
  centre1[2] = param.h - converter->getPhysDeltaX();
  centre2[2] = param.h + converter->getPhysDeltaX();
  IndicatorCylinder3D<T> domainTop( centre1, centre2,
                              param.dExt/2. - param.surfBord*converter->getPhysDeltaX() );

  superGeometry.rename(2, 3, domainTop);

  superGeometry.clean();
  superGeometry.innerClean();

  superGeometry.checkForErrors();
  superGeometry.getStatistics().print();
  clout << "Prepare Geometry ... OK" << std::endl;

  return;
}


//============================================================================================================
// Set up the geometry of the simulation
void prepareLattice ( SuperLattice<T, DESCRIPTOR>& sLattice,
                      IndicatorF3D<T>& domainInternal,
                      SuperGeometry<T,3>& superGeometry )
{
  // MATERIAL # -> FLUID              | BIOKINETIC SPECIES
  // ---------------------------------|------------------------
  // Material 0 -> nothing            | nothing
  // Material 1 -> bulk               | bulk
  // Material 2 -> wall boundary      | no-penetration boundary
  // Material 3 -> free-slip boundary | no-penetration boundary
  OstreamManager clout(std::cout, "prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  const T omega = converter->getLatticeRelaxationFrequency();
  // Material 0
  sLattice.defineDynamics<NoDynamics>(superGeometry.getMaterialIndicator(0));

  // Material 1
  sLattice.defineDynamics<SmagorinskyPowerLawForcedBGKdynamics>(superGeometry.getMaterialIndicator(1));

  // Material 2
#ifndef BOUNCEBACK
  sLattice.defineDynamics<NoDynamics>(superGeometry.getMaterialIndicator(2));
  legacy::setBouzidiZeroVelocityBoundary(sLattice, superGeometry, 2, domainInternal);
#else
  sLattice.defineDynamics<BounceBack>(superGeometry.getMaterialIndicator(2));
#endif

  //Material 3
  sLattice.defineDynamics<NoDynamics>(superGeometry.getMaterialIndicator(3));
  setSlipBoundaryWithDynamics(sLattice, omega, superGeometry, 3); // BUGGED


#ifdef FD
  if (param.antiDiffusionTuning == 0.) {
    //FD, Material 1
    setFdPostProcessor3D<T,DESCRIPTOR,MODEL,FdParams,AD_FIELD>(sLattice, superGeometry, 1);
    //FD, Material 2
    setFdBoundary3D<T,DESCRIPTOR,MODEL,fd::AdNeumannZeroBoundaryScheme<3,T,fd::tag::UPWIND>,FdParams,AD_FIELD> (
      sLattice, superGeometry, 2 );
    //FD, Material 4
    setFdBoundary3D<T,DESCRIPTOR,MODEL,fd::AdNeumannZeroBoundaryScheme<3,T,fd::tag::UPWIND>,FdParams,AD_FIELD> (
      sLattice, superGeometry, 3 );
  }
  else {
    //FD, Material 1
    setFdPostProcessor3D<T,DESCRIPTOR,MODEL_NEGATIVE_DIFF,FdParams,AD_FIELD>(sLattice, superGeometry, 1);
    //FD, Material 2
    setFdBoundary3D<T,DESCRIPTOR,MODEL_NEGATIVE_DIFF,fd::AdNeumannZeroBoundaryScheme<3,T,fd::tag::UPWIND>,FdParams,AD_FIELD> (
      sLattice, superGeometry, 2 );
    //FD, Material 3
    setFdBoundary3D<T,DESCRIPTOR,MODEL_NEGATIVE_DIFF,fd::AdNeumannZeroBoundaryScheme<3,T,fd::tag::UPWIND>,FdParams,AD_FIELD> (
      sLattice, superGeometry, 3 );
  }
#endif

  sLattice.setParameter<descriptors::OMEGA>(converter->getLatticeRelaxationFrequency());
  sLattice.setParameter<powerlaw::M>(param.mLB);
  sLattice.setParameter<powerlaw::N>(param.n);
  sLattice.setParameter<powerlaw::OMEGA_MIN>(1./(param.nuMaxLB*descriptors::invCs2<T,DESCRIPTOR>() + 0.5));
  sLattice.setParameter<powerlaw::OMEGA_MAX>(1./(param.nuMinLB*descriptors::invCs2<T,DESCRIPTOR>() + 0.5));
  sLattice.setParameter<collision::LES::Smagorinsky>(param.smago);

#ifdef FD
  sLattice.setParameter<fd::fdParams::Timestep>(param.iT);
  sLattice.setParameter<fd::fdParams::Diffusivity>(param.diffusivity * converter->getPhysDeltaT() / ( converter->getPhysDeltaX() * converter->getPhysDeltaX()));
  sLattice.setParameter<fd::fdParams::AntiDiffusivityTuning>(param.antiDiffusionTuning);
#endif

  clout << "Prepare Lattice ... OK" << std::endl;
}


//============================================================================================================
//Setting the particle system
void prepareParticleSystem(SuperParticleSystem3D<T, PARTICLE>& spSys,
    ParticleInjectionSystem<T,DESCRIPTOR,PARTICLE>& pInjection,
    SuperGeometry<T,3>& superGeometry)
{
  // in this application has to be larger than diagonal to next 2 neighbors
  spSys.setOverlap(param.particlesOverlap); // for shadow particles

  auto weightForce = std::make_shared < WeightForce3D<T, PARTICLE> > (param.direction, param.g);
  spSys.addForce(weightForce);

  auto buoyancyForce = std::make_shared<BuoyancyForce3D<T,PARTICLE,DESCRIPTOR>> (*converter, param.direction);
  spSys.addForce(buoyancyForce);

  // external force stored in storeForce in particles is transfered to forces
  // to be included into computation, afterwards storeForce fields are cleaned
  auto transferExtF = std::make_shared < TransferExternalForce3D<T, PARTICLE> >();
  spSys.addForce(transferExtF);

  std::set<int> boundMaterial = { 2, 3 };
  auto boundary = std::make_shared < MaterialBoundary3D<T, PARTICLE>  > (superGeometry, boundMaterial);
  spSys.addBoundary(boundary);
  std::vector<T> iVelo(3, T());

  for (long unsigned int i=0; i<param.injT0.size(); i++) {
    std::vector<T> vel { T(), T(), param.charU*param.uInRatio };
    std::vector<T> pos { param.injPos[i][0], param.injPos[i][1], param.injPos[i][2] };
    pInjection.addParticleInjectionUnit( std::make_shared<ParticleInjectionUnit<T,PARTICLE>>(spSys, converter->getPhysDeltaT(),
            param.flowRateValue/param.injT0.size(), param.radius1*param.injScaling[i], param.rhoPnominal, param.rhoL, param.injT0[i], param.injT1[i],
            pos, vel) );
  }
}


//============================================================================================================
//Setting the particle system
void prepareLagrangianCoupling ( ForwardCouplingModel<T,PARTICLE>** forwardCoupling,
                                 BackCouplingModel<T,PARTICLE>** backCoupling,
                                 SuperLattice<T, DESCRIPTOR>& sLattice,
                                 SuperGeometry<T,3>& superGeometry )
{
  OstreamManager clout(std::cout, "prepareLagrangianCoupling");
  std::shared_ptr<DragModel<T,PARTICLE>> drag;
  if (param.dragModel == "Dewsbury") {
    clout << "Selecting Dewsbury drag model." << std::endl;
    drag = std::make_shared<PowerLawDewsburyDragModel<T,DESCRIPTOR,PARTICLE>> (*converter, sLattice);
  }
  else if (param.dragModel == "Morsi") {
    clout << "Selecting Morsi drag model." << std::endl;
    drag = std::make_shared<PowerLawMorsiDragModel<T,DESCRIPTOR,PARTICLE>> (*converter, sLattice);
  }
  else if (param.dragModel == "Sun") {
    clout << "Selecting Sun drag model." << std::endl;
    drag = std::make_shared<PowerLawSunDragModel<T,DESCRIPTOR,PARTICLE>> (*converter, sLattice);
  }
  else {
    throw std::invalid_argument("Invalid drag model.");
  }
  std::shared_ptr<SmoothingFunctional<T,DESCRIPTOR>> smoothingFunctional;

  // forward-coupling
  if (param.forwardCouplingType == "naive") {
    clout << "Selecting naive forward coupling." << std::endl;
    *forwardCoupling = new NaiveForwardCouplingModel<T,DESCRIPTOR,PARTICLE> (*converter, sLattice, superGeometry, drag);
  }
  else if (param.forwardCouplingType == "ladd") {
    clout << "Selecting Ladd forward coupling." << std::endl;
    *forwardCoupling = new LaddForwardCouplingModel<T,DESCRIPTOR,PARTICLE> (*converter, sLattice, superGeometry, drag);
  }
  else if (param.forwardCouplingType == "sungkorn") {
    clout << "Selecting naive non-local forward coupling." << std::endl;
    smoothingFunctional = std::make_shared<DeenSmoothingFunctional<T,DESCRIPTOR>> (param.kernelLength, *converter, sLattice);
    *forwardCoupling = new NaiveNonLocalForwardCouplingModel<T,DESCRIPTOR,PARTICLE> ( *converter,
                          sLattice, superGeometry, drag, smoothingFunctional );
  }
  else if (param.forwardCouplingType == "vanWachem") {
    clout << "Selecting vanWachem forward coupling." << std::endl;
    smoothingFunctional = std::make_shared<vanWachemSmoothingFunctional<T,DESCRIPTOR>> (param.kernelLength, *converter, sLattice, param.radius1, 10);
    *forwardCoupling = new vanWachemForwardCouplingModel<T,DESCRIPTOR,PARTICLE> ( *converter,
                          sLattice, superGeometry, drag, smoothingFunctional, smoothingFunctional->getNvoxelInterpPoints() );
  }
  else if (param.forwardCouplingType == "step") {
    clout << "Selecting step forward coupling." << std::endl;
    smoothingFunctional = std::make_shared<StepSmoothingFunctional<T,DESCRIPTOR>> (param.kernelLength, *converter, sLattice);
    *forwardCoupling = new NaiveNonLocalForwardCouplingModel<T,DESCRIPTOR,PARTICLE> ( *converter,
                          sLattice, superGeometry, drag, smoothingFunctional );
  }
  else {
    throw std::invalid_argument("Invalid forward coupling model.");
  }

  // back-coupling
  if (param.forwardCouplingType == "sungkorn" || param.forwardCouplingType == "step" || param.forwardCouplingType == "vanWachem") {
    clout << "Selecting Sungkorn back coupling." << std::endl;
    *backCoupling = new NonLocalBaseBackCouplingModel<T,DESCRIPTOR,PARTICLE> ( *converter,
                   sLattice, superGeometry, smoothingFunctional, param.latticeOverlap );
  }
  else {
    if (param.backCouplingType == "delta") {
    clout << "Selecting delta back coupling." << std::endl;
      *backCoupling = new CubicDeltaBackCouplingModel<T,DESCRIPTOR,PARTICLE> (*converter, sLattice, superGeometry, param.latticeOverlap);
    }
    else if (param.backCouplingType == "local") {
    clout << "Selecting local back coupling." << std::endl;
      *backCoupling = new LocalBackCouplingModel<T,DESCRIPTOR,PARTICLE> (*converter, sLattice, superGeometry, param.latticeOverlap);
    }
    else {
      throw std::invalid_argument("Invalid back coupling model.");
    }
  }
  if (*forwardCoupling==nullptr) throw std::invalid_argument("forwardCoupling not initialized.");
  if (*backCoupling==nullptr) throw std::invalid_argument("backCoupling not initialized.");
}


//============================================================================================================
//Setting the boundary values at each timestep
void setBoundaryValues(SuperLattice<T, DESCRIPTOR>& sLattice,
                       SuperGeometry<T,3>& superGeometry,
                       IndicatorF3D<T>& domainInternal,
                       SuperParticleSystem3D<T, PARTICLE>& spSys1,
                       ParticleInjectionSystem<T,DESCRIPTOR,PARTICLE>& pInjection,
                       bool& firstIter)
{
  // MATERIAL # -> FLUID              | BIOKINETIC SPECIES
  // ---------------------------------|------------------------
  // Material 0 -> nothing            | nothing
  // Material 1 -> bulk               | bulk
  // Material 2 -> wall boundary      | no-penetration boundary
  // Material 3 -> free-slip boundary | no-penetration boundary
  OstreamManager clout(std::cout, "setBoundaryValues");

  pInjection.process(param.iT);

  // Set initial condition
  if (param.iT == 0) {
    AnalyticalConst<3,T,T> rho(1.);
    AnalyticalConst<3,T,T> phi0(0.);
    AnalyticalConst<3,T,T> phi1(1.);
    AnalyticalConst<3,T,T> u { T(), T(), T() };

    //Initialize all values of distribution functions to their local equilibrium
#ifdef FD
    std::shared_ptr<IndicatorF3D<T>> seed;
    if (param.seedingType == "seed") {
      seed = std::make_shared<IndicatorSeed3D<T>>(sLattice.getCuboidGeometry(), domainInternal, param.seedRelInterval*param.dExt);
    }
    else if (param.seedingType == "ball") {
      seed = std::make_shared<IndicatorSphere3D<T>>( Vector<T,3>(param.ballRelX, T(), param.ballRelZ)*param.dExt, param.ballRelR*param.dExt );
    }
    sLattice.template defineField<AD_FIELD>( superGeometry, 1, phi0 );
    sLattice.template defineField<AD_FIELD>( superGeometry, 2, phi0 );
    sLattice.template defineField<AD_FIELD>( superGeometry, 3, phi0 );
    sLattice.template defineField<AD_FIELD>( superGeometry, *seed.get(), phi1 );
#endif
    sLattice.defineRhoU(superGeometry, 1, rho, u);
    sLattice.defineRhoU(superGeometry, 3, rho, u);
    sLattice.iniEquilibrium(superGeometry, 1, rho, u);

    // Make the lattice ready for simulation
    sLattice.initialize();
  }
}


//============================================================================================================
//Python plot over Y plane
void plotOnPlaneY (int procID, SuperLattice<T, DESCRIPTOR>& sLattice, SuperGeometry<T,3>& superGeometry, T uLegend,
                   T iX0, T iX1, T iterX, T iY, T iZ0, T iZ1, T iterZ)
{
  OstreamManager clout(std::cout, "plotOnPlaneY");

  //Definitions
  std::string coordFilenameX = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.gridFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__x.dat";
  std::string coordFilenameZ = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.gridFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__z.dat";
  std::string velFilenameX   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__x.dat";
  std::string velFilenameY   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__y.dat";
  std::string velFilenameZ   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__z.dat";
  std::string plotFilename   = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.plotFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
  std::string viscoFilename  = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.viscoFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".dat";
  std::string viscoPFilename = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.viscoFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
#ifdef FD
  std::string phiFilename  = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.phiFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".dat";
  std::string phiPFilename = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.phiFilename + "Y_"
                        + std::to_string(iY*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
#endif

  // Building coordinate data files at timestep 0
  if (param.iT == 0) {
    olb_ofstream coordFileX(coordFilenameX.c_str());
    olb_ofstream coordFileZ(coordFilenameZ.c_str());

    for (T iZ=iZ0; iZ<=iZ1; iZ++) {
      for (T iX=iX0; iX<=iX1; iX++) {
        coordFileX << (T)iX*converter->getConversionFactorLength() << " ";
            coordFileZ << (T)iZ*converter->getConversionFactorLength() << " ";
      }
      coordFileX << std::endl;
          coordFileZ << std::endl;
    }
    coordFileX.close();
    coordFileZ.close();

    clout << "Coordinate meshgrid for orthogonal to Y axis at y="
          << (T)iY*converter->getConversionFactorLength() << " plotted." << std::endl;
  }

  //Velocity data files
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity(sLattice, *converter);
  AnalyticalFfromSuperF3D<T,T> intpolateVelocity( velocity, true );
  T point[3]={T(),T(),T()};
    point[1] = (T)iY*converter->getConversionFactorLength();

  olb_ofstream velFileX(velFilenameX.c_str());
  olb_ofstream velFileY(velFilenameY.c_str());
  olb_ofstream velFileZ(velFilenameZ.c_str());

  for (T iZ=iZ0; iZ<=iZ1; iZ++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point[0] = (T)iX*converter->getConversionFactorLength();
        point[2] = (T)iZ*converter->getConversionFactorLength();
      T numerical[3] = {T(),T(),T()};
      intpolateVelocity(numerical,point);
      velFileX << numerical[0] << " ";
          velFileY << numerical[1] << " ";
          velFileZ << numerical[2] << " ";
    }
    velFileX << std::endl;
        velFileY << std::endl;
        velFileZ << std::endl;
  }
  velFileX.close();
  velFileY.close();
  velFileZ.close();

  //Viscosity data files
  SuperLatticePhysViscosity3D<T, DESCRIPTOR> viscosity(sLattice, *converter);
  //SuperLatticePhysViscosity3D<T, DESCRIPTOR> viscosity(sLattice, converter, param.logscale);
  AnalyticalFfromSuperF3D<T,T> intpolateViscosity( viscosity, true );
  T point2[3]={T(),T(),T()};
    point2[1] = (T)iY*converter->getConversionFactorLength();

  olb_ofstream viscoFile(viscoFilename.c_str());

  for (T iZ=iZ0; iZ<=iZ1; iZ++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point2[0] = (T)iX*converter->getConversionFactorLength();
        point2[2] = (T)iZ*converter->getConversionFactorLength();
      T numerical2[1] = {T()};
      intpolateViscosity(numerical2,point2);
      //viscoFile << numerical2[0] << " ";
      viscoFile << util::log10 ( ( numerical2[0] < param.nuMin ? param.nuMin : (
                             numerical2[0] > param.nuMax ? param.nuMax : numerical2[0] )
                           ) / param.physViscosity
                         )
                << " ";
    }
    viscoFile << std::endl;
  }
  viscoFile.close();

#ifdef FD
  //Finite-difference data files
  SuperLatticeExternal3D<T, DESCRIPTOR, AD_FIELD> phi( sLattice, param.iT );
  AnalyticalFfromSuperF3D<T,T> intpolatePhi( phi, true );
  T point3[3]={T(),T(),T()};
    point3[1] = (T)iY*converter->getConversionFactorLength();

  olb_ofstream phiFile(phiFilename.c_str());

  for (T iZ=iZ0; iZ<=iZ1; iZ++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point3[0] = (T)iX*converter->getConversionFactorLength();
        point3[2] = (T)iZ*converter->getConversionFactorLength();
      T numerical3[1] = {T()};
      intpolatePhi(numerical3,point3);
      phiFile << numerical3[0] << " ";
    }
    phiFile << std::endl;
  }
  phiFile.close();
#endif

  // Console message
  clout << "Velocity field for timestep t=" << converter->getPhysTime(param.iT)
        << " and orthogonal to Y axis at y="
        << (T)iY*converter->getConversionFactorLength() << " plotted." << std::endl;

  //Velocity plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLB.py "
                      + plotFilename + " " + coordFilenameX + " " + coordFilenameZ + " "
                      + velFilenameX + " " + velFilenameY   + " " + velFilenameZ   + " "
                      + std::to_string(uLegend)             + " y " + std::to_string(param.charU/param.uRef) + " &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLB.py" << std::endl;
      }
    }
  } // \OUTPUT

  //Viscosity plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBvisco.py "
                      + viscoPFilename + " "
                      + coordFilenameX + " " + coordFilenameZ + " "
                      + viscoFilename + " "
                      + std::to_string(util::log10(param.nuMin/param.physViscosity))
                      + " " + std::to_string(util::log10(param.nuMax/param.physViscosity)) + " &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLBvisco.py" << std::endl;
      }
    }
  } // \OUTPUT

#ifdef FD
  //Finite-difference plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBphi.py "
                      + phiPFilename + " "
                      + coordFilenameX + " " + coordFilenameZ + " "
                      + phiFilename + " 0 0.05 &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLBphi.py" << std::endl;
      }
    }
  } // \OUTPUT
#endif

  // Geometry
  if (param.iT == 0) {
    std::string geoFilename  = singleton::directories().getLogOutDir()
                          + param.inDir  + "/geoY_"
                          + std::to_string(iY*converter->getConversionFactorLength())
                          + "__" + std::to_string(param.iT) + ".dat";
    std::string geoPFilename = singleton::directories().getLogOutDir()
                          + param.outDir + "/geoY_"
                          + std::to_string(iY*converter->getConversionFactorLength())
                          + "__" + std::to_string(param.iT) + ".pdf";

    SuperLatticeGeometry3D<T, DESCRIPTOR> geo(sLattice, superGeometry);
    AnalyticalFfromSuperF3D<T,T> intpolateGeo( geo, true );
    T pointG[3]={T(),T(),T()};
      pointG[1] = (T)iY*converter->getConversionFactorLength();

    olb_ofstream geoFile(geoFilename.c_str());

    for (T iZ=iZ0; iZ<=iZ1; iZ++) {
      for (T iX=iX0; iX<=iX1; iX++) {
        pointG[0] = (T)iX*converter->getConversionFactorLength();
          pointG[2] = (T)iZ*converter->getConversionFactorLength();
        T numericalG[1] = {T()};
        intpolateGeo(numericalG,pointG);
        geoFile << numericalG[0] << " ";
      }
      geoFile << std::endl;
    }
    geoFile.close();

    if (param.test==0) { // OUTPUT
      if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
        const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBphi.py "
                        + geoPFilename + " "
                        + coordFilenameX + " " + coordFilenameZ + " "
                        + geoFilename + " 0 5 &" );
        if ( system(command.c_str()) ) {
          std::cout << "Error at python3: plotLBphi.py" << std::endl;
        }
      }
    } // \OUTPUT
  }
}


//============================================================================================================
//Python plot across Z plane
void plotOnPlaneZ (int procID, SuperLattice<T, DESCRIPTOR>& sLattice, SuperGeometry<T,3>& superGeometry, T uLegend,
                   T iX0, T iX1, T iterX, T iY0, T iY1, T iterY, T iZ)
{
  OstreamManager clout(std::cout, "plotOnPlaneY");

  //Definitions
  std::string coordFilenameX = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.gridFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__x.dat";
  std::string coordFilenameY = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.gridFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__y.dat";
  std::string velFilenameX   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__x.dat";
  std::string velFilenameY   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__y.dat";
  std::string velFilenameZ   = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.velFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + "__z.dat";
  std::string plotFilename   = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.plotFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
  std::string viscoFilename  = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.viscoFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".dat";
  std::string viscoPFilename = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.viscoFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
#ifdef FD
  std::string phiFilename  = singleton::directories().getLogOutDir()
                        + param.inDir  + "/" + param.phiFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".dat";
  std::string phiPFilename = singleton::directories().getLogOutDir()
                        + param.outDir + "/" + param.phiFilename + "Z_"
                        + std::to_string(iZ*converter->getConversionFactorLength())
                        + "__" + std::to_string(param.iT) + ".pdf";
#endif

  // Building coordinate data files at timestep 0
  if (param.iT == 0) {
    olb_ofstream coordFileX(coordFilenameX.c_str());
    olb_ofstream coordFileY(coordFilenameY.c_str());

    for (T iY=iY0; iY<=iY1; iY++) {
      for (T iX=iX0; iX<=iX1; iX++) {
        coordFileX << (T)iX*converter->getConversionFactorLength() << " ";
            coordFileY << (T)iY*converter->getConversionFactorLength() << " ";
      }
      coordFileX << std::endl;
          coordFileY << std::endl;
    }
    coordFileX.close();
    coordFileY.close();

    clout << "Coordinate meshgrid for orthogonal to Z axis at z="
          << (T)iZ*converter->getConversionFactorLength() << " plotted." << std::endl;
  }

  //Velocity data files
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity(sLattice, *converter);
  AnalyticalFfromSuperF3D<T,T> intpolateVelocity( velocity, true );
  T point[3]={T(),T(),T()};
    point[2] = (T)iZ*converter->getConversionFactorLength();

  olb_ofstream velFileX(velFilenameX.c_str());
  olb_ofstream velFileY(velFilenameY.c_str());
  olb_ofstream velFileZ(velFilenameZ.c_str());

  for (T iY=iY0; iY<=iY1; iY++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point[0] = (T)iX*converter->getConversionFactorLength();
        point[1] = (T)iY*converter->getConversionFactorLength();
      T numerical[3] = {T(),T(),T()};
      intpolateVelocity(numerical,point);
      velFileX << numerical[0] << " ";
          velFileY << numerical[1] << " ";
          velFileZ << numerical[2] << " ";
    }
    velFileX << std::endl;
        velFileY << std::endl;
        velFileZ << std::endl;
  }
  velFileX.close();
  velFileY.close();
  velFileZ.close();

  //Viscosity data files
  SuperLatticePhysViscosity3D<T, DESCRIPTOR> viscosity(sLattice, *converter);
  //SuperLatticePhysViscosity3D<T, DESCRIPTOR> viscosity(sLattice, converter, param.logscale);
  AnalyticalFfromSuperF3D<T,T> intpolateViscosity( viscosity, true );
  T point2[3]={T(),T(),T()};
    point2[2] = (T)iZ*converter->getConversionFactorLength();

  olb_ofstream viscoFile(viscoFilename.c_str());

  for (T iY=iY0; iY<=iY1; iY++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point2[0] = (T)iX*converter->getConversionFactorLength();
        point2[1] = (T)iY*converter->getConversionFactorLength();
      T numerical2[1] = {T()};
      intpolateViscosity(numerical2,point2);
      //viscoFile << numerical2[0] << " ";
      viscoFile << util::log10 ( ( numerical2[0] < param.nuMin ? param.nuMin : (
                             numerical2[0] > param.nuMax ? param.nuMax : numerical2[0] )
                           ) / param.physViscosity
                         )
                << " ";
    }
    viscoFile << std::endl;
  }
  viscoFile.close();

#ifdef FD
  //Finite-difference data files
  SuperLatticeExternal3D<T, DESCRIPTOR, AD_FIELD> phi( sLattice, param.iT );
  AnalyticalFfromSuperF3D<T,T> intpolatePhi( phi, true );
  T point3[3]={T(),T(),T()};
    point3[2] = (T)iZ*converter->getConversionFactorLength();

  olb_ofstream phiFile(phiFilename.c_str());

  for (T iY=iY0; iY<=iY1; iY++) {
    for (T iX=iX0; iX<=iX1; iX++) {
      point3[0] = (T)iX*converter->getConversionFactorLength();
        point3[1] = (T)iY*converter->getConversionFactorLength();
      T numerical3[1] = {T()};
      intpolatePhi(numerical3,point3);
      phiFile << numerical3[0] << " ";
    }
    phiFile << std::endl;
  }
  phiFile.close();
#endif

  // Console message
  clout << "Velocity field for timestep t=" << converter->getPhysTime(param.iT)
        << " and orthogonal to Z axis at z="
        << (T)iZ*converter->getConversionFactorLength() << " plotted." << std::endl;

  //Velocity plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLB.py "
                      + plotFilename + " " + coordFilenameX + " " + coordFilenameY + " "
                      + velFilenameX + " " + velFilenameY   + " " + velFilenameZ   + " "
                      + std::to_string(uLegend)             + " z " + std::to_string(param.charU/param.uRef) + " &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLB.py" << std::endl;
      }
    }
  } // \OUTPUT

  //Viscosity plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBvisco.py "
                      + viscoPFilename + " "
                      + coordFilenameX + " " + coordFilenameY + " "
                      + viscoFilename + " "
                      + std::to_string(util::log10(param.nuMin/param.physViscosity))
                      + " " + std::to_string(util::log10(param.nuMax/param.physViscosity)) + " &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLBvisco.py" << std::endl;
      }
    }
  } // \OUTPUT

#ifdef FD
  //Finite-difference plots
  if (param.test==0) { // OUTPUT
    if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
      const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBphi.py "
                      + phiPFilename + " "
                      + coordFilenameX + " " + coordFilenameY + " "
                      + phiFilename + " 0 0.05 &" );
      if ( system(command.c_str()) ) {
        std::cout << "Error at python3: plotLBphi.py" << std::endl;
      }
    }
  } // \OUTPUT
#endif

  // Geometry
  if (param.iT == 0) {
    std::string geoFilename  = singleton::directories().getLogOutDir()
                          + param.inDir  + "/geoZ_"
                          + std::to_string(iZ*converter->getConversionFactorLength())
                          + "__" + std::to_string(param.iT) + ".dat";
    std::string geoPFilename = singleton::directories().getLogOutDir()
                          + param.outDir + "/geoZ_"
                          + std::to_string(iZ*converter->getConversionFactorLength())
                          + "__" + std::to_string(param.iT) + ".pdf";

    SuperLatticeGeometry3D<T, DESCRIPTOR> geo(sLattice, superGeometry);
    AnalyticalFfromSuperF3D<T,T> intpolateGeo( geo, true );
    T pointG[3]={T(),T(),T()};
      pointG[2] = (T)iZ*converter->getConversionFactorLength();

    olb_ofstream geoFile(geoFilename.c_str());

    for (T iY=iY0; iY<=iY1; iY++) {
      for (T iX=iX0; iX<=iX1; iX++) {
        pointG[0] = (T)iX*converter->getConversionFactorLength();
          pointG[1] = (T)iY*converter->getConversionFactorLength();
        T numericalG[1] = {T()};
        intpolateGeo(numericalG,pointG);
        geoFile << numericalG[0] << " ";
      }
      geoFile << std::endl;
    }
    geoFile.close();

    if (param.test==0) { // OUTPUT
      if ( singleton::mpi().getRank() == procID%singleton::mpi().getSize() ) {
        const std::string command ( "python3 " + homeDir + "/pyResources/plotsLBphi.py "
                        + geoPFilename + " "
                        + coordFilenameX + " " + coordFilenameY + " "
                        + geoFilename + " 0 5 &" );
        if ( system(command.c_str()) ) {
          std::cout << "Error at python3: plotLBphi.py" << std::endl;
        }
      }
    } // \OUTPUT
  }
}


//============================================================================================================
//Printing results & plotting data
void getResults(SuperLattice<T, DESCRIPTOR>& sLattice,
                SuperGeometry<T,3>& superGeometry,
                IndicatorF3D<T>& domainInternal,
                SuperParticleSystem3D<T, PARTICLE>& spSys1,
                Timer<double>& timer,
                          bool& firstIter, bool hasConverged=false, bool hasDiverged=false  )
{
  // MATERIAL # -> FLUID              | BIOKINETIC SPECIES
  // ---------------------------------|------------------------
  // Material 0 -> nothing            | nothing
  // Material 1 -> bulk               | bulk
  // Material 2 -> wall boundary      | no-penetration boundary
  // Material 3 -> free-slip boundary | no-penetration boundary
  OstreamManager clout(std::cout, "getResults");

  static Gnuplot<T> gplot( "uCentre" );

  if (hasDiverged) {
    spSys1.printDeep("spSys1 ");
  }

  /// Functionals definition:
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity(sLattice, *converter);
  SuperLatticePhysPressure3D<T, DESCRIPTOR> pressure(sLattice, *converter);
  SuperLatticePhysViscosity3D<T, DESCRIPTOR> viscosity(sLattice, *converter, param.logscale);
  SuperLatticePhysShearRateMag3D<T, DESCRIPTOR> shearRateMag(sLattice, *converter);
#ifdef FD
  SuperLatticeExternal3D<T, DESCRIPTOR, AD_FIELD> phi( sLattice, param.iT );
#endif
  SuperLatticeGeometry3D<T, DESCRIPTOR> geometry(sLattice, superGeometry);

  /// Console & gnuplot:
  if (param.iT % converter->getLatticeTime(param.Tstatus) == 0) {
     clout << std::endl;
    timer.update(param.iT);
    timer.printStep();
    sLattice.getStatistics().print(param.iT, converter->getPhysTime(param.iT));
    clout << "Global number of active particles: " << spSys1.globalNumOfActiveParticles() << std::endl;
    clout << "Global number of all particles: " << spSys1.globalNumOfParticles() << std::endl;
    SuperLatticeYplus3D<T, DESCRIPTOR> yPlus( sLattice, *converter, superGeometry, domainInternal, 2 );
    SuperMax3D<T>     yPlusMaxF( yPlus, superGeometry, 1 );
    SuperAverage3D<T> yPlusAvgF( yPlus, superGeometry, 1 );
    int input[4]= {};
    T yPlusMax[1];
    T yPlusAvg[1];
    yPlusMaxF( yPlusMax,input );
    yPlusAvgF( yPlusAvg,input );
    clout << "y+:  maximum=" << yPlusMax[0] << "   average=" << yPlusAvg[0] << std::endl;

    if (param.test==0) { // OUTPUT
      AnalyticalConst<3,T,T> zero(0.);
#ifdef FD
      int input_volume[3];
      T output_volume[1];
      auto indicatorF = superGeometry.getMaterialIndicator(1);
      SuperIntegral3D<T> volume(geometry, superGeometry, 1);
      SuperAbsoluteErrorL1Norm3D<T> phiAvg(phi, zero, indicatorF);
      int tmp[] {};
      T phi0Val[1];
      phiAvg(phi0Val, tmp);
      volume(output_volume, input_volume);
      phi0Val[0] /= output_volume[0];
      AnalyticalConst<3,T,T> phi0Field(phi0Val[0]);
      SuperAbsoluteErrorL1Norm3D<T> phiDistFromAvg(phi, phi0Field, indicatorF);
      T UI[1] {};
      phiDistFromAvg(UI, tmp);
      UI[0] /= 2. * phi0Val[0] * output_volume[0];
      clout << "Uniformity Index: UI=" << UI[0] << std::endl;
#endif
    } // \OUTPUT
  }

#ifndef NOVTM
  /// VTM
  if (param.test==0) { // OUTPUT
    SuperVTMwriter3D<T> vtkWriter("bubblePlume");
    vtkWriter.addFunctor(velocity);
    vtkWriter.addFunctor(pressure);
    vtkWriter.addFunctor(viscosity);
#ifdef FD
    vtkWriter.addFunctor(phi);
#endif
    vtkWriter.addFunctor(shearRateMag);

    if (firstIter) {
      SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid(sLattice);
      SuperLatticeRank3D<T, DESCRIPTOR> rank(sLattice);
      vtkWriter.write(geometry);
      vtkWriter.write(cuboid);
      vtkWriter.write(rank);
      vtkWriter.createMasterFile();
    }

    if (param.iT % converter->getLatticeTime(param.Tvtm) == 0 || hasConverged || hasDiverged) {
      vtkWriter.write(param.iT);
    }
  } // \OUTPUT
#endif

  /// ImageData & Python3
  if ( (param.test==0 && param.iT % converter->getLatticeTime(param.Tvtm) == 0) || hasConverged || hasDiverged ) { // OUTPUT

    /*
    // <--------------------------------------
    // write output of velocity as JPEG
    SuperEuklidNorm3D<T, DESCRIPTOR> normVel(velocity);
    Vector<T,3> centre = {T(), T(), T()};
    Vector<T,3> u = {(T)1., T(), T()};
    Vector<T,3> v = {T(), T(), (T)1.};
    BlockReduction3D2D<T> planeReductionVel( normVel, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
    BlockReduction3D2D<T> planeReductionPr( pressure, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
    BlockReduction3D2D<T> planeReductionVisco( viscosity, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
#ifdef FD
    BlockReduction3D2D<T> planeReductionPhi( phi, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
#endif
    BlockReduction3D2D<T> planeReductionShearRateMag( shearRateMag, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
    BlockReduction3D2D<T> planeReductionGeometry( geometry, centre, u, v,
                          200, BlockDataSyncMode::ReduceOnly );
    heatmap::write(planeReductionVel, param.iT);
    heatmap::write(planeReductionPr, param.iT);
    heatmap::write(planeReductionVisco, param.iT);
#ifdef FD
    heatmap::write(planeReductionPhi, param.iT);
#endif
    heatmap::write(planeReductionShearRateMag, param.iT);
    heatmap::write(planeReductionGeometry, param.iT);
    // <--------------------------------------
    */

    // python3
    plotOnPlaneY( 1, sLattice, superGeometry, param.uLegend0,
                          -(T).5*param.dExt/converter->getConversionFactorLength(),
               (T).5*param.dExt/converter->getConversionFactorLength(),
               1.,
                           T(),
               -param.h0/converter->getConversionFactorLength(),
               param.h/converter->getConversionFactorLength()+1,
               1.
               );
    if (param.geoType == "lab") {
      plotOnPlaneY( 0, sLattice, superGeometry, param.uLegendOffs,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                            (T)param.yOffset/converter->getConversionFactorLength(),
                            T(),
                param.h/converter->getConversionFactorLength()+1,
                1.
                );
    }
#ifndef NOZPLOT
      /*
      plotOnPlaneZ( 2, sLattice, superGeometry, param, param.uLegendOffs,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                0.02/converter->getConversionFactorLength()
                );
      plotOnPlaneZ( 3, sLattice, superGeometry, param, param.uLegendOffs,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                0.10/converter->getConversionFactorLength()
                );
      */
      plotOnPlaneZ( 3, sLattice, superGeometry, param.uLegendOffs,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                            -(T).5*param.dExt/converter->getConversionFactorLength(),
                (T).5*param.dExt/converter->getConversionFactorLength(),
                1.,
                0.05/converter->getConversionFactorLength()
                );
#endif
  } // \OUTPUT

  /// Save to file
  if (param.test==0 && param.iT % converter->getLatticeTime(param.Tsave) == 0 && param.iT != 0) { //OUTPUT
    clout << "Saving for time="  << converter->getPhysTime(param.iT) << std::endl;
    sLattice.save( "validation.checkpoint" );

     olb_ofstream lastSaveFile(param.lastSave.c_str());
     lastSaveFile << converter->getPhysTime(param.iT);
     lastSaveFile.close();
     clout << "Timestep " << converter->getPhysTime(param.iT) << " saved." << std::endl;
  } // \OUTPUT

  /// change firstIter if necessary
  if (firstIter) {
    firstIter = false;
  }
}


//============================================================================================================
//Creating a directory
void createDirectory(std::string path)
{
  OstreamManager clout(std::cout,"createDirectory");
  int rank = 0;
#ifdef PARALLEL_MODE_MPI
  rank = singleton::mpi().getRank();
#endif
  if (rank == 0) {
  struct stat statbuf;
    if (stat(path.c_str(), &statbuf) != 0) {
#ifdef _WIN32
       mkdir(path.c_str());
 #else  /* Unix */
       if (mkdir(path.c_str(), 0775) == 0) {
          clout << "Directory " << path << " created." << std::endl;
       }
       //else
       //  clout << "Directory " << path << " failed." << std::endl;
#endif
     }
  }
}


//============================================================================================================
int main(int argc, char* argv[])
{
  /// === 1st Step: Initialization ===
  olbInit(&argc, &argv);
  if (argc == 1) {
    homeDir = "zzz_test";
  }
  else if ( ! ( (argc == 2) && (std::string(argv[1]) != "-h") && (std::string(argv[1]) != "-H") && (std::string(argv[1]) != "--help") ) ) {
    if (singleton::mpi().isMainProcessor())
      std::cout << std::endl
                << "Usage:" << std::endl << "mpirun -np <nProcs> diffusionPipe <xlmFileName>" << std::endl
                << std::endl
                << std::endl;
    singleton::exit(0);
  }
  else {
    homeDir = argv[1];
  }
  OstreamManager clout(std::cout, "main");
  clout << "Home folder: " << homeDir << std::endl;
  singleton::directories().setOutputDir(homeDir + "/tmp/");

  converter = new UnitConverter<T,DESCRIPTOR>(param.initParamAndCreateConverter(homeDir + "/input.xml"));
  createDirectory((singleton::directories().getLogOutDir() + param.inDir ).c_str());
  createDirectory((singleton::directories().getLogOutDir() + param.outDir).c_str());


  /// If in testing, the maximum times are modified accordingly
  if (param.test > 0) {
    param.Tmax = converter->getPhysTime(1);
  }

  converter->print();
  NiuStability();
  param.print();

  /// === 2rd Step: Prepare Geometry ===
  /// Instantiation of a cuboidGeometry with weights
  auto domain = std::make_shared<IndicatorDomain3D<T> >(param.dExt, param.dInt,
                  param.h, param.h0);
  auto domainInternal = std::make_shared<IndicatorInternal3D<T> >(*domain.get(), converter->getConversionFactorLength());

  /// Instantiation of a cuboidGeometry with weights
#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = (int)( param.cuboidsMult*singleton::mpi().getSize() );
#else
  const int noOfCuboids = 7;
#endif
  CuboidGeometry3D<T> cuboidGeometry( domain, converter->getConversionFactorLength(), noOfCuboids );

  cuboidGeometry.print();

  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);
  SuperGeometry<T,3> superGeometry(cuboidGeometry,
                                   loadBalancer, param.latticeOverlap); // default overlap=2
  //SuperGeometry<T,3> superGeometry(cuboidGeometry, loadBalancer, 1); // overlap=1 (crashes!!)
  prepareGeometry(*domain.get(), superGeometry);

  /// === 3rd Step: Prepare Lattice ===
  SuperLattice<T, DESCRIPTOR> sLattice(superGeometry);

  prepareLattice(sLattice, *domainInternal.get(), superGeometry);
  clout << "Lattice prepared." << std::endl;

  /// === 4th Step: Prepare particle dynamics ===
  SuperParticleSystem3D<T, PARTICLE> spSys1(cuboidGeometry, loadBalancer, superGeometry);
  ParticleInjectionSystem<T,DESCRIPTOR,PARTICLE> pInjection(spSys1, *converter);
  prepareParticleSystem(spSys1, pInjection, superGeometry);

  // Step: coupling
  ForwardCouplingModel<T,PARTICLE>* forwardCoupling = nullptr;
  BackCouplingModel<T,PARTICLE>* backCoupling = nullptr;
  prepareLagrangianCoupling(&forwardCoupling, &backCoupling, sLattice, superGeometry);

  // Communicator
  auto& commFields = sLattice.getCommunicator(stage::PostPostProcess());
  commFields.requestField<OMEGA>();
#ifdef FD
  commFields.requestField<AD_FIELD>();
#endif
  commFields.requestOverlap(param.latticeOverlap);
  commFields.exchangeRequests();

  /// === 5th Step: Main Loop with Timer ===
  Timer<double> timer(converter->getLatticeTime(param.Tmax), superGeometry.getStatistics().getNvoxel());
  timer.start();

  if (param.Tinit > 0) {
    sLattice.load("validation.checkpoint");
    clout << "Lattice checkpoint loaded." << std::endl;

    clout << "Lattice checkpoint loaded." << std::endl;
  }

  bool hasConverged = false;
  bool hasDiverged = false;
  bool firstIter = true;
  for ( param.iT = converter->getLatticeTime(param.Tinit);
        param.iT < converter->getLatticeTime(param.Tmax)+10;
        ++param.iT ) {
    sLattice.setParameter<fd::fdParams::Timestep>(param.iT);

    /// === 6th Step: Definition of Initial and Boundary Conditions ===
    setBoundaryValues(sLattice, superGeometry, *domainInternal.get(), spSys1, pInjection, firstIter);

    /// === 7th Step: Computation and Output of the Results ===
    getResults(sLattice, superGeometry, *domainInternal.get(), spSys1, timer, firstIter, hasConverged, hasDiverged);

    /// === 8th Step: Collide and Stream Execution ===
    sLattice.collideAndStream();
    backCoupling->resetExternalField(1);
    spSys1.simulateWithTwoWayCoupling_Davide(converter->getConversionFactorTime(), *forwardCoupling, *backCoupling, 1, param.subCycles);

      // Stopping if simulation diverging (checking on averag energy value).
      // NaN is not equal to any other number, including itself.
      if ( sLattice.getStatistics().getAverageEnergy() != sLattice.getStatistics().getAverageEnergy() || sLattice.getStatistics().getMaxU() > 1. ) {
      olb_ofstream divFile("diverged");
      divFile.close();
      hasDiverged = true;
      getResults(sLattice, superGeometry, *domainInternal.get(), spSys1, timer, firstIter, hasConverged, hasDiverged);
      clout << "SIMULATION DIVERGED AT TIMESTEP iT= " << param.iT << ", CORRESPONDING AT TIME= " << converter->getPhysTime(param.iT) << std::endl;
          break;
    }
  }

  timer.stop();
  timer.printSummary();

  if (param.test==0) { // OUTPUT
    if (!hasDiverged) {
      std::string minTimestepFile =
          singleton::directories().getLogOutDir() + param.inDir  + "/0MinTimestep.txt";
      olb_ofstream minTimestep(minTimestepFile.c_str());
      minTimestep << converter->getLatticeTime(param.TavgPlot);
      minTimestep.close();

          int iY = (T)param.yOffset/converter->getConversionFactorLength();

      std::string inputDir       = singleton::directories().getLogOutDir() + param.inDir;
      std::string coordFilenameX = inputDir + "/" + param.gridFilename + "Y_"
                            + std::to_string( iY*converter->getConversionFactorLength() )
                            + "__x.dat";
      std::string coordFilenameZ = inputDir + "/" + param.gridFilename + "Y_"
                            + std::to_string( iY*converter->getConversionFactorLength() )
                            + "__z.dat";
      std::string velFilenameX   = inputDir + "/0x.txt";
      std::string velFilenameZ   = inputDir + "/0z.txt";
      std::string velPath        = param.velFilename + "Y"
                            + std::to_string( iY*converter->getConversionFactorLength() );
      std::string plotFilename   = singleton::directories().getLogOutDir() + "/"
                            + param.outDir + ".pdf";

      if ( singleton::mpi().getRank() == 0%singleton::mpi().getSize() ) {
        const std::string command1 ( "python3 " + homeDir + "/pyResources/averageLB.py "
            + inputDir + " " + velPath + " x " + velFilenameX );
        if ( system(command1.c_str()) ) {
          std::cout << "Error at python3" << std::endl;
        }
      }
      if ( singleton::mpi().getRank() == 0%singleton::mpi().getSize() ) {
        const std::string command1 ( "python3 " + homeDir + "/pyResources/averageLB.py "
            + inputDir + " " + velPath + " z " + velFilenameZ );
        if ( system(command1.c_str()) ) {
          std::cout << "Error at python3" << std::endl;
        }
      }

      if ( singleton::mpi().getRank() == 0%singleton::mpi().getSize() ) {
        const std::string command ( "python3 " + homeDir + "/pyResources/plotsLB.py "
                        + plotFilename + " " + coordFilenameX + " " + coordFilenameZ + " "
                        + velFilenameX + " " + velFilenameZ   + " " + velFilenameZ   + " "
                        + std::to_string(param.uLegendOffs)   + " y &" );
        if ( system(command.c_str()) ) {
          std::cout << "Error at python3" << std::endl;
        }
      }

      std::string allDoneName = homeDir + "/allDone";
      olb_ofstream allDoneFile(allDoneName.c_str());
      allDoneFile.close();
      clout << "tmp/allDone file created." << std::endl;
    }
  } // \OUTPUT
}
