/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef CLASSES_HH
#define CLASSES_HH

#include "classes.h"

namespace olb {


////////////////////////  Class IndicatorDomain3D  ////////////////////////////////////////////

template <typename T>
IndicatorDomain3D<T>::IndicatorDomain3D(T dExt, T dInt, T h, T h0)
  : _dExt(dExt), _dInt(dInt), _h(h), _h0(h0)
{
  std::vector<T> provv(3,T());
  provv[0] = -0.5*_dExt;
  provv[1] = -0.5*_dExt;
  provv[2] = -_h0;
  this->_myMin = provv;

  provv[0] = 0.5*_dExt;
  provv[1] = 0.5*_dExt;
  provv[2] = _h;
  this->_myMax = provv;
}


template <typename T>
bool IndicatorDomain3D<T>::operator()(bool output[], const T input[])
{
  //Precondition: the point stays within the whole vertical span.
  output[0] = (input[2] >= -_h0) && (input[2] <= _h);
  if (!output[0]) return true;

  // Top cylinder.
  if (input[2] >= T()) {
    output[0] = input[0]*input[0] + input[1]*input[1] <= 0.25*_dExt*_dExt;
    return true;
  }

  // Bottom reverse frustum.
  output[0] = (4.0*_h0*_h0*(input[0]*input[0] + input[1]*input[1]) <= util::pow(_h0*_dInt + (input[2] + _h0)*(_dExt - _dInt), 2));
  return true;
}


////////////////////////  Class IndicatorSeed3D  ////////////////////////////////////////////

template <typename T>
IndicatorSeed3D<T>::IndicatorSeed3D(CuboidGeometry3D<T>& cuboidGeometry, IndicatorF3D<T>& domainInternal, T interval)
  : _cuboidGeometry(cuboidGeometry), _domainInternal(domainInternal), _interval(interval)
{ }


template <typename T>
bool IndicatorSeed3D<T>::operator()(bool output[], const T input[])
{
  // get the nearest seed point
  T nearestSeedPoint[] { std::round(input[0]/_interval)*_interval,
                        std::round(input[1]/_interval)*_interval,
                        std::round(input[2]/_interval)*_interval };

  // check that the input point falls inside the domain's bulk
  bool isInternal[] {false};
  _domainInternal(isInternal, input);
  if (! isInternal[0] ) {
    output[0] = false;
    return true;
  }

  // check that input and nearest seed point fall inside the same cell
  int approxInput[4];
  int approxNearestSeedPoint[4];
  if ( _cuboidGeometry.getLatticeR(approxInput, input) &&
       _cuboidGeometry.getLatticeR(approxNearestSeedPoint, nearestSeedPoint) ) {
    output[0] = (approxInput[0] == approxNearestSeedPoint[0]) &&
                (approxInput[1] == approxNearestSeedPoint[1]) &&
                (approxInput[2] == approxNearestSeedPoint[2]) &&
                (approxInput[3] == approxNearestSeedPoint[3]);
    return true;
  }

  output[0] = false;
  return true;
}


//////////////////////////////////////// class PointValueTracer ////////////////////////////////

template <typename T, typename DESCRIPTOR>
PointValueTracer<T,DESCRIPTOR>::PointValueTracer ( SuperLattice<T, DESCRIPTOR>& sLattice,
    UnitConverter<T,DESCRIPTOR>& converter,
    int deltaT, T epsilon,
    T point[3] )
  : util::ValueTracer<T>(deltaT, epsilon),
    _point { point[0], point[1], point[2] },
    _sLattice(sLattice),
    _converter(converter),
    _velocity( std::make_shared<SuperLatticePhysVelocity3D<T,DESCRIPTOR>>(_sLattice, _converter) ),
    _intpolateVelocity ( std::make_shared<AnalyticalFfromSuperF3D<T>> (*_velocity.get(), true) )
{}

template <typename T, typename DESCRIPTOR>
void PointValueTracer<T,DESCRIPTOR>::takeValue(bool doPrint)
{
  T value[3];
  _intpolateVelocity.get()->operator()(value, _point);
  util::ValueTracer<T>::takeValue( util::sqrt(value[0]*value[0] + value[1]*value[1] + value[2]*value[2]), doPrint);
}

} // namespace olb

#endif
