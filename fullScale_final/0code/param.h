/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARAM_H
#define PARAM_H
namespace olb {

// Class containing all the run's data
template <typename T, typename DESCRIPTOR>
class Param {
private:
  void buildVectorParam (std::vector<T>& par, std::string content);
  void adjustInjZ(std::vector<T>& R); // Adjust injector's z coordinate
public:
  /// === Method prototypes ===
  UnitConverter<T,DESCRIPTOR> initParamAndCreateConverter(std::string fName);
  void print();

  /// === Back-coupling ===
  // Back-coupling type.
  // 'delta' -> as per Maier et al. (2017)
  // 'local' -> only to the cell occupied by the particle
    // (if forwardCouplingType=='sungkorn' or 'step', this choice is disabled!)
  std::string backCouplingType = "";

  // Forward-coupling type.
  // 'naive' -> naive coupling
  // 'ladd'  -> Ladd coupling
  // 'sungkorn'  -> coupling as in Sungkorn et al. (2011)
    // 'step'  -> stepwise, non-local coupling
  // 'vanWachem' -> as in Evrard, Denner and van Wachem (2019)
  std::string forwardCouplingType = "";

  // Drag model.
  // 'Dewsbury' -> Dewsbury drag model
  // 'Morsi'    -> Morsi drag model
  // 'Sun'      -> Sun drag model
  std::string dragModel = "";

  T kernelLength = T(); // Back-coupling kernel length.

  /// === simul ===
  size_t iT = 0; // Timestep iterator
  int cmc = -1; // CMC concentration index
  int flowRate = -1; // Flow rate index
  std::string gridFilename = ""; // Grid filename for Python elaborations
  std::string inDir = ""; // Dirname for Python input dir (where text files are written)
  std::string geoType = ""; // exp -> experimental setup; pilot -> pilot setup
  std::string lastSave = ""; // Where to write the last saved timestep
  std::string outDir = ""; // Dirname for Python outpot dir (where plots are produced)
  std::string velFilename = ""; // Velocity filename for Python elaborations
  std::string plotFilename = ""; // Plot filename for Python velocity elaborations
  std::string phiFilename = ""; // Plot filename for Python fnite-difference elaborations
  std::string viscoFilename = ""; // Plot filename for Python viscosity elaborations
  int test = 0; // 0: no testing; >0: after an initialization collide-and-stream,
                // test the first TEST timesteps with suppressed non-console output.

  /// === setup ===
  bool logscale = false; // plot the viscosity in logarighmic scale?
  int cuboidsMult = 1; // No. of cuboids = cuboidsMult * mpi size
  T latticeU = T(); // Velocity in lattice units (to be modified from input.xml)
  int Nx = -1; // Number of lattice spaces through X direction: [179 200 225 251 282]
  T smago = T(); // Smagorinsky constant (to be modified from input.xml)
  int subCycles = 1; // Number of Lagrangian fractionary timesteps
  int latticeOverlap = 4;
  T particlesOverlap = 3.;
  T antiDiffusionTuning = T(); // =0: no anti-diffusion correction; =1: full anti-diffusion correction

  /// === time ===
  T Tinit = T(); // Initial time
  T Tmax = T(); // Maximum simulation time of preliminary run
  T Tsave = T(); // Frequency at which the simulation is written on ASCII file
  T Tstatus = T(); // Frequency at which the simulation is written on display
  T Tvtm = T(); // Frequency at which the simulation is written on vtm file
  T TavgPlot = T(); // Time at which the velocity field starts to be averaged for the averaged plot.

  /// === data - dynamics ===
  T charU = T(); // Average rising bubble velocity in m/s
  T uLegend0 = T(); // Legend maximum velocity for central plotting.
  T uLegendOffs = T(); // Legend maximum velocity for offset plotting.
  T uExp = T(); // Experimental rising bubble velocity in m/s.
                          // cmc=0, frate=0 -> uExp = 0.41635
                          // cmc=0, frate=1 -> uExp = 0.47782
                          // cmc=0, frate=2 -> uExp = 0.49860
                          // cmc=1, frate=0 -> uExp = 0.27232
                          // cmc=1, frate=1 -> uExp = 0.36388
                          // cmc=1, frate=2 -> uExp = 0.39337
                          // cmc=2, frate=0 -> uExp = 0.06813
                          // cmc=2, frate=1 -> uExp = 0.19380
                          // cmc=2, frate=2 -> uExp = 0.31705
  T uRef = T(); // Reference (unscaled) value for charU in m/s.

  // Power-law rheology
  T m = T(); // Consistency coefficient
  T mLB = T(); // Consistency coefficient, dimensionless units
  T n = T(); // Power-law exponent
  T nuMin = T(); // Minimum viscosity
  T nuMax = T(); // Maximum viscosity
  T nuMinLB = T(); // Minimum viscosity, dimensionless units
  T nuMaxLB = T(); // Maximum viscosity, dimensionless units
  T physViscosity = T();

  /// === data - Biokinetics ===
  T diffusivity = T(); // Diffusivity in m^2/s

  // Bubble dimensions
  T d = T(); // Bubble diameter (in mm!): d[cmc][flowRate]. It will inform radius.
  T g = T();  // Nominal acceleration of gravity
  T radius1 = T(); // Bubble radius in m in preliminary run
  T rhoL = T(); // Sludge density
  T rhoPnominal = T(); // Nominal bubble density
  T Pmax = 1.0; // Maximum value of numerical porosity for DLBM. To be tuned to stabilize.
  T uInRatio = T(); // Bubble inlet velocity (ratio of charU)
  T minRange = T(); // Minimum relative quota for central colum z-component velocity averaging

  /// === data - geometry ===
  int surfBord = 0; // Number of lattice spaces setted as bounce-back in place
                    // of partial-slip at the border of the surface

  T dExt = T(); // External diameter
  T dInt = T(); // Diameter at the bottom of the frustum
  T h    = T(); // Cylinder height
  T h0   = T(); // Frustum height

  T yOffset = T(); // offset of vertical Y plane from centre in PIV measurements
  std::vector<T> direction = { T(), T(), -1. };

  //Injectors
  T flowRateValue = T(); // Injected biogas flow rate in m^3/s
  T injEquivD = T(); // Injector's equivalent diameter (m)
  std::vector<std::vector<T>> injPos {}; // Set of injector positions (m).
  std::vector<T> injScaling {}; // Scaling value for bubble diameter.
  std::vector<T> injT0 {}; // Set of injector start times (s).
  std::vector<T> injT1 {}; // Set of injector stop times (s).

  /// === Post-processing ===
  T ballRelR = T(); // Tracer ball's radius, in multiples of tank diameter
  T ballRelX = T(); // Tracer ball's radius x coordinate, in multiples of tank diameter
  T ballRelZ = T(); // Tracer ball's radius z coordinate, in multiples of tank diameter
  T seedRelInterval = T(); //  Displacements of seed tracer from origin, in multiples of tank diameter
  std::string seedingType = ""; // "seed" -> small seed uniformly distributed
                                // "ball" -> single ball near the inlet
};

} // namespace olb

#endif
