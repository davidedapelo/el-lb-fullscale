/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef CLASSES_H
#define CLASSES_H


#include "functors/analytical/indicator/indicatorBaseF3D.h"
namespace olb {

////////////////////////  Class IndicatorDomain3D  ////////////////////////////////////////////
/// Indicator function for the computational domain (cylindrical tank with sloped bottom,
/// or, geometrically, a cylinder over a reverted frustrum)
template <typename T>
class IndicatorDomain3D : public IndicatorF3D<T> {
private:
T _dExt; // cylinder and frustrum's larger diameter
T _dInt; // frustrum's smaller diameter
T _h;    // cylinder's height
T _h0;   // frustrum's height
public:
IndicatorDomain3D(T dExt, T dInt, T h, T h0);
bool operator() (bool output[], const T input[]) override;
};


////////////////////////  Class IndicatorSeed3D  ////////////////////////////////////////////
/// Indicator function to initialize seed tracer at regular intervals
template <typename T>
class IndicatorSeed3D : public IndicatorF3D<T> {
private:
CuboidGeometry3D<T>& _cuboidGeometry;
IndicatorF3D<T>& _domainInternal; // Internal part of the computational domain
T _interval; // Displacements intervals of seed tracer from origin along coordinate dimensiont
public:
IndicatorSeed3D(CuboidGeometry3D<T>& cuboidGeometry, IndicatorF3D<T>& domainInternal, T interval);
bool operator() (bool output[], const T input[]) override;
};


//////////////////////////////////////// class PointValueTracer ////////////////////////////////

template <typename T, typename DESCRIPTOR>
class PointValueTracer : public util::ValueTracer<T> {
public:
  PointValueTracer ( SuperLattice<T, DESCRIPTOR>& sLattice,
                     UnitConverter<T,DESCRIPTOR>& converter,
                     int deltaT, T epsilon,
                     T point[] );
  void takeValue(bool doPrint=false);
private:
T _point[3];
SuperLattice<T, DESCRIPTOR>& _sLattice;
UnitConverter<T,DESCRIPTOR>& _converter;
std::shared_ptr<SuperLatticePhysVelocity3D<T,DESCRIPTOR>> _velocity;
std::shared_ptr<AnalyticalFfromSuperF3D<T>> _intpolateVelocity;
};

} // namespace olb

#endif
