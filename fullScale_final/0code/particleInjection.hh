/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARTICLE_INJECTION_HH
#define PARTICLE_INJECTION_HH

#include "core/unitConverter.h"
#include "particleInjection.h"

namespace olb {


/////////////////////////////////////////////////// ParticleInjectionUnit ///////////////////////////////////////////////////

template<typename T, template<typename U> class PARTICLETYPE>
ParticleInjectionUnit<T,PARTICLETYPE>::ParticleInjectionUnit ( SuperParticleSystem3D<T,PARTICLETYPE>& spSys, T timestep,
                          T flowRate, T radius, T rhoParticle, T rhoFluid, T t0, T t1, std::vector<T> pos, std::vector<T> vel )
  : _spSys(spSys),
    _radius(radius),
    _mas(4./3. * M_PI * util::pow(_radius, 3) * rhoParticle),
    //_masAdd(4./3. * M_PI * util::pow(_radius, 3) * (rhoParticle + 0.5*rhoFluid)), // Before merging
    _masAdd(2./3. * M_PI * util::pow(_radius, 3) * rhoFluid),
    _injectionInterval(_mas / (rhoParticle * flowRate)),
    _timestepOverInjection(timestep / _injectionInterval),
    _t0(t0), _t1(t1),
    _pos(pos), _vel(vel)
{}

template<typename T, template<typename U> class PARTICLETYPE>
void ParticleInjectionUnit<T,PARTICLETYPE>::addParticle()
{
  _residual += _timestepOverInjection;
  int particlesToAdd = (int)(util::floor(_residual));
  _residual -= particlesToAdd;
  for (int i=0; i<particlesToAdd; i++) {
    PARTICLETYPE<T> par(_pos, _vel, _mas, _radius, 0, _masAdd);
    _spSys.addParticle(par);
  }
}


template<typename T, template<typename U> class PARTICLETYPE>
T ParticleInjectionUnit<T,PARTICLETYPE>::getT0()
{
  return _t0;
}

template<typename T, template<typename U> class PARTICLETYPE>
T ParticleInjectionUnit<T,PARTICLETYPE>::getT1()
{
  return _t1;
}


/////////////////////////////////////////////////// ParticleInjectionSystem ///////////////////////////////////////////////////

template<typename T, typename DESCRIPTOR, template<typename U> class PARTICLETYPE>
ParticleInjectionSystem<T,DESCRIPTOR,PARTICLETYPE>::
ParticleInjectionSystem(SuperParticleSystem3D<T,PARTICLETYPE>& spSys, UnitConverter<T,DESCRIPTOR>& converter)
  : _spSys(spSys),
    _converter(converter)
{}

template<typename T, typename DESCRIPTOR, template<typename U> class PARTICLETYPE>
void ParticleInjectionSystem<T,DESCRIPTOR,PARTICLETYPE>::
addParticleInjectionUnit(std::shared_ptr<ParticleInjectionUnit<T,PARTICLETYPE>> injectionUnit)
{
  _injectionUnits.push_back(injectionUnit);
}

template<typename T, typename DESCRIPTOR, template<typename U> class PARTICLETYPE>
void ParticleInjectionSystem<T,DESCRIPTOR,PARTICLETYPE>::process(size_t iT)
{
  for (auto&& injectionUnit : _injectionUnits) {
    if ( (iT > _converter.getLatticeTime(injectionUnit->getT0())) &&
         (iT < _converter.getLatticeTime(injectionUnit->getT1())) ) {
      injectionUnit->addParticle();
    }
  }
}


} // namespace olb

#endif
