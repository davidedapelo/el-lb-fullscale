/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARTICLE_INJECTION_H
#define PARTICLE_INJECTION_H

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleInjectionUnit {
public:
  // Constructor
  ParticleInjectionUnit ( SuperParticleSystem3D<T,PARTICLETYPE>& spSys, T timestep,
                          T flowRate, T radius, T rhoParticle, T rhoFluid, T t0, T t1, std::vector<T> pos, std::vector<T> vel={T(),T(),T()} );
  // Adds a particle to _spSys if the conditions are met
  void addParticle();
  // Returns _injectionInterval (to allow ParticleInjectionSystem determine the right timestep to add a particle)
  // Returns t0
  T getT0();
  // Returns t1
  T getT1();
private:
  // Reference to the SuperParticleSystem3D
  SuperParticleSystem3D<T,PARTICLETYPE>& _spSys;
  // Particle nominal radius
  T _radius;
  // Particle mass
  T _mas;
  // Particle added mass
  T _masAdd;
  // Time interval between two successive particle injections
  T _injectionInterval;
  // Ratio of timestep over injection ingterval
  T _timestepOverInjection;
  // Fraction of particle still to be injected
  T _residual {T()};
  // Injection starting and stopping times
  T _t0, _t1;
  // Initial particle position
  std::vector<T> _pos;
  // Initial particle velocity
  std::vector<T> _vel;
};

template<typename T, typename DESCRIPTOR, template<typename U> class PARTICLETYPE>
class ParticleInjectionSystem {
public:
  // Constructor
  ParticleInjectionSystem(SuperParticleSystem3D<T,PARTICLETYPE>& spSys, UnitConverter<T,DESCRIPTOR>& converter);
  // Adds an element to _injectionUnits
  void addParticleInjectionUnit(std::shared_ptr<ParticleInjectionUnit<T,PARTICLETYPE>> injectionUnit);
  // Makes the actual job at the timestep iT - which is, adds the particles if the conditions meet
  void process(size_t iT);
private:
  // Reference to the SuperParticleSystem3D
  SuperParticleSystem3D<T,PARTICLETYPE>& _spSys;
  // Reference to a UnitConverter
  UnitConverter<T,DESCRIPTOR>& _converter;
  // Array of smart pointers of ParticleInjectionUnit
  std::vector<std::shared_ptr<ParticleInjectionUnit<T,PARTICLETYPE>>> _injectionUnits;
};

} // namespace olb

#endif
