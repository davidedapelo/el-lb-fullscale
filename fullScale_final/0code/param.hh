/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2020 Mathias J, Krause, Davide Dapelo
 *  Vojtech Cvrcek, Peter Weisbrod
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARAM_HH
#define PARAM_HH

#include "param.h"
#include <stdexcept>

namespace olb {

template <typename T, typename DESCRIPTOR>
void Param<T,DESCRIPTOR>::buildVectorParam (std::vector<T>& par, std::string content)
{
  std::stringstream ss;
  T s;
  ss << content;

  while(true) {
    ss >> s;
    if (! ss) break;
    par.push_back(s);
  }
}

template <typename T, typename DESCRIPTOR>
UnitConverter<T,DESCRIPTOR> Param<T,DESCRIPTOR>::initParamAndCreateConverter(std::string fName)
{
  OstreamManager clout(std::cout,"initParamAndCreateConverter");

  XMLreader config(fName);

  config["Coupling"]["backCouplingType"].read(backCouplingType);
  config["Coupling"]["kernelLength"].read(kernelLength);
  config["Coupling"]["forwardCouplingType"].read(forwardCouplingType);
  config["Coupling"]["dragModel"].read(dragModel);

  config["setup"]["Nx"].read(Nx);
  config["setup"]["latticeU"].read(latticeU);
  config["setup"]["nuMin"].read(nuMin);
  config["setup"]["nuMax"].read(nuMax);
  config["setup"]["smago"].read(smago);
  config["setup"]["subCycles"].read(subCycles);
  config["setup"]["minRange"].read(minRange);
  config["setup"]["uInRatio"].read(uInRatio);
  config["setup"]["cuboidsMult"].read(cuboidsMult);
  config["setup"]["logscale"].read(logscale);
  config["setup"]["antiDiffusionTuning"].read(antiDiffusionTuning);

  config["simul"]["cmc"].read(cmc);
  config["simul"]["flowRate"].read(flowRate);
  config["simul"]["inDir"].read(inDir);
  config["simul"]["outDir"].read(outDir);
  config["simul"]["geoType"].read(geoType);
  config["simul"]["gridFilename"].read(gridFilename);
  config["simul"]["velFilename"].read(velFilename);
  config["simul"]["plotFilename"].read(plotFilename);
  config["simul"]["phiFilename"].read(phiFilename);
  config["simul"]["viscoFilename"].read(viscoFilename);
  config["simul"]["test"].read(test);

  config["dynamics"]["charU"].read(charU);
  config["dynamics"]["d"].read(d);
  config["dynamics"]["uLegend0"].read(uLegend0);
  config["dynamics"]["uLegendOffs"].read(uLegendOffs);
  config["dynamics"]["m"].read(m);
  config["dynamics"]["n"].read(n);
  config["dynamics"]["g"].read(g);
  config["dynamics"]["rhoL"].read(rhoL);
  config["dynamics"]["rhoPnominal"].read(rhoPnominal);
  config["dynamics"]["uExp"]["v" + std::to_string(cmc) + std::to_string(flowRate)].read(uExp);
  config["dynamics"]["uRef"].read(uRef);
  config["dynamics"]["physViscosity"].read(physViscosity);

  config["postProcessing"]["seedingType"].read(seedingType);
  config["postProcessing"]["ballRelR"].read(ballRelR);
  config["postProcessing"]["ballRelX"].read(ballRelX);
  config["postProcessing"]["ballRelZ"].read(ballRelZ);
  config["postProcessing"]["seedRelInterval"].read(seedRelInterval);

  config["geometry"]["surfBord"].read(surfBord);
  config["geometry"]["dExt"].read(dExt);
  config["geometry"]["dInt"].read(dInt);
  config["geometry"]["h"].read(h);
  config["geometry"]["h0"].read(h0);
  config["geometry"]["yOffset"].read(yOffset);

  config["injectors"]["flowRateValue"].read(flowRateValue);
  config["injectors"]["injEquivD"].read(injEquivD);
  std::vector<T> injPosX {};
  std::vector<T> injPosY {};
  std::vector<T> injPosZ {};
  std::string str_injPosX = "";
  std::string str_injPosY = "";
  std::string str_injPosZ = "";
  std::string str_injScaling = "";
  std::string str_injT0 = "";
  std::string str_injT1 = "";
  config["injectors"]["injPosX"].read(str_injPosX);
  config["injectors"]["injPosY"].read(str_injPosY);
  config["injectors"]["injPosZ"].read(str_injPosZ);
  config["injectors"]["injScaling"].read(str_injScaling);
  config["injectors"]["injT0"].read(str_injT0);
  config["injectors"]["injT1"].read(str_injT1);
  buildVectorParam(injPosX, str_injPosX);
  buildVectorParam(injPosY, str_injPosY);
  buildVectorParam(injPosZ, str_injPosZ);
  buildVectorParam(injScaling, str_injScaling);
  buildVectorParam(injT0, str_injT0);
  buildVectorParam(injT1, str_injT1);
  assert(injPosX.size() == injPosY.size());
  assert(injPosX.size() == injPosZ.size());
  assert(injPosX.size() == injT0.size());
  assert(injPosX.size() == injT1.size());
  for (size_t i=0; i<injPosX.size(); i++) {
    std::vector<T> provv {injPosX[i], injPosY[i], injPosZ[i]};
    adjustInjZ(provv);
    injPos.push_back(provv);
  }

  config["time"]["Tinit"].read(Tinit);
  config["time"]["Tmax"].read(Tmax);
  config["time"]["Tsave"].read(Tsave);
  config["time"]["Tstatus"].read(Tstatus);
  config["time"]["Tvtm"].read(Tvtm);
  config["time"]["TavgPlot"].read(TavgPlot);

  // Preliminary-run quantities #1
  radius1 = 0.5 * d * 1e-3;
  T physDeltaX = dExt / Nx;
  T physDeltaT = latticeU / charU * physDeltaX;

  UnitConverter<T,DESCRIPTOR> const converter(
    ( T )   physDeltaX, //physDeltaX
    ( T )   physDeltaT, //physDeltaT,
    ( T )   injEquivD, //charPhysLength // <---
    ( T )   charU, //charPhysVelocity
    ( T )   physViscosity, //physViscosity
      ( T )   rhoL
  );

  mLB = m/( util::pow( converter.getConversionFactorTime(),n-2 )*util::pow( converter.getConversionFactorLength(),2 ));
  nuMinLB = nuMin*converter.getConversionFactorTime()/util::pow( converter.getConversionFactorLength(),2 );
  nuMaxLB = nuMax*converter.getConversionFactorTime()/util::pow( converter.getConversionFactorLength(),2 );

  if (forwardCouplingType=="sungkorn" || forwardCouplingType=="step" || forwardCouplingType=="vanWachem") {
    particlesOverlap = 2*converter.getLatticeLength(kernelLength);
    latticeOverlap = (int)(particlesOverlap)+1;
  }
  else if (forwardCouplingType=="naive" && backCouplingType=="local") {
    particlesOverlap = 1;
    latticeOverlap = 2;
  }

  return converter;
}


template <typename T, typename DESCRIPTOR>
void Param<T,DESCRIPTOR>::adjustInjZ(std::vector<T>& R)
{
  if (h0 == T())
    return;
  T r = util::sqrt(R[0]*R[0] + R[1]*R[1]);
  R[2] -= h0 * (r>dInt ? ( (2.*r - dInt) / (dExt - dInt) ) : 1.);
}


template <typename T, typename DESCRIPTOR>
void Param<T,DESCRIPTOR>::print()
{
  OstreamManager clout(std::cout,"Param");

  clout << "------------------ Statistics of the current run ------------------" << std::endl;
  if (test==0) {
    clout << "Ordinary run." << std::endl;
  }
  else {
    clout << "Testing with suppressed non-console output the first " << test << " timesteps." << std::endl;
  }

  clout << std::endl;
  clout << "-- Coupling:" << std::endl;
  if (forwardCouplingType == "naive") {
    clout << "Naive forward-coupling." << std::endl;
  }
  else if (forwardCouplingType == "ladd") {
    clout << "Ladd forward-coupling." << std::endl;
  }
  else if (forwardCouplingType == "sungkorn") {
    clout << "Forward-coupling as in Sungkorn et al. (2011), kernel length=" << kernelLength << std::endl;
  }
  else if (forwardCouplingType == "vanWachem") {
    clout << "Forward-coupling as in Evrard, Denner and van Wachem (2019), kernel length=" << kernelLength << std::endl;
  }
  else if (forwardCouplingType == "step") {
    clout << "Stepwise, non-local forward-coupling, kernel length=" << kernelLength << std::endl;
  }
  else {
    clout << "Wrong forward-coupling type. Quitting." << std::endl;
    singleton::exit(1);
  }

  if ( ! (forwardCouplingType == "sungkorn" || forwardCouplingType == "step" || forwardCouplingType == "vanWachem") ) {
    if (backCouplingType == "delta") {
      clout << "Back-coupling as per in Meier et al. (2017)." << std::endl;
    }
    else if (backCouplingType == "local") {
      clout << "Back-coupling only at the cell occupied by the particle." << std::endl;
    }
    else {
      clout << "Wrong back-coupling type. Quitting." << std::endl;
      singleton::exit(1);
    }
  }
  else {
    clout << "Back-coupling model following the forward-coupling." << std::endl;
  }

  if (dragModel == "Dewsbury") {
    clout << "Dewsbury drag model." << std::endl;

  }
  else if (dragModel == "Morsi") {
    clout << "Morsi drag model." << std::endl;
  }
  else if (dragModel == "Sun") {
    clout << "Sun drag model." << std::endl;
  }
  else {
    clout << "Wrong drag model. Quitting." << std::endl;
    singleton::exit(1);
  }

  clout << "Lattice overlap                  latticeOverlap=  " << latticeOverlap <<std::endl;
  clout << "Particles overlap                particlesOverlap=" << particlesOverlap << std::endl;
  clout << std::endl;
  clout << "-- Post-processing:" << std::endl;
  clout << "Seeding type:                    seedingType= "     << seedingType << std::endl;
  if (seedingType == "ball") {
    clout << "Ball rel radius:                 ballRelR= "        << ballRelR << std::endl;
    clout << "Ball rel x coordinate:           ballRelX= "        << ballRelX << std::endl;
    clout << "Ball rel z coordinate:           ballRelZ= "        << ballRelZ << std::endl;
  }
  else if (seedingType == "seed") {
    clout << "Seed rel interval:               seedRelInterval= " << seedRelInterval << std::endl;
  }
  else { throw std::invalid_argument("Wrong seedingType=" + seedingType + ": only ball and seed admittable."); }
  clout << "Anti-diffusion tuning:           antiDiffusionTuning= " << antiDiffusionTuning << std::endl;
  clout << std::endl;
  clout << "-- Indices:" << std::endl;
  clout << "CMC index:                       cmc=             " << cmc << std::endl;
  clout << "Flow rate index:                 flowRate=        " << flowRate << std::endl;
  clout << "Cuboids multiplier:              cuboidsMult=     " << cuboidsMult << std::endl;
  clout << "Minimum quota vor vel avg:       minRange=        " << minRange << std::endl;
  clout << std::endl;
  clout << "-- Geometry:" << std::endl;
  clout << "Voxels along X direction:        Nx=              " << Nx << std::endl;
  clout << "External diameter (m)            dExt=            " << dExt << std::endl;
  clout << "Bottom frustum diameter (m):     dInt=            " << dInt << std:: endl;
  clout << "Cylinder height (m):             h=               " << h << std::endl;
  clout << "Frustum height (m):              h0=              " << h0 << std::endl;
  clout << "Injector equiv. diameter (m):    injEquivD=       " << injEquivD << std::endl;
  clout << "Voxels at the surface border:    surfBord=        " << surfBord << std::endl;
  clout << "PIV vertical plane's offset(m):  yOffset=         " << yOffset << std::endl;
  for (size_t i=0; i<injT0.size(); i++) {
    clout << "Nozzle series " << i << ": injScaling=" << injScaling[i] << "; injPos=(" << injPos[i][0] << ", " << injPos[i][1] << ", " << injPos[i][2] << "); time interval=(" << injT0[i] << ", " << injT1[i] << ")" << std::endl;
  }
  clout << std::endl;
  clout << "-- Dynamics:" << std::endl;
  clout << "Legend max vel for plot(m/s):    uLegend0=        " << uLegend0 << std::endl;
  clout << "Legend max vel for plot(m/s):    uLegendOffs=     " << uLegendOffs << std::endl;
  clout << "Exp rising bubble vel(m/s):      uExp=            " << uExp << std::endl;
  clout << "Reference (unscaled) vel(m/s):   uRef=            " << uRef << std::endl;
  clout << "Flow rate(m^3/s):                flowRateValue=   " << flowRateValue << std::endl;
  clout << "Power-law index:                 n=               " << n << std::endl;
  clout << "Power-law coeff.(m^2*s^(n-2)):   m=               " << m << std::endl;;
  clout << "Minimum viscosity 1(m^2/s):      nuMin=           " << nuMin << std::endl;
  clout << "Maximum viscosity 1(m^2/s):      nuMax=           " << nuMax << std::endl;
  clout << "Smagorinsky constant:            smago=           " << smago <<std::endl;
  clout << "No. of Lagrangian subcycles:     subCycles=       " << subCycles <<std::endl;
  clout << std::endl;
  clout << "-- Bubbles:" << std::endl;
  clout << "Inlet vel (ratio over charU)     uInRatio=        " << uInRatio << std::endl;
  clout << "Diameter(mm):                    d=               " << d << std::endl;
  clout << "Radius, prelim. run(m):          radius1=         " << radius1 << std::endl;
  clout << "Nominal density(kg/m^3):         rhoPnominal=     " << rhoPnominal << std::endl;
  clout << std::endl;
  clout << "-- Simulation times:" << std::endl;
  clout << "Initial timestep(s):             Tinit=           " << Tinit << std::endl;
  clout << "Preliminary run maximum time(s): Tmax=            " << Tmax << std::endl;
  clout << "Print-on-screen time(s):         Tstatus=         " << Tstatus << std::endl;
  clout << "Saving time(s):                  Tsave=           " << Tsave << std::endl;
  clout << "Print-on-vtm time(s):            Tvtm=            " << Tvtm << std::endl;
  clout << "First timestep to b averaged(s): TavgPlot=        " << TavgPlot << std::endl;
  clout << "-------------------------------------------------------------------" << std::endl;
}

} // namespace olb

#endif
