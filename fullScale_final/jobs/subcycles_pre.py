import sys

from resources.DigesterOLBxml import OrigDigesterOLBxml as XMLgenerator
from resources.pyOperations import createNames_std as createNames, standardOperations_pre

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    tmpFilename = sys.argv[1] # WARNING: this cannot be changed!!

    # Data: run-specific
    array_index = int(sys.argv[2])

    # Data: common to this series of runs
    subcycles = [ \
            20, 60, 80, 100,
            120, 140, 160, 180, 200,
            220, 240, 260, 280, 300,
            320, 340, 360, 380, 400 ]

    # WARNING: change the values, but NOT the items!!
    data = {
            'antiDiffusionTuning': 0.,
            'diamStr': '050',
            'flowRateRatioStr': '05',
            'nx' : 30,
            'nx0' : 60,
            'rheoIndex': 0,
            'seedingType': 'seed',
            'subcycles': subcycles[array_index],
            'uLB0' : 0.15,
            'viscoCorrection': 0.25,
            'viscoHalfSpan' : 10
            }

    # All the standard operations. WARNING: this cannot be changed!!
    standardOperations_pre(tmpFilename, data, XMLgenerator(data), createNames)
