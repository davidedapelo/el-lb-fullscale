import sys

from resources.DigesterOLBxml import OrigDigesterOLBxml as XMLgenerator
from resources.pyOperations import createNames_std, standardOperations_pre

###############################################################################################################################
def createNames(olbXML, data):
###############################################################################################################################
    """
    Creates the folder name containing all the run series, the folder name for the current run and the log filename.
    Generating sequence: git_ignore/long120__<TS>__<flow>__<d>__<nuCorr>/<nx0>__<uLB0>__<subC>.
    Args:
        olbXML:      OLBxml: XML generator
        data:        dict:   Dictionary containing the custom input to the xml
    Returns:
        folder_base: str:    Folder name containing all the run series
        folderRun:   str:    Folder name for the current run
        logFilename: str:    Log filename for the current run
    """
    return createNames_std(olbXML, data, 'long60__')

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    tmpFilename = sys.argv[1] # WARNING: this cannot be changed!!

    # Data: run-specific
    array_index = int(sys.argv[2])
    subcycles = [ \
             25,  35,  45,  55,  65,  75,  85,  95,  105,  115, \
            125, 135, 145, 155, 165, 175, 185, 195,  205,  215, \
            225, 235, 245, 255, 265, 275, 285, 295,  305,  315, \
            325, 335, 345, 355, 365, 375, 385, 395,  405,  415, \
             25,  35,  45,  55,  65,  75,  85,  95,  105,  115, \
            125, 135, 145, 155, 165, 175, 185, 195,  205,  215, \
            225, 235, 245, 255, 265, 275, 285, 295,  305,  315, \
            325, 335, 345, 355, 365, 375, 385, 395,  405,  415, \
            420
            ]
    seeding = [\
            'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', \
            'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', \
            'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', \
            'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', 'ball', \
            'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', \
            'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', \
            'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', \
            'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', 'seed', \
            'seed'
            ]
 
    # Data: common to this series of runs

    # WARNING: change the values, but NOT the items!!
    data = {
            'antiDiffusionTuning': 0.,
            'diamStr': '050',
            'flowRateRatioStr': '05',
            'nx' : 80,
            'nx0' : 60,
            'rheoIndex': 0,
            'seedingType': seeding[array_index],
            'subcycles': subcycles[array_index],
            'uLB0' : 0.15,
            'viscoCorrection': 0.25,
            'viscoHalfSpan' : 10
            }

    # All the standard operations. WARNING: this cannot be changed!!
    standardOperations_pre(tmpFilename, data, XMLgenerator( \
            data, \
            input_postProcessing='jobs/input_long60_postProcessing.xml', \
            input_time='jobs/input_long60_time.xml'\
            ), createNames)
