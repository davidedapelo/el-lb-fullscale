import sys

from resources.DigesterOLBxml import OrigDigesterOLBxml as XMLgenerator
from resources.pyOperations import createNames_std, standardOperations_pre

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    tmpFilename = sys.argv[1] # WARNING: this cannot be changed!!

    # Data: run-specific
    array_index = int(sys.argv[2])

    # Data: common to this series of runs
    number_of_nodes = sys.argv[3]

    # WARNING: change the values, but NOT the items!!
    data = {
            'antiDiffusionTuning': 0.,
            'diamStr': '050',
            'flowRateRatioStr': '05',
            'nx' : 320,
            'nx0' : 60,
            'rheoIndex': 0,
            'seedingType': 'ball',
            'subcycles': 20,
            'uLB0' : 0.15,
            'viscoCorrection': 0.25,
            'viscoHalfSpan' : 10
            }

#==============================================================================================================================
    def createNames(olbXML, data):
#==============================================================================================================================
        """
        Creates the folder name containing all the run series, the folder name for the current run and the log filename.
        Generating sequence: git_ignore/<name[array_index]>__<TS>__<flow>__<d>__<nuCorr>/<nx0>__<uLB0>__<subC>.
        Args:
            olbXML:      OLBxml: XML generator
            data:        dict:   Dictionary containing the custom input to the xml
        Returns:
            folder_base: str:    Folder name containing all the run series
            folderRun:   str:    Folder name for the current run
            logFilename: str:    Log filename for the current run
        """
        return createNames_std(olbXML, data, 'scaleup_' + number_of_nodes + '_')

    # All the standard operations. WARNING: this cannot be changed!!
    standardOperations_pre(tmpFilename, data, XMLgenerator(data,
        input_time='jobs/input_time__scaleup.xml',
        ), createNames)
