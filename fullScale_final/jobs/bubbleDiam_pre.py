import sys

from resources.DigesterOLBxml import OrigDigesterOLBxml as XMLgenerator
from resources.pyOperations import createNames_std as createNames, standardOperations_pre

###############################################################################################################################
if __name__ == "__main__":
###############################################################################################################################
    tmpFilename = sys.argv[1] # WARNING: this cannot be changed!!

    # Data: run-specific
    array_index = int(sys.argv[2])

    # Data: common to this series of runs
    diamStr = [\
            '010', '020', '030', '040' \
            ]

    # WARNING: change the values, but NOT the items!!
    data = {
            'antiDiffusionTuning': 0.,
            'diamStr': diamStr[array_index],
            'flowRateRatioStr': '05',
            'nx' : 80,
            'nx0' : 60,
            'rheoIndex': 0,
            'seedingType': 'ball',
            'subcycles': 100,
            'uLB0' : 0.15,
            'viscoCorrection': 0.25,
            'viscoHalfSpan' : 10
            }

    # All the standard operations. WARNING: this cannot be changed!!
    standardOperations_pre(tmpFilename, data, XMLgenerator(data), createNames)
