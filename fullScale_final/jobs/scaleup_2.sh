#!/usr/bin/env bash

# Barkla setup
#SBATCH -D ./
#SBATCH --export=ALL
#SBATCH --mem-per-cpu=8000M
#SBATCH -o scaleup_1_%j.out
#SBATCH -J scal_1
#SBATCH -p nodes -N 2 -n 80
#SBATCH -t 00:60:00

##############################################################################################
# JOB SCRIPT                                                                                 #
# To be called through sbatch command to run a simulation on HPC (or laptop).                #
# MUST be launched from fullScale_final folder!                                              #
#                                                                                            #
# Usage on Cirrus:                                                                           #
# qsub 0resources/script.sh                                                                  #
#                                                                                            #
# Usage on the other HPCs:                                                                   #
# sbatch 0resources/script.sh                                                                #
#                                                                                            #
# Only on laptop:                                                                            #
# .j/script.sh                                                                               #
##############################################################################################

# resources/hpc_bradford.sh does the job in a general way. It requires two arguments:
# 1: The path of a python post-processing script;
# 2: The path of a python post-processing script.
chmod a+x resources/hpc_bradford.sh && ./resources/hpc_bradford.sh \
  jobs/scaleup_pre.py \
  jobs/subcycles_post.py
