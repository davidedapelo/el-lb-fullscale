#!/bin/bash

#SBATCH --job-name=job_longS
#SBATCH --account=66019_seed001
#SBATCH --partition=compute
#SBATCH --ntasks=40
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node=40
#SBATCH --ntasks-per-socket=20
#SBATCH --time 40:00:00
#SBATCH --array 0-80
# standard: ntask=40, nodes=1, cpus-per-node=40, cpu-per-task=1, ntasks-per-node=40, ntasks-per-socket=20

##############################################################################################
# JOB SCRIPT                                                                                 #
# To be called through sbatch command to run a simulation on HPC (or laptop).                #
# MUST be launched from fullScale_final folder!                                              #
#                                                                                            #
# Usage on Cirrus:                                                                           #
# qsub 0resources/script.sh                                                                  #
#                                                                                            #
# Usage on the other HPCs:                                                                   #
# sbatch 0resources/script.sh                                                                #
#                                                                                            #
# Only on laptop:                                                                            #
# .j/script.sh                                                                               #
##############################################################################################

# resources/hpc_bradford.sh does the job in a general way. It requires two arguments:
# 1: The path of a python post-processing script;
# 2: The path of a python post-processing script.
chmod a+x resources/hpc_bradford.sh && ./resources/hpc_bradford.sh \
  jobs/longS_pre.py \
  jobs/subcycles_post.py
